# docproc

Bringing Model-View-Controller to business communcation documents

## embedded json-schema for validation

With this feature it is possible to add a json schema within an html template (matter/documentdefinition) within a script tag that has a class attribute with value schema and the type attribute with the value "application/json" like this

```html
<script class="schema" type="application/json">
{ // ... }
</script>
```

The schema of an template can be retrieved via http by adding the type query parameter with the "schema" value, e.g. `http://localhost/documentdefinitions/example.html?type=schema`

If the template contains/references not other templates then the response of that endpoint will be the content of the script tag. Otherwise it will be an allOf combination of the script tags content and $ref elements pointing to the schemas of the reference templates. 

```json
{
  "allOf": [
    {
      //content of script tag
    },
    {
      "$ref": "http://localhost/matter/postaladdress.html?type=schema"
    }
  ]
}
```

this enables a cascade of schemas which the starting point at the url of the template that is associated with a job. 
So when that association is established we can use the url with the `type=schema` parameter to validate the data of the job against that cascade

## rules

It's possible to use the composer web application to add smaller templates (aka "matter") to a larger template (aka "documentdefinition") and add a boolean expression to it: a rule. when the document is rendered that expression decides whether the "matter" is added to the rendered document or not.

![composer view where the rule editor icon (a §) when the mouse hovers above a matter included in the documentdefinition](src/main/docs/rules/ruleicon_when_hovering_over_matter.png)

The rule is written as a thymeleaf standard expression and can use any data that has been saved on the job before it was started. Also the [Expression Utility Objects](https://www.thymeleaf.org/doc/tutorials/3.0/usingthymeleaf.html#appendix-b-expression-utility-objects) provided by thymeleaf can be used.

![an opened ruleeditor containing the expression ${cases.installationNeeded} and ${#lists.contains(cases.supplements,'TCK007')} ](src/main/docs/rules/ruleeditor_ognl.png)

## blockly

to define rules docproc comes with a blockly editor which enables users which don't want to write the thymeleaf expressions by hand to create the rules dynamically

there is a block that allows to define comparisons and contains operations:

<table>
<tr><th>block image</th><th>thymeleaf expression</th>
<tr>
  <td><img src="src/main/docs/rules/blockly/blockly_logic_compare.png"/></td>
  <td>
    <pre>${cases.installationNeeded} == true</pre>
    <pre>${cases.installationNeeded} != true</pre>
    <pre>${cases.installationNeeded} &gt; true</pre>
    <pre>${cases.installationNeeded} &ge; true</pre>
    <pre>${cases.installationNeeded} &lt; true</pre>
    <pre>${cases.installationNeeded} &le; true</pre>
    <pre>${#links.contains(cases.supplements, 'TCK007')}</pre>
    <pre>${#strings.contains(ingredients, 'blue milk')}</pre>
  </td>
</tr>
<tr>
  <td><img src="src/main/docs/rules/blockly/blockly_logic_operation.png"/></td>
  <td>
    <pre>${cases.installationNeeded} == true and ${#lists.contains(cases.supplements, 'TCK007')}</pre>
    <pre>${cases.installationNeeded} == true or ${#lists.contains(cases.supplements, 'TCK007')}</pre>
  </td>
</tr>
</table>

Implementation hint: there is a subtle difference between the operation "contains" when it's used for strings or arrays from the json. Because the contains is actually implemented with an OGNL expression that calls contains method on the Expression Object Helper "#lists" or "#strings" respectively. Whereas the other operators are implemented as Expression objects in thymeleaf. 
