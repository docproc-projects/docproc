package net.lueckonline.docproc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class DocprocApplication {

	public static void main(String[] args) {
		SpringApplication.run(DocprocApplication.class, args);
	}

}
