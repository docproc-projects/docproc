package net.lueckonline.docproc.configs;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JobConfig {

   @Bean("jobExecutors") 
   public ExecutorService createJobExecutors(@Value("${jobs.maxThreads:5}") int maxThreads) {

     ThreadPoolExecutor threadPool = (ThreadPoolExecutor) Executors.newFixedThreadPool(1);
     threadPool.setMaximumPoolSize(maxThreads);
     return  threadPool;
   }
}
