package net.lueckonline.docproc.configs;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter{

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.authorizeRequests()
      // all GET requests are accepted even for non authenticated users.
      // we need this because the template engine and the pdf converters will pull the resources 
      // and we would need to configure them with authentication data
      .antMatchers("/static/composer.html").authenticated()
      .antMatchers(HttpMethod.GET).permitAll()
      .anyRequest().authenticated().and()
      .httpBasic().and()
      .formLogin().defaultSuccessUrl("/static/composer.html").and()
      .headers().frameOptions().sameOrigin().and()
      // we dont't have a state/disable session handling; so there should be no need for csrf
      .csrf().disable();
  }
  
}
