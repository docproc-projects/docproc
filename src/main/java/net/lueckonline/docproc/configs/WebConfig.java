package net.lueckonline.docproc.configs;

import java.nio.file.Path;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

  @Value("${documentporfolio.matter.path}")
  Path matterPath;
  
  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    
    /*
     * fetching the content of matter resources is done with a resource handler 
     * for serving static content.
     */
    registry
      /* Restrict the handler to serve html only 
       * be very careful with this because it may serve confidential files */
      .addResourceHandler("/matter/**/*.html")
      /* the trailing slash is important to make sure it's used as a directory*/
      .addResourceLocations(matterPath.toUri().toString()+ "/");
    
    if(!registry.hasMappingForPattern("/webjars/**")) {
      registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
  
    if(!registry.hasMappingForPattern("/**")) {
      registry.addResourceHandler("/**").addResourceLocations("classpath:/static");
    }
    
  }
  
}
