package net.lueckonline.docproc.controllers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import net.lueckonline.docproc.services.BlocklyConversionService;
import net.lueckonline.docproc.services.BlocklyConversionService.ConversionError;
import net.lueckonline.docproc.thymeleaf.blockly.Workspace;

/**
 * Controller which helps with the integration of blockly
 * 
 * @author thuri
 */
@Controller
public class BlocklyController {

  private final Charset CHARSET = StandardCharsets.UTF_8;
  
  private static final Logger logger = LogManager.getLogger();

  private final BlocklyConversionService conversionService;
  private TransformerFactory transformerFactory;

  BlocklyController(BlocklyConversionService conversionService) {
    this.conversionService = conversionService;
    transformerFactory = TransformerFactory.newInstance();
  }
  
  @PostMapping(path = "/rules/transformation", produces = "application/vnd.blockly+xml")
  ResponseEntity<?> convertThymeleafExpressionToBlockly(@RequestBody String body) {
    
    try {
      
      Workspace workspace = conversionService.convertRule(body);
      
      var transformer = transformerFactory.newTransformer();
      transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
      transformer.setOutputProperty(OutputKeys.ENCODING, CHARSET.name());
      transformer.setOutputProperty(OutputKeys.INDENT, "no");
      
      try (var baos =  new ByteArrayOutputStream()) {
        transformer.transform(new DOMSource(workspace.export()), new StreamResult(baos));
        return ResponseEntity.ok().body(baos.toString(CHARSET));
      }
      
    } catch (ConversionError e) {
      logger.atError().log("Error during conversion", e);
      return ResponseEntity.badRequest().body("Error while trying to convert the rule. "+e.getMessage());
    } catch (TransformerConfigurationException e) {
      logger.atError().log("Error while creating Transformer ", e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    } catch (IOException | TransformerException e) {
      logger.atError().log("Error while transforming workspace xml to stream", e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
  }
}
