package net.lueckonline.docproc.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.util.AntPathMatcher;
import org.springframework.web.servlet.HandlerMapping;

final class ControllerUtils {

  /**
   * this method returns the part of the request path that doesn't contain
   * the part that is defined in RequestMapping annotations on the handling controller.
   * 
   * @param request
   * @return partial, relative path
   */
  public static String extractRelativeResourcePathFromRequest(HttpServletRequest request) {
    String path           = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
    String matchedPattern = (String) request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE);

    AntPathMatcher antMatcher = new AntPathMatcher();
    return antMatcher.extractPathWithinPattern(matchedPattern, path);
  }
}
