package net.lueckonline.docproc.controllers;

import static net.lueckonline.docproc.controllers.ControllerUtils.extractRelativeResourcePathFromRequest;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import net.lueckonline.docproc.errors.DocProcException;
import net.lueckonline.docproc.model.DocumentDefinition;
import net.lueckonline.docproc.services.DocDefService;
import net.lueckonline.docproc.services.PublishingService;
import net.lueckonline.docproc.services.TemplateService;

@Controller
@RequestMapping("/documentdefinitions")
public class DocDefController {
  
  Logger logger = LogManager.getLogger();
  
  private final DocDefService docDefService;

  private final PublishingService pubservice;
  
  private final TemplateService templateService;

  public DocDefController(DocDefService matterService, PublishingService pubservice, TemplateService templateService) {
    super();
    this.docDefService = matterService;
    this.pubservice = pubservice;
    this.templateService = templateService;
  }

  @GetMapping
  ResponseEntity<?> index() throws URISyntaxException {
    
    try {
      var docDefResources = docDefService.getDocumentDefinitions().stream()
         .map(dd -> EntityModel.of(dd))
         .collect(Collectors.toList());
      
      return ResponseEntity.status(HttpStatus.OK).body(CollectionModel.of(docDefResources));
      
    } catch (DocProcException e) {
      logger.atError().log("Error while trying to fetch documentdefinitions",e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
  }
  
  @GetMapping(path = "/**/*.*")
  ResponseEntity<?> docdef(HttpServletRequest request) {
    
    final var docdefpath = extractRelativeResourcePathFromRequest(request);
    
    try {
      
      var docdefContent = docDefService.readDocumentDefinition(docdefpath);
      var baseUrl = ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();
      
      if(StringUtils.equals(request.getParameter("type"), "schema")) {
        return ResponseEntity
          .ok()
          .contentType(MediaType.valueOf("application/json;charset=UTF-8"))
          // actually it would be better to have the docdef Service return a stream to the resource.
          // but currently it's not implemented this way.
          // this could change when Matter and docdefs are handeled in the same manner in the future
          .body(templateService.schema(new ByteArrayInputStream(docdefContent.getBytes(StandardCharsets.UTF_8)), baseUrl));
      } else {
        return ResponseEntity
          .status(HttpStatus.OK)
          .contentType(MediaType.valueOf("text/html;charset=UTF-8"))
          .body(docdefContent);
      }
    } catch (FileNotFoundException e) {
      logger.atError().log("requested file couldn't be found for uri {}", request.getRequestURI(), e);
      return ResponseEntity.notFound().build();
    } catch (IllegalArgumentException e) {
      logger.atError().log("requested file path was invalid for uri {}", request.getRequestURI(), e);
      return ResponseEntity.badRequest().build();
    } catch (IOException e) {
      logger.atError().log("error while trying to read docdef for uri {}", request.getRequestURI(), e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
  }
  
  @PostMapping(path = "/**/*.*")
  ResponseEntity<?> uploadDocDef(@RequestBody String body, HttpServletRequest request) {
    
    String docdefpath = extractRelativeResourcePathFromRequest(request);
    DocumentDefinition dd = DocumentDefinition.builder().templatepath(docdefpath).build();
    
    final String baseUri = ServletUriComponentsBuilder.fromRequest(request).build().toUriString();
    
    Document doc = Jsoup.parse(body, baseUri);
    
    /* 
     * the matter parts of the posted documentdefiniton could be removed with the following line.
     * But currently I think it's better to leave them in the HTML and 
     * make the link contain a commit hash or something so that the matter is fixed
     * there could be issues when the history is rewritten in git. 
     * doc.select("[data-matter-link]").forEach(e -> e.html(""));
     */
    
    try {
      docDefService.saveDocumentDefinition(dd, doc.outerHtml());
    } catch (IOException e) {
      logger.atError().log("error writing template {}", dd.getTemplatepath(), e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
    
    return ResponseEntity.status(HttpStatus.OK).build();
  }
  
  @PostMapping(path = "/**/publishment")
  public ResponseEntity<?> publishDocDef(HttpServletRequest request) {
    try {
      var docdefpath = extractRelativeResourcePathFromRequest(request).replace("/publishment", "");
      var docdef     = docDefService.getDocumentDefinitionForPath(docdefpath);
      pubservice.publish(docdef);
    } catch (DocProcException e) {
      logger.atError().log("Error while trying to publish docdef for URI {}", request.getRequestURI(), e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    } catch (FileNotFoundException e) {
      logger.atError().log("documentdefinition not found for requestUri {}", request.getRequestURI(), e);
      return ResponseEntity.notFound().build();
    }
    
    return ResponseEntity.noContent().build();
  }
}
