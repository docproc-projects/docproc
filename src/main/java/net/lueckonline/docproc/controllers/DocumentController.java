package net.lueckonline.docproc.controllers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/documents")
public class DocumentController {
  
  private static final Logger logger = LogManager.getLogger();

  private final Path outputDir;

  public DocumentController(@Value("${output.dir}") Path outputDir) {
    this.outputDir = outputDir;
  }
  
  @GetMapping(path = "/{jobId}")
  public ResponseEntity<?> getDocumentForJob(@PathVariable String jobId) {
    
    if(!Files.exists(outputDir.resolve(jobId+".pdf"))) 
      return ResponseEntity.notFound().build();
    else
      try {
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(Files.newInputStream(outputDir.resolve(jobId+".pdf"))));
      } catch (IOException e) {
        logger.atError().log("Error accesing document for jobId {}", jobId, e);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
      }
  }
}
