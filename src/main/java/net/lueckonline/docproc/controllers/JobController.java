package net.lueckonline.docproc.controllers;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.lueckonline.docproc.model.Job;
import net.lueckonline.docproc.repositories.JobRepository;
import net.lueckonline.docproc.services.validation.ValidationService;

@RestController
public class JobController {

  private static final Logger logger = LogManager.getLogger();
  
  private final JobRepository jobRepository;
  
  private final ValidationService validationService;
  
  private final ObjectMapper objectMapper;

  @Autowired
  public JobController(JobRepository jobRepository,
                       @Value("${documentdefinitions.servers.allowed:#{null}") 
                       String allowedServersCSV,
                       ObjectMapper objectMapper,
                       ValidationService validationService) {
    this.jobRepository = jobRepository;
    this.objectMapper  = objectMapper;
    this.validationService = validationService;
  }

  @PostMapping(path = "/jobs/{id}/htmltemplate", consumes = "text/uri-list")
  public ResponseEntity<?> associateDocDefWithJob(@PathVariable String id, @RequestBody String htmlTemplateUri) {
    
    URL docdefUrl;
    try {
      docdefUrl = new URL(htmlTemplateUri);
    } catch (MalformedURLException e) {
      return ResponseEntity.badRequest().body("Invalid URI passed as body");
    }
    
    final String baseUrl = ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();
    if(!htmlTemplateUri.startsWith(baseUrl))
      return ResponseEntity.badRequest().body("URI for documentdefiniton not allowed");

    Job job = jobRepository.findById(id).orElse(null);
    if (job == null) return ResponseEntity.notFound().build();
    
    try {
      var report = validationService.validate(docdefUrl, objectMapper.readTree(job.getData()));
      
      if(!report.isSuccess()) 
        return ResponseEntity.badRequest().header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN_VALUE).body(report.toString());
      
    } catch (JsonProcessingException e) {
      logger.atError().log("Error parsing json from job", e);
      return ResponseEntity.badRequest().header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN_VALUE).body("Invalid data passed to endpoint");
    }

    job.setHtmlTemplateUri(docdefUrl);
    jobRepository.save(job);

    return ResponseEntity.noContent().build();
  }
  
  @GetMapping(path = "/jobs/{id}/htmltemplate")
  public ResponseEntity<?> redirectToDocDef(@PathVariable String id) {
   
    Optional<Job> job = jobRepository.findById(id);
    if(job.isEmpty() || job.get().getHtmlTemplateUri() == null) return ResponseEntity.notFound().build();
    
    return ResponseEntity.status(HttpStatus.SEE_OTHER).header(org.springframework.http.HttpHeaders.LOCATION, job.get().getHtmlTemplateUri().toString()).build(); 
  }
}
