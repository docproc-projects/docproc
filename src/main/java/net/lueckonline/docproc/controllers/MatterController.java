package net.lueckonline.docproc.controllers;

import static net.lueckonline.docproc.controllers.ControllerUtils.extractRelativeResourcePathFromRequest;
import static org.apache.commons.lang3.StringUtils.contains;
import static org.apache.commons.lang3.StringUtils.startsWith;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.InputStreamResource;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import net.lueckonline.docproc.errors.DocProcException;
import net.lueckonline.docproc.model.Matter;
import net.lueckonline.docproc.services.MatterService;
import net.lueckonline.docproc.services.TemplateService;

@Controller
@RequestMapping("/matter")
public class MatterController {

  Logger logger = LogManager.getLogger();
  
  private final MatterService matterService;

  private final TemplateService templateService;
  
  public MatterController(MatterService matterService, TemplateService templateService) {
    super();
    this.matterService = matterService;
    this.templateService = templateService;
  }

  @GetMapping
  ResponseEntity<?> documentdefinitions() throws URISyntaxException {
    
    try {
      var matterResources= matterService.getMatter().stream()
                   .map(dd -> EntityModel.of(dd))
                   .collect(Collectors.toList());
      
      matterResources.forEach(e -> e.add(Link.of("/matter/"+e.getContent().getMatterPath()).withSelfRel()));
      
      return ResponseEntity.status(HttpStatus.OK).body(CollectionModel.of(matterResources));
      
    } catch (DocProcException e) {
      logger.atError().log("Error while trying to fetch matter",e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
  }
  
  @GetMapping(path = "/**/*.*")
  ResponseEntity<?> matter(HttpServletRequest request) {
    
    final var resourcePath = extractRelativeResourcePathFromRequest(request);
    final var baseUrl = ServletUriComponentsBuilder.fromContextPath(request).build().toUriString();
    
    try {
      Matter matterForPath = matterService.getMatterForPath(resourcePath);
      // using a supplier here because we don't want to open the stream before actually using it
      // but we can't use a try-with-resource here because it would close the stream to early when using an 
      // InputStreamResource as body for the ResponseEntity
      Supplier<InputStream> streamSupplier = () -> {
        try {
          return matterService.readMatter(matterForPath);
        } catch (IOException e) {
          throw new IllegalStateException("error while trying to read matter", e);
        }
      };
      if(contains(request.getHeader(HttpHeaders.ACCEPT), "text/css") || StringUtils.equals(request.getParameter("type"), "css")) {
        if(!startsWith(matterForPath.getContentType(), "text/html")) 
          return ResponseEntity.badRequest().body("styles can only be returned for html matter");
        
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_TYPE, "text/css;charset=UTF-8")
                .body(templateService.css(streamSupplier.get(), baseUrl));
      } if(StringUtils.equals(request.getParameter("type"), "schema")) {
        
        if(!startsWith(matterForPath.getContentType(), "text/html")) 
          return ResponseEntity.badRequest().body("styles can only be returned for html matter");
        
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_TYPE, "application/json;charset=UTF-8")
                .body(templateService.schema(streamSupplier.get(), baseUrl));
      }
      else {
        return ResponseEntity
            .ok()
            .header(HttpHeaders.CONTENT_TYPE, matterForPath.getContentType())
            .body(new InputStreamResource(streamSupplier.get()));
      }
    } catch (FileNotFoundException e) {
      logger.atError().log("requested matter couldn't be found", e);
      return ResponseEntity.notFound().build();
    } catch (IllegalArgumentException e) {
      logger.atError().log("requested matter path was invalid", e);
      return ResponseEntity.badRequest().build();
    } catch (IllegalStateException | IOException e) {
      logger.atError().log("error while trying to read matter", e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
  }
}
