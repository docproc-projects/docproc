package net.lueckonline.docproc.errors;

public class DocProcException extends Exception {

  private static final long serialVersionUID = 8960018064897048420L;

  public DocProcException() {
    super();
  }

  public DocProcException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

  public DocProcException(String message, Throwable cause) {
    super(message, cause);
  }

  public DocProcException(String message) {
    super(message);
  }

  public DocProcException(Throwable cause) {
    super(cause);
  }

}
