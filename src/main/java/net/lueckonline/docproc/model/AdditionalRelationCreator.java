package net.lueckonline.docproc.model;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.webmvc.RepositoryLinksResource;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelProcessor;

import net.lueckonline.docproc.controllers.DocDefController;
import net.lueckonline.docproc.controllers.DocumentController;
import net.lueckonline.docproc.controllers.JobController;
import net.lueckonline.docproc.controllers.MatterController;

@Configuration
public class AdditionalRelationCreator {

  @Bean 
  public RepresentationModelProcessor<EntityModel<Job>> jobResourceProcess() {
    return new RepresentationModelProcessor<EntityModel<Job>>() {
      @Override
      public EntityModel<Job> process(EntityModel<Job> model) {
        model.add(linkTo(methodOn(JobController.class).associateDocDefWithJob(model.getContent().getId(), null)).withRel("htmltemplate"));
        model.add(linkTo(methodOn(DocumentController.class).getDocumentForJob(model.getContent().getId())).withRel("document"));
        return model;
      }
    };
  }
  
  @Bean 
  public RepresentationModelProcessor<EntityModel<DocumentDefinition>> docDefResourceProcessor() {
    return new RepresentationModelProcessor<EntityModel<DocumentDefinition>>() {
      @Override
      public EntityModel<DocumentDefinition> process(EntityModel<DocumentDefinition> model) {
        model.add(linkTo(DocDefController.class).slash(model.getContent().getTemplatepath()).withSelfRel());
        model.add(linkTo(DocDefController.class).slash(model.getContent().getTemplatepath()).slash("/publishment").withRel("publishment"));
        return model;
      }
    };
  }
  
  @Bean
  public RepresentationModelProcessor<RepositoryLinksResource> rootResourceAddition() {
    return new RepresentationModelProcessor<RepositoryLinksResource> () {
      @Override
      public RepositoryLinksResource process(RepositoryLinksResource model) {
        model.add(linkTo(DocDefController.class).withRel("documentdefinitions"));
        model.add(linkTo(MatterController.class).withRel("matter"));
        return model;
      }
    };
  }
}
