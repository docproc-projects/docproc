package net.lueckonline.docproc.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DocumentDefinition {
  private String templatepath;
}
