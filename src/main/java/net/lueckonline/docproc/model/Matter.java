package net.lueckonline.docproc.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Matter {
  private String matterPath;
  //TODO use a real type for the mimetype? Won't use MediaType from Spring because it's contained in the http package
  // and i don't want to use http packages in the Service Layer.
  private String contentType;
  
}
