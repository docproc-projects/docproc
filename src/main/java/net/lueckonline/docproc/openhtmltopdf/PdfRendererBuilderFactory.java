package net.lueckonline.docproc.openhtmltopdf;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

import org.jsoup.Jsoup;
import org.jsoup.helper.W3CDom;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

import com.openhtmltopdf.pdfboxout.PdfRendererBuilder;

/**
 * we use this class as a bean which creates {@link PdfRendererBuilder} instances.
 * even if it's called Builder instances of that class actually do have a state 
 * and we need a new instance for every document
 * 
 * @author thuri
 *
 */
@Component
public class PdfRendererBuilderFactory {
  
  public PdfRendererBuilder createRendererBuilder(InputStream in, OutputStream ou,  String baseUri) throws IOException {
    
    final Document document = Jsoup.parse(in, StandardCharsets.UTF_8.name(), baseUri);
    
    PdfRendererBuilder builder = new PdfRendererBuilder();
    builder.useFastMode();
    builder.withW3cDocument(new W3CDom().fromJsoup(document), baseUri);
    builder.toStream(ou);
    
    return builder;
  }
}
