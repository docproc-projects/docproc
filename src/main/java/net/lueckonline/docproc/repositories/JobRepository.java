package net.lueckonline.docproc.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.PagingAndSortingRepository;

import net.lueckonline.docproc.model.Job;

public interface JobRepository extends PagingAndSortingRepository<Job, String> {
  
  @Transactional
  public List<Job> findByStartedIsNullAndHtmlTemplateUriIsNotNull();
  
}
