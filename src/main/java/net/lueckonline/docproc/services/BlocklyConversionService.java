package net.lueckonline.docproc.services;

import net.lueckonline.docproc.thymeleaf.blockly.Workspace;

public interface BlocklyConversionService {

  /**
   * converts the expression which should contain a Rule into a blockly workspace
   * 
   * @param expression the expression to convert
   * @return a blockly workspace 
   * @throws ConversionError if expression can't be converted
   */
  Workspace convertRule(String expression) throws ConversionError ;

  
  public static class ConversionError extends Exception {
    private static final long serialVersionUID = -2642290267785130640L;

    public ConversionError(String message, Throwable cause) {
      super(message, cause);
    }

    public ConversionError(String message) {
      super(message);
    }
  }
}