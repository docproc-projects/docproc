package net.lueckonline.docproc.services;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import net.lueckonline.docproc.errors.DocProcException;
import net.lueckonline.docproc.model.DocumentDefinition;

public interface DocDefService {
  
  public List<DocumentDefinition> getDocumentDefinitions() throws DocProcException;

  public DocumentDefinition getDocumentDefinitionForPath(String docdefpath) throws FileNotFoundException;
  
  public String readDocumentDefinition(String docdefpath) throws FileNotFoundException, IOException;
  
  public void saveDocumentDefinition(DocumentDefinition eq, String content) throws IOException;
  
}
