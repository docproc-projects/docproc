package net.lueckonline.docproc.services;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;

import net.lueckonline.docproc.model.Job;

/**
 * implementations of this interface MUST always be thread safe
 * They MUST be exposed as beans to the Application Context
 * and SHOULD use Scope {@link ConfigurableBeanFactory#SCOPE_PROTOTYPE} if they
 * own any fields 
 * 
 * @author thuri
 *
 */
public interface JobProcessor {

  public void process(Job job) throws Exception;
}
