package net.lueckonline.docproc.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import net.lueckonline.docproc.errors.DocProcException;
import net.lueckonline.docproc.model.Matter;

public interface MatterService {

  public List<Matter> getMatter() throws DocProcException;
  
  public Matter getMatterForPath(String path) throws IOException;

  public InputStream readMatter(Matter matter) throws IOException;

}
