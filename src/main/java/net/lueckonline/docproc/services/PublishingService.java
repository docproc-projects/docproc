package net.lueckonline.docproc.services;

import net.lueckonline.docproc.errors.DocProcException;
import net.lueckonline.docproc.model.DocumentDefinition;

public interface PublishingService {

  public void publish(DocumentDefinition path) throws DocProcException;
}
