package net.lueckonline.docproc.services;

import java.io.IOException;
import java.io.InputStream;

public interface TemplateService {

  String css(InputStream contentStream, String baseUrl) throws IOException;

  String schema(InputStream contentStream, String baseUrl) throws IOException;

}