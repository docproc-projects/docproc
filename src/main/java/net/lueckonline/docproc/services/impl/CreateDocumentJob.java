package net.lueckonline.docproc.services.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.openhtmltopdf.pdfboxout.PdfRendererBuilder;

import net.lueckonline.docproc.model.Job;
import net.lueckonline.docproc.openhtmltopdf.PdfRendererBuilderFactory;
import net.lueckonline.docproc.services.JobProcessor;
import net.lueckonline.docproc.thymeleaf.JsonNodeToObjectMapper;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CreateDocumentJob implements JobProcessor {
  
  private static final Logger logger = LogManager.getLogger();

  private final Path outdir;
  
  private final ITemplateEngine templateEngine;

  private final ObjectMapper objectMapper;

  private final PdfRendererBuilderFactory rendererFactory;
  
  @Autowired
  public CreateDocumentJob(@Value("${output.dir}") Path outdir, ITemplateEngine templateEngine, ObjectMapper objectMapper, PdfRendererBuilderFactory rendererFactory) throws IOException{
    
    //TODO validate parameters
    this.outdir = outdir;
    Files.createDirectories(outdir);
    this.templateEngine = templateEngine;
    this.objectMapper = objectMapper;
    this.rendererFactory = rendererFactory;
  }
  
  @Override
  public void process(Job job) throws Exception {
    
    logger.atDebug().log("Processing job with id {}", job.getId());
    
    String baseUri = job.getHtmlTemplateUri().toURI().toString();
    
    final JsonNode jsonNode = objectMapper.readTree(job.getData());
    if(jsonNode.getNodeType() != JsonNodeType.OBJECT) {
      throw new Exception(String.format("json data for job %s is invalid. must not be null and an json object", job.getId()));
    }
    
    logger.atDebug().log("Calling templateEngine for job with id {}", job.getId());
    final byte[] htmlBytes;
    //TODO: Pipe the writing and reading to reduce memory consumption?
    try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
      
      Context context = new Context();
      // each direct field/property of the data object is mapped to it's own context variable
      for(var i = jsonNode.fields(); i.hasNext(); ) {
        var jsonNodeField= i.next();
        // the elements of the root object need to be passed to the JsonNodeToObjectMapper
        // which will convert scalar nodes (strings, numbers, booleans) to the appropriate 
        // java types
        // embdedded objects will be returned as JsonNodes and mapped later by the OGNLRuntime
        context.setVariable(jsonNodeField.getKey(), JsonNodeToObjectMapper.mapJsonNode(jsonNodeField.getValue()));
      }
      templateEngine.process(baseUri, context, new OutputStreamWriter(baos, StandardCharsets.UTF_8));

      htmlBytes = baos.toByteArray();
    }
    logger.atTrace().log("Result of template enging for job with id {} : {} ", () -> job.getId(), () -> new String(htmlBytes, StandardCharsets.UTF_8));
    
    Path outputPath = outdir.resolve(job.getId()+".pdf");
    logger.atDebug().log("Start PDF creation in File {}", () -> outputPath.toAbsolutePath().toString());
    try(ByteArrayInputStream bais = new ByteArrayInputStream(htmlBytes); 
        OutputStream fos = Files.newOutputStream(outputPath)) {
      
      PdfRendererBuilder rendererBuilder = rendererFactory.createRendererBuilder(bais, fos, baseUri);
      rendererBuilder.run();
    }
    
    logger.atDebug().log("Finished processing of job with id {}. Wrote file to {}", () -> job.getId(), () -> outputPath.toAbsolutePath().toString());
  }
}
