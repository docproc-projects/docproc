package net.lueckonline.docproc.services.impl;

import static net.lueckonline.docproc.services.impl.ResourceServiceUtils.isFileAccepted;
import static net.lueckonline.docproc.services.impl.ResourceServiceUtils.getValidPathToParent;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import net.lueckonline.docproc.errors.DocProcException;
import net.lueckonline.docproc.model.DocumentDefinition;
import net.lueckonline.docproc.services.DocDefService;

@Service
public class DocDefServiceImpl implements DocDefService {

  @Value("${documentporfolio.docdef.path}")
  Path docDefLibraryPath;
  
  @Override
  public List<DocumentDefinition> getDocumentDefinitions() throws DocProcException {
    
    try (Stream<Path> files = Files.walk(docDefLibraryPath)) {
      return 
          files.filter(f -> isFileAccepted(f))
          .map(f -> docDefLibraryPath.relativize(f).toString())
          .map(s -> DocumentDefinition.builder().templatepath(s).build())
          .collect(Collectors.toList());
    } catch (IOException e) {
      throw new DocProcException("Error while trying to fetch documentdefinitions ", e);
    }
  }
  
  @Override
  public String readDocumentDefinition(String filepath) throws IOException {
    Path validatedPath = getValidPathToParent(this.docDefLibraryPath, filepath);
    return Files.readString(this.docDefLibraryPath.resolve(validatedPath), StandardCharsets.UTF_8);
  }

 @Override
  public void saveDocumentDefinition(DocumentDefinition docdef, String content) throws IOException {
    Path validatedPath = getValidPathToParent(this.docDefLibraryPath, docdef.getTemplatepath());
    // TODO: use the encoding from the HTML file <meta charset="UTF-8">
    Files.writeString(this.docDefLibraryPath.resolve(validatedPath),content, StandardCharsets.UTF_8); 
  }

  @Override
  public DocumentDefinition getDocumentDefinitionForPath(String docdefpath) throws FileNotFoundException {
    // call makePathToResource to check, that it is valid before returning a docdef object with that path
    Path validatedPath = getValidPathToParent(this.docDefLibraryPath, docdefpath);
    return DocumentDefinition.builder()
        .templatepath(validatedPath.toString()).build();
  }
  
}
