package net.lueckonline.docproc.services.impl;

import java.io.IOException;
import java.nio.file.Path;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PushCommand;
import org.eclipse.jgit.api.errors.EmptyCommitException;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.transport.RefSpec;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import net.lueckonline.docproc.errors.DocProcException;
import net.lueckonline.docproc.model.DocumentDefinition;
import net.lueckonline.docproc.services.PublishingService;

@Service
public class JGitPublishingService implements PublishingService{

  private static final Logger logger = LogManager.getLogger();
  
  private final Path docDefLibraryPath;
  
  @Value("${publishment.author.name:docproc}")
  private String authorName;

  @Value("${publishment.author.email:docproc@lueckonline.net}")
  private String authorEmail;

  @Value("${publishment.git.remote:staging}")
  private String pushRemote;
  
  @Value("${publishment.git.commit.message:commited by docproc Application}")
  private String commitMessage;
  
  @Value("${publishment.git.remote.user:null}")
  private String gitUsername;
  
  @Value("${publishment.git.remote.password:null}")
  private String gitPassword;

  public JGitPublishingService(@Value("${documentporfolio.docdef.path}") Path docDefLibraryPath) throws IOException {
    this.docDefLibraryPath = docDefLibraryPath;
  }
  
  @Override
  public void publish(DocumentDefinition docdef) throws DocProcException {
    try(Git git = Git.open(this.docDefLibraryPath.getParent().toFile())) {
      Path validatedPath        = ResourceServiceUtils.getValidPathToParent(docDefLibraryPath, docdef.getTemplatepath());
      Path absoluteResourcePath = this.docDefLibraryPath.resolve(validatedPath);
      String filepath           = this.docDefLibraryPath.getParent().relativize(absoluteResourcePath).toString();
      git.add().addFilepattern(filepath).call();
      git.commit()
        .setAllowEmpty(false)
        .setAuthor(authorName,authorEmail)
        .setMessage(commitMessage)
        .setCommitter(authorName, authorEmail)
        .call();
      
      PushCommand pushCommand = git.push().setRemote(pushRemote).setRefSpecs(new RefSpec("master:master"));
      if(gitUsername != null)
        pushCommand = pushCommand.setCredentialsProvider(new UsernamePasswordCredentialsProvider(gitUsername, gitPassword));
      pushCommand.call();
    } catch(EmptyCommitException e) {
      logger.atError().log("Tried to create an  empty commit for docdef with templatepath {}", () -> docdef.getTemplatepath(), () -> e);
    } catch (IOException | GitAPIException e) {
      throw new DocProcException("Error while trying to publish docdef for templatepath "+docdef.getTemplatepath(), e);
    }
  }

}
