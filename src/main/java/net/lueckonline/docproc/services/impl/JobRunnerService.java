package net.lueckonline.docproc.services.impl;

import java.util.Date;
import java.util.concurrent.ExecutorService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import net.lueckonline.docproc.repositories.JobRepository;
import net.lueckonline.docproc.services.JobProcessor;

@Component
public class JobRunnerService implements ApplicationContextAware{

  private static final Logger logger = LogManager.getLogger();
  
  private final JobRepository jobRepository;
  private final ExecutorService executorService;

  private ApplicationContext applicationContext;

  @Autowired
  public JobRunnerService(JobRepository jobRepository, @Qualifier("jobExecutors") ExecutorService executorService) {

    this.jobRepository = jobRepository;
    this.executorService = executorService;
  }
  
  @Scheduled(cron = "${jobs.interval}")
  public void executeJobs() {
    
    logger.info(() -> "Running JobRunnerService#executeJobs");
    
    jobRepository.findByStartedIsNullAndHtmlTemplateUriIsNotNull().stream()
      .map(j -> new Runnable() {
        @Override
        public void run() {

          j.setStarted(new Date());
          jobRepository.save(j);
          
          try {

            // JobProcessor implementations must always be thread safe and should use prototype
            // scope if they own any fields
            applicationContext.getBean(JobProcessor.class).process(j);
            
            j.setFinished(new Date());
            jobRepository.save(j);
          } catch (Exception e) {
            logger.atError().log("Error while trying to execute job {}", j.getId(), e);
          }
        }
      })
      .forEach(r -> executorService.execute(r));
  }
  
  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    this.applicationContext = applicationContext;
  }
  
}
