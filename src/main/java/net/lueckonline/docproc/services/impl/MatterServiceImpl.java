package net.lueckonline.docproc.services.impl;

import static net.lueckonline.docproc.services.impl.ResourceServiceUtils.getValidPathToParent;
import static net.lueckonline.docproc.services.impl.ResourceServiceUtils.isFileAccepted;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import net.lueckonline.docproc.errors.DocProcException;
import net.lueckonline.docproc.model.Matter;
import net.lueckonline.docproc.services.MatterService;

@Service
public class MatterServiceImpl implements MatterService {

  @Value("${documentporfolio.matter.path}")
  Path matterPath;
  
  @Override
  public List<Matter> getMatter() throws DocProcException {
    try (Stream<Path> files = Files.walk(matterPath)) {
      return 
          files.filter(f -> isFileAccepted(f))
          .map(f -> matterPath.relativize(f).toString())
          .map(s -> Matter.builder().matterPath(s).build())
          .collect(Collectors.toList());
    } catch (IOException e) {
      throw new DocProcException("Error while trying to fetch matter ", e);
    }
  }

  @Override
  public Matter getMatterForPath(String path) throws IOException {
    
    final Path validatedPath = getValidPathToParent(matterPath, path);
    
    String contentType = Files.probeContentType(this.matterPath.resolve(validatedPath));

    final MediaType mediaType;
    if(contentType == null) {
      if(validatedPath.toString().endsWith(".properties")) mediaType = MediaType.valueOf("text/plain;charset=UTF-8");
      else 
        mediaType = null;
    } else {
      MediaType guess = MediaType.valueOf(contentType);
      if(MediaType.valueOf("text/*").includes(guess) && guess.getCharset() == null) {
        guess = MediaType.valueOf(guess.getType()+"/"+guess.getSubtype()+";charset=UTF-8");
      }
      mediaType = guess;
    }
    
    return Matter.builder()
        .matterPath(validatedPath.toString())
        .contentType(mediaType != null ? mediaType.toString() : null)
        .build();
  }

  @Override
  public InputStream readMatter(Matter matter) throws IOException {
    Path validatedPath = getValidPathToParent(matterPath, matter.getMatterPath());
    return  Files.newInputStream(this.matterPath.resolve(validatedPath));
  }
}
