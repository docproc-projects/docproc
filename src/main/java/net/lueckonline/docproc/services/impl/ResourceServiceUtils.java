package net.lueckonline.docproc.services.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class ResourceServiceUtils {

  private static final Logger logger = LogManager.getLogger();
  
  static final boolean isFileAccepted(Path file) {
    try {
      return Files.isReadable(file) && Files.isRegularFile(file) && !Files.isHidden(file);
    } catch (IOException e) {
      logger.atError().log("Error while tryint to checking acceptance of file {}", file.toString(), e);
      return false;
    }
  }

  static Path getValidPathToParent(Path parent, String filepath) throws FileNotFoundException {
    final String normalizedPath = StringUtils.trimToEmpty(filepath);
    if(normalizedPath.contains("..") || normalizedPath.isEmpty()) {
      throw new IllegalArgumentException("Invalid path "+filepath);
    }
    
    if(Paths.get(filepath).isAbsolute())
      throw new IllegalArgumentException("Absolute paths not allowed");
    
    Path pathToFile = parent.resolve(filepath);
    if(!isFileAccepted(pathToFile)) throw new FileNotFoundException("Could not find requested file");
    
    return parent.relativize(pathToFile);
  }
}
