package net.lueckonline.docproc.services.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import net.lueckonline.docproc.services.TemplateService;

@Service
public class TemplateServiceImpl implements TemplateService {

    @Override
    public String css(InputStream contentStream, String baseUrl) throws IOException {
      return Jsoup.parse(contentStream, null, baseUrl).select("style").html();
    }
    
    @Override
    public String schema(InputStream contentStream, String baseUrl) throws IOException {
      
      var document = Jsoup.parse(contentStream, null, baseUrl);

      var schemaReferences = new ArrayList<String>();
      
      var docSchema = getSchemaFromDocument(document);
      if(docSchema != null) schemaReferences.add(docSchema);
      
      document
          .select("div[data-matter-link]").stream()
          .map(e -> "{ \"$ref\" : \"" + e.attr("data-matter-link") + "?type=schema\"}")
          .distinct() // this is very important for the composer app when it comes to creating a complete schema because it makes it way easier
          .forEach(schemaReferences::add);
      
      if(schemaReferences.size() > 1) {
        return 
            "{"
          + "  \"allOf\" : ["
          + String.join(",", schemaReferences)
          + "  ]"
          + "}";
      } else if(schemaReferences.size() == 1) {
        return schemaReferences.get(0);
      } else {
        return "{}";
      }
    }

    private String getSchemaFromDocument(Document doc) {
      Elements schemaElements = doc.select("script.schema[type = application/json]");
      if(schemaElements.size() > 1) throw new IllegalStateException("Expected at most one schema element in document but found "+schemaElements.size());
      if(schemaElements.size() == 0) return null;
      
      return schemaElements.html();
    }
}
