package net.lueckonline.docproc.services.impl;

import java.net.URISyntaxException;
import java.net.URL;
import java.text.MessageFormat;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.cfg.ValidationConfiguration;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.load.configuration.LoadingConfiguration;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

import net.lueckonline.docproc.services.validation.ValidationReport;
import net.lueckonline.docproc.services.validation.ValidationService;

@Service
public class ValidationServiceImpl implements ValidationService {

  private static final Logger logger = LogManager.getLogger();
  
  private final JsonSchemaFactory schemaFactory;
  
  public ValidationServiceImpl(@Value("${json.schema.caches.enabled:true}") boolean enableCaches) {
    var loadingConfigBuilder = LoadingConfiguration.newBuilder();
    var validationConfig = ValidationConfiguration.newBuilder();
    if(enableCaches == false) {
      loadingConfigBuilder.setCacheSize(0);
      validationConfig.setCacheSize(0);
    }
    schemaFactory = JsonSchemaFactory.newBuilder()
        .setLoadingConfiguration(loadingConfigBuilder.freeze())
        .setValidationConfiguration(validationConfig.freeze())
        .freeze(); 
  }
  
  @Override
  public ValidationReport validate(URL templateUri, JsonNode data) {
    try {
      
      var schemaUri = UriComponentsBuilder.fromUri(templateUri.toURI()).queryParam("type", "schema").toUriString();
      var schema = schemaFactory.getJsonSchema(schemaUri);
      
      return new ProcessingReportWrapper(schema.validate(data, true));
      
    } catch (URISyntaxException e) {
      var errorMsg = MessageFormat.format("Error while trying to create uri to schema for docdef with URI {0}", templateUri.toString());
      logger.atError().log(errorMsg, e);
      return new ExceptionValidationReport(errorMsg);
    } catch (ProcessingException e) {
      var errorMsg = "Error while trying to load schema for docdef with URI "+templateUri.toString();
      logger.atError().log(errorMsg, e);
      return new ExceptionValidationReport(errorMsg);
    }
  }
  
  private static final class ProcessingReportWrapper implements ValidationReport {

    private final ProcessingReport wrappedReport;

    public ProcessingReportWrapper(ProcessingReport wrappedReport) {
      this.wrappedReport = wrappedReport;
    }

    @Override
    public boolean isSuccess() {
      return wrappedReport.isSuccess();
    }

    @Override
    public String toString() {
      return wrappedReport.toString();
    }
    
  }
  
  private static final class ExceptionValidationReport implements ValidationReport{

    private final String errorMessage;

    public ExceptionValidationReport(String errorMessage) {
      this.errorMessage = errorMessage;
    }
    
    @Override
    public boolean isSuccess() {
      return false;
    }
    
    @Override
    public String toString() {
      return errorMessage;
    }
  }

}
