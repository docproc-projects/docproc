package net.lueckonline.docproc.services.validation;

public interface ValidationReport {

  public boolean isSuccess();
  
  public String toString();
  
}
