package net.lueckonline.docproc.services.validation;

import java.net.URL;

import com.fasterxml.jackson.databind.JsonNode;

public interface ValidationService {

  public ValidationReport validate(URL templateUri, JsonNode data);
  
}
