package net.lueckonline.docproc.thymeleaf;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class JsonNodeToObjectMapper {
  
  public static Object mapJsonNode(JsonNode targetElement) {
    switch(targetElement.getNodeType()) {
      case OBJECT: {
        ObjectNode objTarget = (ObjectNode) targetElement;
        var fieldIterator = objTarget.fields();
        var resultMap = new HashMap<String, Object>();
        while(fieldIterator.hasNext()) {
          var field = fieldIterator.next();
          if(field.getValue().getNodeType() == JsonNodeType.OBJECT) 
            resultMap.put(field.getKey(), field.getValue());
          else
            resultMap.put(field.getKey(), mapJsonNode(field.getValue()));
        }
        return resultMap;
      }
      case BOOLEAN: return targetElement.asBoolean();
      case STRING: {
        final String val = targetElement.asText();
        try {
          //currently no better idea the to try conversion to date first and if it fails
          //return the text. Probably we could use the OGNL context for a better guess
          return Date.from(Instant.from(DateTimeFormatter.ISO_INSTANT.parse(val)));
        } catch (DateTimeParseException e) {
          return val;
        }
      }
      case NUMBER: return targetElement.isInt() 
                    ? targetElement.asInt() : targetElement.isLong() 
                    ? targetElement.asLong() : targetElement.isDouble() 
                    ? targetElement.asDouble() 
                    : targetElement.asText(); // this may look strange but perhaps OGNL or thymeleaf have a better idea how to convert the value; 
      case ARRAY: {
        ArrayNode arrayNode = (ArrayNode)targetElement;
        var iterator = arrayNode.elements();
        var result = new ArrayList<Object>(arrayNode.size());
        while(iterator.hasNext()) {
          // we need to call mapJsonNode here because the engine won't do it and use toString() on each element. 
          // that would result in wrapping quote signs for TextNodes. So instead of printing <Hello World> it would bring <"Hello World>
          // where the < and > sign are not printed only here to show the boundaries of printed text 
          result.add(mapJsonNode(iterator.next()));
        }
        return result;
      }
      default: return targetElement;
    }
  }
}
