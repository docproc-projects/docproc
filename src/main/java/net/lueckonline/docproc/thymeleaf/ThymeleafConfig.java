package net.lueckonline.docproc.thymeleaf;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.templateresolver.UrlTemplateResolver;

import com.fasterxml.jackson.databind.JsonNode;

import ognl.OgnlContext;
import ognl.OgnlException;
import ognl.OgnlRuntime;
import ognl.PropertyAccessor;

@Configuration
public class ThymeleafConfig {

  @Value("${documentportfolio.docdef.cache:300000}")
  Long cacheTTLMs;
  
  @Bean
  public TemplateEngine buildTemplateEngine() {
    
    TemplateEngine templateEngine = new TemplateEngine();
    
    UrlTemplateResolver templateResolver = new UrlTemplateResolver();
    templateResolver.setTemplateMode("HTML");
    templateResolver.setCharacterEncoding("UTF-8");
    if(cacheTTLMs > 0) {
      templateResolver.setCacheable(true);
      templateResolver.setCacheTTLMs(cacheTTLMs);
    } else {
      templateResolver.setCacheable(false);
    }
    templateEngine.setTemplateResolver(templateResolver);
    
    /* 
     * even if it has nothing to do with the TemplateEngine Bean we 
     * configure the OgnlRuntime here.
     * This enables a mapping of OGNL syntax to the generic JsonNode Objects parsed by jackson. 
     */
    OgnlRuntime.setPropertyAccessor(JsonNode.class, new PropertyAccessor() {
      @Override
      public void setProperty(@SuppressWarnings("rawtypes") java.util.Map context, Object target, Object name, Object value) throws OgnlException {
        throw new UnsupportedOperationException("setting values on the json object is not supported");
      }
      
      @Override
      public String getSourceSetter(OgnlContext context, Object target, Object index) {
        return null;
      }
      
      @Override
      public String getSourceAccessor(OgnlContext context, Object target, Object index) {
        return null;
      }
      
      @Override
      public Object getProperty(@SuppressWarnings("rawtypes") java.util.Map context, Object target, Object name) throws OgnlException {
        final JsonNode targetElement = ((JsonNode) target).get((String) name);
        if(targetElement == null) return null;
        return JsonNodeToObjectMapper.mapJsonNode(targetElement);
      }
    });
    
    return templateEngine;
  }
}
