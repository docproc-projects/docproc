package net.lueckonline.docproc.thymeleaf.blockly;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

interface ExportableBlock {

  /**
   * method to create the XML elements for the block and append it
   * to the children of the passed argument.
   * 
   * @param parent
   * @return the &lt;block&gt; Element that was created for the instance
   */
  public Element toDOM(Node parent);

}