package net.lueckonline.docproc.thymeleaf.blockly;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder
@AllArgsConstructor
class LogicComparisonBlock implements OperandBlock {

  private static final String BLOCKLY_TYPE = "logic_compare_stackable";
  
  static enum Operation {
    EQ,
    NEQ,
    GT,
    GE,
    LT,
    LE,
    CONTAINS
  }
  
  private final Operation operation;
  
  private ValueBlock leftValueBlock;
  private ValueBlock rightValueBlock;
  
  public LogicComparisonBlock(Operation operation) {
    super();
    if(operation == null) throw new IllegalArgumentException("operation must not be null");
    this.operation = operation;
  }

  @Override
  public Element toDOM(Node parent) {
    
    var doc = parent.getOwnerDocument();
    
    var blockElement = doc.createElementNS(Workspace.BLOCKLY_NAMESPACE, "block");
    parent.appendChild(blockElement);
    blockElement.setAttribute("type", BLOCKLY_TYPE);
    
    var fieldElement = doc.createElementNS(Workspace.BLOCKLY_NAMESPACE,"field");
    blockElement.appendChild(fieldElement);
    fieldElement.setAttribute("name", "OP");
    fieldElement.setTextContent(operation.name());

    buildValueBlock(blockElement, "LEFT", leftValueBlock);
    buildValueBlock(blockElement, "RIGHT", rightValueBlock);
    
    return blockElement;
  }
  
  private void buildValueBlock(Node parent, String name, ExportableBlock block) {
    
    if(block == null) return;
    
    var valueElement = parent.getOwnerDocument().createElementNS(Workspace.BLOCKLY_NAMESPACE,"value");
    parent.appendChild(valueElement);
    valueElement.setAttribute("name", name);
    
    block.toDOM(valueElement);
  }

  public ValueBlock getLeftValueBlock() {
    return leftValueBlock;
  }

  public void setLeftValueBlock(ValueBlock leftValueBlock) {
    this.leftValueBlock = leftValueBlock;
  }

  public ValueBlock getRightValueBlock() {
    return rightValueBlock;
  }

  public void setRightValueBlock(ValueBlock rightValueBlock) {
    this.rightValueBlock = rightValueBlock;
  }

  public Operation getOperation() {
    return operation;
  }
}
