package net.lueckonline.docproc.thymeleaf.blockly;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import lombok.Builder;

@Builder
class LogicOperationBlock implements OperandBlock {

  private static final String BLOCKLY_TYPE = "logic_operation_stackable";
  
  static enum Operation {
    AND,
    OR
  }
  
  private final List<OperandBlock> operands = new ArrayList<>();

  private final Operation operation;
  
  public LogicOperationBlock(Operation operation) {
    super();
    if(operation == null) throw new IllegalArgumentException("operation must not be null");
    this.operation = operation;
  }

  @Override
  public Element toDOM(Node parent) {
    
    var doc = parent.getOwnerDocument();
    
    var blockElement = doc.createElementNS(Workspace.BLOCKLY_NAMESPACE,"block");
    parent.appendChild(blockElement);
    
    blockElement.setAttribute("type", BLOCKLY_TYPE);
    
    Element fieldElement = doc.createElementNS(Workspace.BLOCKLY_NAMESPACE,"field");
    blockElement.appendChild(fieldElement);
    fieldElement.setAttribute("name", "OP");
    fieldElement.setTextContent(this.operation.name());
    
    if(operands.size() > 0) {
      final Element statementElement = doc.createElementNS(Workspace.BLOCKLY_NAMESPACE,"statement");
      blockElement.appendChild(statementElement);
      statementElement.setAttribute("name", "CONDITIONS");
      
      Element parentElement = statementElement;
      for(var it = operands.iterator(); it.hasNext(); ) {
        
        var operandNode = it.next().toDOM(parentElement);
        if(it.hasNext()) {
          var nextElement = doc.createElementNS(Workspace.BLOCKLY_NAMESPACE,"next");
          operandNode.appendChild(nextElement);
          parentElement = nextElement; 
        }
      }
    }
    
    return blockElement;
  }
  
  public LogicOperationBlock addOperand(OperandBlock condition) {
    this.operands.add(condition);
    return this;
  }

  public Operation getOperation() {
    return operation;
  }

  public List<OperandBlock> getOperands() {
    return operands;
  }
  
}
