package net.lueckonline.docproc.thymeleaf.blockly;

/**
 * marker interface for elements that can be used as operand in {@link LogicOperationBlock}
 * 
 * @author thuri
 *
 */
interface OperandBlock extends ExportableBlock {

}
