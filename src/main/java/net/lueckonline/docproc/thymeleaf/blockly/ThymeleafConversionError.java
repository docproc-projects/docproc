package net.lueckonline.docproc.thymeleaf.blockly;

import org.thymeleaf.standard.expression.IStandardExpression;

import net.lueckonline.docproc.services.BlocklyConversionService.ConversionError;

public class ThymeleafConversionError extends ConversionError {

  private static final long serialVersionUID = -1753347945588320735L;

  private final String errorExpression;
  
  private ThymeleafConversionError(String message, String errorExpression) {
    super(message);
    this.errorExpression = errorExpression;
  }
  
  private ThymeleafConversionError(String message, Throwable cause, String errorExpression) {
    super(message, cause);
    this.errorExpression = errorExpression;
  }
  
  public static ThymeleafConversionError build(IStandardExpression errorExpression, Throwable cause) {
    var errMsg = "Error while trying to convert Expression \""+errorExpression.getStringRepresentation()+"\" to Blockly";
    
    var errorExpressionStr = errorExpression.getStringRepresentation();
    
    if(cause == null) 
      return new ThymeleafConversionError(errMsg, errorExpressionStr);
    else 
      return new ThymeleafConversionError(errMsg, cause, errorExpressionStr);
  }

  public String getErrorExpression() {
    return errorExpression;
  }
  
}
