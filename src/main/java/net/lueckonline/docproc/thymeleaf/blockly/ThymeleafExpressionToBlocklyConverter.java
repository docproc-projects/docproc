package net.lueckonline.docproc.thymeleaf.blockly;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.thymeleaf.IEngineConfiguration;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.context.IEngineContext;
import org.thymeleaf.standard.expression.AndExpression;
import org.thymeleaf.standard.expression.BinaryOperationExpression;
import org.thymeleaf.standard.expression.BooleanTokenExpression;
import org.thymeleaf.standard.expression.EqualsExpression;
import org.thymeleaf.standard.expression.GreaterOrEqualToExpression;
import org.thymeleaf.standard.expression.GreaterThanExpression;
import org.thymeleaf.standard.expression.IStandardExpression;
import org.thymeleaf.standard.expression.IStandardExpressionParser;
import org.thymeleaf.standard.expression.LessOrEqualToExpression;
import org.thymeleaf.standard.expression.LessThanExpression;
import org.thymeleaf.standard.expression.MinusExpression;
import org.thymeleaf.standard.expression.NotEqualsExpression;
import org.thymeleaf.standard.expression.NumberTokenExpression;
import org.thymeleaf.standard.expression.OrExpression;
import org.thymeleaf.standard.expression.StandardExpressions;
import org.thymeleaf.standard.expression.TextLiteralExpression;
import org.thymeleaf.standard.expression.VariableExpression;

import net.lueckonline.docproc.services.BlocklyConversionService;
import net.lueckonline.docproc.thymeleaf.blockly.ValueBlock.Type;
import ognl.ASTChain;
import ognl.ASTConst;
import ognl.ASTMethod;
import ognl.ASTProperty;
import ognl.ASTVarRef;
import ognl.Node;
import ognl.Ognl;

@Service
public class ThymeleafExpressionToBlocklyConverter implements BlocklyConversionService {
  
  private IStandardExpressionParser expressionParser;
  /**
   * <p>expressionContext created during initialization of the class</p>
   * <p>because we only want to parse the expressions there may not be the need to change the context for each expression.
   * That should only be necessary when we want to evaluate the expression</p> 
   * 
   */
  private IEngineContext expressionContext;

  public ThymeleafExpressionToBlocklyConverter(TemplateEngine templateEngine) {
    
    IEngineConfiguration engineConfig = templateEngine.getConfiguration();
    expressionParser = StandardExpressions.getExpressionParser(engineConfig);
    
    expressionContext = engineConfig.getEngineContextFactory().createEngineContext(engineConfig, null, null, new Context());
  }
  
  @Override
  public Workspace convertRule(String expression) throws BlocklyConversionService.ConversionError {
    
    var expressionAST = expressionParser.parseExpression(expressionContext, expression);
    
    // see comments on the Visitor implementation!
    var visitor = new ThyemeleafExpressionVisitor();
    var rootBlock = visitor.dispatch(expressionAST);
    
    var workspace = new Workspace();
    workspace.setRootBlock(rootBlock);
    
    return workspace;
  }
  
  /**
   * <p>as long as the thymeleaf {@link IStandardExpression} interface doesn't support the visitor pattern 
   * or some other possibiblity to traverse the AST we use a special implementation of the visitor pattern.</p> 
   * <p>in this implementation the visitor itself has a dispatch method that is a large if-elseif-else block 
   * and decides which visit method to call. This is necessary because the overloading and late binding doesn't work
   * here.</p>
   * @author thuri
   *
   */
  static final class ThyemeleafExpressionVisitor {

    /**
     * <p>the method takes the binary operation instance given as argument and builds a {@link LogicOperationBlock} for it</p>
     * <p>the method flattens all operations that are children of binaryOp and have the same expression type. see {@link #findAllOperands(BinaryOperationExpression)} </p>
     * 
     * @param binaryOp the expression in the thymeleaf AST. Should be either {@link AndExpression} or {@link OrExpression}. the behaviour is not tested for other operations
     * @param blocklyOp the type of the operation to be used in the returned {@link LogicOperationBlock}
     * @return a {@link LogicOperationBlock} created for the {@link BinaryOperationExpression}
     * @throws ThymeleafConversionError when the operands are neither comparisions nor other logical operations
     */
    private LogicOperationBlock binOpExpressionToLogicOperationBlock(BinaryOperationExpression binaryOp, LogicOperationBlock.Operation blocklyOp) throws ThymeleafConversionError {
      var block = LogicOperationBlock.builder().operation(blocklyOp).build();
      
      List<IStandardExpression> operands = findAllOperands(binaryOp);
      
      for(var op : operands) {
        try {
          block.addOperand((OperandBlock) this.dispatch(op));
        } catch (ClassCastException e ) {
          throw ThymeleafConversionError.build(op, e);
        }
      }
      
      return block;
    }
    
    /**
     * <p>this method flattens adjacent logical operations like AND and OR that are adjacent to the parameter</p>
     * <p>So if the binaryOp is an {@link AndExpression} and has children that are also {@link AndExpression}s the resulting {@link LogicOperationBlock} won't
     * have a nested {@link LogicOperationBlock} for them. Only the descendands of the nested {@link AndExpression}s which are not {@link AndExpression}s themselves
     * are added as operand to the returned {@link LogicOperationBlock} </p>
     * <p> Example: the expression <pre>1 == 1 and 2 >= 2 and 3 <= 3 or true != false</pre> would result in an AST like the following
     * <pre>
     *                OR
     *               /   \
     *             AND   !=
     *            /   \  
     *          ==    AND
     *               /   \
     *              >=   <=
     * </pre>
     * 
     * <p>when called for the root node the method will return a list containing only the direct children of OR.</p>
     * <p>but when called for the highest AND node it will return a list like this [ ==, >=, <= ] where the symbols stand for the apprioate {@link IStandardExpression} type 
     * @param binaryOp the node in the AST for which to find the operands in it's descendants 
     * @return a list of operands that can be chained with the operation described by binaryOp 
     */
    private List<IStandardExpression> findAllOperands(BinaryOperationExpression binaryOp) {
      
      List<IStandardExpression> operands = new ArrayList<>();  

      List<Supplier<IStandardExpression>> suppliers = new ArrayList<Supplier<IStandardExpression>>();
      suppliers.add(binaryOp::getLeft);
      suppliers.add(binaryOp::getRight);
      
      for(var s : suppliers) {
        IStandardExpression childExpression = s.get();
        if(childExpression.getClass() == binaryOp.getClass()) {
          operands.addAll(findAllOperands((BinaryOperationExpression)childExpression));
        } else {
          operands.add(childExpression);
        }
      }

      return operands;

    }

    /**
     * <p>method to be used for {@link BinaryOperationExpression} that are acually comparison expressions like ==, !=, >= etc.</p> 
     * <p>use {@link #binOpExpressionToLogicOperationBlock(BinaryOperationExpression, net.lueckonline.docproc.thymeleaf.blockly.LogicComparisonBlock.Operation)} 
     * for {@link AndExpression} and {@link OrExpression}</p>
     * 
     * @param binaryOp binary Operation to build blocky block of Type {@link LogicComparisonBlock}
     * @param blocklyOp the operation to use in the returned {@link LogicComparisonBlock}. Should be equivalent to the operation of the {@link BinaryOperationExpression}
     * @return a {@link LogicComparisonBlock} created from the binaryOp and it's children
     * @throws ThymeleafConversionError if the children of the binaryOp are not convertible to {@link ValueBlock}
     */
    private LogicComparisonBlock binOpExpressionToLogicOperationBlock(BinaryOperationExpression binaryOp, LogicComparisonBlock.Operation blocklyOp) throws ThymeleafConversionError {
      var block = LogicComparisonBlock.builder().operation(blocklyOp).build();
      
      try {
        block.setLeftValueBlock((ValueBlock) this.dispatch(binaryOp.getLeft()));
      } catch (ClassCastException e ) {
        throw ThymeleafConversionError.build(binaryOp.getLeft(), e);
      }
      
      try {
        block.setRightValueBlock((ValueBlock) this.dispatch(binaryOp.getRight()));
      } catch (ClassCastException e ) {
        throw ThymeleafConversionError.build(binaryOp.getRight(), e);
      }
      
      return block;
    }
    
    ExportableBlock visit(AndExpression expression) throws ThymeleafConversionError {
      return this.binOpExpressionToLogicOperationBlock(expression, LogicOperationBlock.Operation.AND);
    }
    
    ExportableBlock visit(OrExpression expression) throws ThymeleafConversionError {
      return this.binOpExpressionToLogicOperationBlock(expression, LogicOperationBlock.Operation.OR);
    }

    ExportableBlock visit(LessOrEqualToExpression expression) throws ThymeleafConversionError {
      return binOpExpressionToLogicOperationBlock(expression, LogicComparisonBlock.Operation.LE);
    }

    ExportableBlock visit(LessThanExpression expression) throws ThymeleafConversionError {
      return binOpExpressionToLogicOperationBlock(expression, LogicComparisonBlock.Operation.LT);
    }

    ExportableBlock visit(GreaterOrEqualToExpression expression) throws ThymeleafConversionError {
      return binOpExpressionToLogicOperationBlock(expression, LogicComparisonBlock.Operation.GE);
    }

    ExportableBlock visit(GreaterThanExpression expression) throws ThymeleafConversionError {
      return binOpExpressionToLogicOperationBlock(expression, LogicComparisonBlock.Operation.GT);
    }

    ExportableBlock visit(NotEqualsExpression expression) throws ThymeleafConversionError {
      return binOpExpressionToLogicOperationBlock(expression, LogicComparisonBlock.Operation.NEQ);
    }

    ExportableBlock visit(EqualsExpression expression) throws ThymeleafConversionError {
      return binOpExpressionToLogicOperationBlock(expression, LogicComparisonBlock.Operation.EQ);
    }
    
    ExportableBlock visit(TextLiteralExpression expression) {
      return ValueBlock.builder().type(Type.STRING).value(expression.getValue().getValue()).build();
    }
    
    ExportableBlock visit(BooleanTokenExpression expression) {
      var booleanValue =  ((Boolean)expression.getValue());
      return ValueBlock.builder().type(Type.BOOLEAN).value(booleanValue ? "TRUE" : "FALSE").build();
    }
    
    ExportableBlock visit(NumberTokenExpression expression) {
      return ValueBlock.builder().type(Type.NUMBER).value(expression.getStringRepresentation()).build();
    }
    
    ExportableBlock visit(MinusExpression expression) {
      return ValueBlock.builder().type(Type.NUMBER).value(expression.getStringRepresentation()).build();
    }
    
    /**
     * variable expressions are actually converted by the {@link OgnlVisitor}
     * @param expression
     * @return
     * @throws ThymeleafConversionError
     */
    ExportableBlock visit(VariableExpression expression) throws ThymeleafConversionError {
      try {
        return new OgnlVisitor().dispatch((Node) Ognl.parseExpression(expression.getExpression()));
      } catch (Exception e) { // catch all exceptions especially NotImplementedExceptions thrown by the OgnlVisitor
        throw ThymeleafConversionError.build(expression, e);
      }
    }

    public ExportableBlock dispatch(IStandardExpression expression) throws ThymeleafConversionError {
      
      if (expression instanceof AndExpression) {
        return this.visit((AndExpression) expression);
      } else if (expression instanceof OrExpression) {
        return this.visit((OrExpression) expression);
      } else if (expression instanceof EqualsExpression) {
        return this.visit((EqualsExpression) expression);
      } else if (expression instanceof NotEqualsExpression) {
        return this.visit((NotEqualsExpression) expression);
      } else if (expression instanceof GreaterThanExpression) {
        return this.visit((GreaterThanExpression) expression);
      } else if (expression instanceof GreaterOrEqualToExpression) {
        return this.visit((GreaterOrEqualToExpression) expression);
      } else if (expression instanceof LessThanExpression) {
        return this.visit((LessThanExpression) expression);
      } else if (expression instanceof LessOrEqualToExpression) {
        return this.visit((LessOrEqualToExpression) expression);
      } else if (expression instanceof VariableExpression) {
        return this.visit((VariableExpression) expression);
      } else if (expression instanceof TextLiteralExpression) {
        return this.visit((TextLiteralExpression) expression);
      } else if (expression instanceof BooleanTokenExpression) {
        return this.visit((BooleanTokenExpression) expression);
      } else if (expression instanceof NumberTokenExpression) {
        return this.visit((NumberTokenExpression) expression);
      } else if (expression instanceof MinusExpression) {
        return this.visit((MinusExpression) expression);
      } else {  
        throw ThymeleafConversionError.build(expression, new NotImplementedException("no handler implemented for "+expression.getClass().getName()));
      }
    }
  }
  
  /**
   * <p>this visitor is used for all {@link VariableExpression} that have been found in the AST parsed from the
   * complete expression/p>
   * <p>the content of the {@link VariableExpression} should be parsed with Ognl and the result is to be passed to the {@link OgnlVisitor#dispatch(Node)}
   * method.</p>
   * <p>the {@link OgnlVisitor#dispatch(Node)} method is necessary because the OGNL tree doesn't support the visitor pattern</p>
   * @author thuri
   *
   */
  static class OgnlVisitor {

    /**
     * <p>the method converts a ASTProperty to a {@link ValueBlock} instance</p>
     * <p>but it must only be called if it's sure, that it's a scalar property and NOT a part of an {@link ASTChain}.
     * scalar properties are just strings like 'foobar' without any dot within them. Expressions like 'foobar.test' MUST be handled by 
     * {@link #visit(ASTChain)}</p>
     * @param node a node whose parent IS NOT a chain
     * @return a value block with type {@link Type#VARIABLE} and the name of the variable in the value member
     */
    private ExportableBlock visit(ASTProperty node) {
      // the expression was something like "foobar"
      return ValueBlock.builder().type(Type.VARIABLE).value(node.toString()).build();
    }

    /**
     * <p> converts an {@link ASTChain} to
     * <ul>
     *  <li> a {@link ValueBlock} if it represents a property chain like 'foobar.test.bla'</li>
     *  <li> a {@link LogicComparisonBlock} if it represents a call to the 'contains' method of either the 
     *       <code>#strings</code> or the <code>#lists</code> variable 
     *  </ul>
     * </p>
     * <p> other expressions are currently not supported and result in a {@link NotImplementedException}
     * </p>
     * @param chain the chain to convert
     * @return either {@link ValueBlock} or {@link LogicComparisonBlock} (see above)
     * @throws NotImplementedException if ASTChain could not be converted because of not supported syntax
     */
    private ExportableBlock visit(ASTChain chain) {
      /*
       * ognl.ASTChain::#lists.contains(cases.supplements, "TCK007")
       *   ognl.ASTVarRef::#lists
       *   ognl.ASTMethod::contains(cases.supplements, "TCK007")
       *    ognl.ASTChain::cases.supplements
       *     ognl.ASTProperty::cases
       *      ognl.ASTConst::"cases"
       *     ognl.ASTProperty::supplements
       *      ognl.ASTConst::"supplements"
       *    ognl.ASTConst::"TCK007"
       * 
       */
      
      // the expression was something like "foobar.test"
      if(chain.jjtGetChild(0) instanceof ASTProperty) {

        // we CAN NOT use the visit method for the ASTProperty here because we need to call 
        // toString on the ASTChain Object to get the complete variable name!!!
        
        return ValueBlock.builder()
            .type(Type.VARIABLE)
            .value(chain.toString()) // CAREFUL here: We need to call toString on the chain object to get the complete expression like 'foobar.test'
            .build();
      }
       // we make it "easy" for now and only support lists.contains and strings.contains
      else if(chain.jjtGetChild(0) instanceof ASTVarRef
           &&  StringUtils.equalsAny(chain.jjtGetChild(0).toString(), "#lists", "#strings")
           && chain.jjtGetChild(1) instanceof ASTMethod
           && "contains".equals(((ASTMethod)chain.jjtGetChild(1)).getMethodName()))
      {
          var method = (ASTMethod) chain.jjtGetChild(1);
          
          var valueBlockLeft  = method.jjtGetNumChildren() >= 1 ? (ValueBlock) dispatch(method.jjtGetChild(0)) : null;
          var valueBlockRight = method.jjtGetNumChildren() == 2 ? (ValueBlock) dispatch(method.jjtGetChild(1)) : null;
          
          return LogicComparisonBlock.builder()
            .operation(LogicComparisonBlock.Operation.CONTAINS)
            .leftValueBlock(valueBlockLeft)
            .rightValueBlock(valueBlockRight)
            .build();
      }
      throw new NotImplementedException("Handling of OGNL Expression "+chain.toString()+ " currently not supported");
    }
    
    /**
     * <p> converts the {@link ASTConst} parameter to a {@link ValueBlock}</p>
     * 
     * @param node the node to convert
     * @return the ValueBlock converted from the parameter
     * @throws NotImplementedException if the ASTConst has a value of a nun supported type
     */
    private ValueBlock visit(ASTConst node) {

      var valueBlock = ValueBlock.builder();

      // OGNL actually parses the value into the correct type.
      // which allows the if-else construct below to check for the type and 
      // use that for the type in the ValueBlock
      var value = node.getValue(); 
      
      if (value.getClass().isAssignableFrom(String.class)) {
        valueBlock.type(Type.STRING).value(value.toString());
      } else if (Number.class.isAssignableFrom(value.getClass())) {
        valueBlock.type(Type.NUMBER).value(node.toString());
      } else if (node.getValue().getClass().isAssignableFrom(Boolean.class)) {
        var booleanValue = (Boolean) node.getValue();
        valueBlock.type(Type.BOOLEAN).value(booleanValue ? "TRUE" : "FALSE");
      } else {
        throw new NotImplementedException("no handling definied for "+value.getClass());
      }
      
      return valueBlock.build();
    }
    
    /**
     * dispatcher method that is needed because the OGNL clases don't support the visitor pattern
     * @param node
     * @return
     */
    ExportableBlock dispatch(Node node) {
      if (node instanceof ASTChain) { 
        return visit((ASTChain) node);
      } else if (node instanceof ASTProperty) {
        return visit((ASTProperty) node);
      } else if (node instanceof ASTConst) {
        return visit((ASTConst) node);
      } else {
        throw new NotImplementedException("Handling of ognl expression of type "+node.getClass().getName()+ " not implemented");
      }
    }
  }
}
