package net.lueckonline.docproc.thymeleaf.blockly;

import org.apache.commons.lang3.NotImplementedException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import lombok.Builder;

@Builder
class ValueBlock implements ExportableBlock {

  static enum Type {
    VARIABLE,
    BOOLEAN,
    STRING,
    NUMBER
  }
  
  private final Type type;
  private final String value;
  
  public ValueBlock(Type type, String value) {
    super();
    if(type == null) throw new IllegalArgumentException("type must not be null");
    this.type = type;
    this.value = value;
  }

  @Override
  public Element toDOM(Node parent) {
    
    var doc = parent.getOwnerDocument();
    
    final String blockType;
    final String fieldName;
    switch(type) {
      case VARIABLE: blockType = "rule_input_data"; fieldName = "INPUTDATA"; break;
      case BOOLEAN:  blockType = "logic_boolean"; fieldName = "BOOL"; break;
      case NUMBER :  blockType = "math_number"; fieldName = "NUM"; break;
      case STRING :  blockType = "text"; fieldName = "TEXT"; break;
      default     :  throw new NotImplementedException("no handling for type "+type+ " implemented");
    }
    
    var blockElement = doc.createElementNS(Workspace.BLOCKLY_NAMESPACE,"block");
    parent.appendChild(blockElement);
    blockElement.setAttribute("type", blockType);
    
    var fieldElement = doc.createElementNS(Workspace.BLOCKLY_NAMESPACE,"field");
    blockElement.appendChild(fieldElement);
    fieldElement.setAttribute("name", fieldName);
    fieldElement.setTextContent(value);
    
    return blockElement;
    
  }

  public Type getType() {
    return type;
  }

  public String getValue() {
    return value;
  }

}
