package net.lueckonline.docproc.thymeleaf.blockly;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class Workspace {

  public static final String BLOCKLY_NAMESPACE = "https://developers.google.com/blockly/xml";
  
  private ExportableBlock rootBlock;
  private DocumentBuilder documentBuilder;

  private XPathExpression blockXPath;

  public Workspace() {
    super();
    try {
      DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
      dbf.setNamespaceAware(true);
      documentBuilder = dbf.newDocumentBuilder();
      
      javax.xml.xpath.XPathFactory xPathFactory = javax.xml.xpath.XPathFactory.newInstance();
      blockXPath = xPathFactory.newXPath().compile("//*[local-name()='block']");
      
    } catch (ParserConfigurationException | XPathExpressionException e) {
      throw new IllegalStateException("Can't initialize DOM or XPath factories", e);
    }
  }

  public Document export() {
    var document = documentBuilder.newDocument();
    var rootElement = document.createElementNS(BLOCKLY_NAMESPACE, "xml");
    document.appendChild(rootElement);
    
    if(this.rootBlock != null) rootBlock.toDOM(rootElement);
    
    try {
      NodeList nodeList = (NodeList) blockXPath.evaluate(document, XPathConstants.NODESET);
      
      for(int i=0; i<nodeList.getLength(); i++) {
        var blockElement = (Element) nodeList.item(i);
        var id = blockElement.getAttributes().getNamedItem("id");
        if(id == null)
          blockElement.setAttribute("id", String.valueOf(i));
      }
      
    } catch (XPathExpressionException e) {
      throw new IllegalStateException("Unable to find block elements in workspace XML");
    }
    
    return document;
  }
  
  public ExportableBlock getRootBlock() {
    return rootBlock;
  }

  public <T extends ExportableBlock> T setRootBlock(T rootBlock) {
    this.rootBlock = rootBlock;
    return rootBlock;
  }
  
}
