import * as StringUtils from './string-utils.js'

import initVariables from './ruleeditor.js';
import './thymeleaf.js';
import SchemaLoader from '/static/composer/schemaloader.js';

/*
 * this is our config object for all elements in the document definition
 * after which a matter can be inserted using drag & drop
 * we make this a global constant because we need to assign it to elements 
 * of the document definition in the iframe after it has been loaded
 * and we need the exact same config for matter elements when they are inserted
 */
const DROPPABLECONFIG = {
  // this should prevent jquery from adding css classes which is good for us 
  // because they may leave empty class attributes when the droppable handling 
  // is removed during saving
  // and we may have a lot droppable elements and this could be an performance issue
  addClasses: false,
  // config must be greedy which means that only the element on which 
  // the draggable item is dropped receives the appropriate event
  // if we left it to the default value of "false" all parent elements
  // would also get the event and the eventhandlers in the config object will be executed in
  // their context too
  greedy: true, 
  // handle the event when the object is dropped
  drop: function (event, ui) {
    
    // the element on which the drag has ended
    var target = $(event.target);
    // the element that was dragged (or a copy of it) must contain the url of the matter
    var matterurl = ui.helper.attr("href");
    // insert an element that we can fill with the matter content as immediate sibling after the 
    // element on to which the draggable item was dropped. We will load the content of the matter 
    // into this element
    // the element overrides the global "contenteditable" as matters are reusable and must be changed
    // separately
    // the element also get's an attribute that references the inserted matter
    // this will be saved with the document definition and the matter will later be inserted when a document
    // is created from this
    
  	target.after("<div contenteditable='false' data-matter-link='"+matterurl+"'></div>");
  	// load the html of the matter and insert all direct children of elements that have the css class "matter"
  	target.next().load(matterurl + " .matter > *", function(response, status, xhr) {
  	  if(status !== "error") {
  	    decoratePlaceholdersCommand.execute($("#composer-iframe").contents());
        ruleEditorIconCommand.execute($("#composer-iframe").contents());
  	  } else {
  	    toast("Fehler","Sorry but there was an error: "+xhr.status+" "+xhr.statusText, true);
  	  }
  	});
  	
  	// we need to add the type=css parameter because openhtmltopdf doesn't set the accept header when fetching the resource
  	$(target.prop("ownerDocument")).find("head").prepend("<link rel=\"stylesheet\" type=\"text/css\" href=\""+matterurl+"?type=css\">")
  	
  	// make the new element droppable so that other matter can be inserted after it.
  	target.next().droppable(DROPPABLECONFIG);
  	
  	// as the "out" handler is not called when the element is dropped, we need to to the same
  	// here as in the out handler
    $(event.target).removeClass("insertionPoint");
  },
  over: function (event, ui) {
    // assign the css class insertionPoint to the element on which the cursor is positioned
    // during the drag of an element.
    // the class is used to give a visual hint to the user where the draggable element will be positioned
    // when the the mousebutton is released
    $(event.target).addClass("insertionPoint");
  },
  out: function (event, ui) {
    // remove the visual hint when the mouse leaves the element while the dragable element is still moved
    // this event is not called when the mousebutton is released which will trigger the "drop" event
	$(event.target).removeClass("insertionPoint");
  }
};

const editableEditorCommand = {
  execute(iframeContents) {
  	// make the whole documentdefinition editable in the browser; 
  	// child elements that are not editable in the composer should override this
    iframeContents.find("body").attr("contenteditable", "true");
  },
  undo(iframeContents) {
    iframeContents.find("body").attr("contenteditable", null);
  }
}

const styleEditorCommand = {
    execute(iframeContents) {
      
      var bodyElement = iframeContents.find("body");

      if(bodyElement.attr("style") !== undefined) {
        // save the old style attribute so we can reset it in the undo function
        bodyElement.attr("data-old-attr-style", bodyElement.attr("style"));  
      }
      
   	  /*
   	   * now we add a little css to the document in the iframe.
  	   * the class insertionPoint serves as a visual hint thats shows where a draggable item will be inserted
  	   * we use the global attribute "class" to assign a class that is easy to identify in the undo function
  	   */
  	  iframeContents.find("head").append("<style class='composerStyle' type='text/css'>"
  	      +".insertionPoint {outline:2px solid red;}"
  	      +".placeholders{ color:red !important; background-color: lightgrey !important;}"
  	      +".ruleicon {position:absolute; right:0px;}"
  	      +"*[data-matter-link] {position: relative;}"
  	      +"*[data-matter-link] .ruleicon {opacity: 0;}"
  	      +"*[data-matter-link]:hover .ruleicon {opacity: 1;}"
  	      +"</style>");
      
  	  /*
  	   * adding margins to the page that would be used in the printed version.
  	   * the values are read from the last CSS Page Rule @page
  	   */
  	  var marginCSS = Object.create({});
   	  if(iframeContents.length > 0) {
   	    for(var styleSheetIdx in iframeContents[0].styleSheets) {
   	      var styleSheet = iframeContents[0].styleSheets[styleSheetIdx];
   	      var rules = styleSheet.rules || styleSheet.cssRules;
   	      for(var ruleIdx in rules) {
   	        var rule = rules[ruleIdx];
   	        if (Object.getPrototypeOf(rule) == document.querySelector("#composer-iframe").contentWindow.CSSPageRule.prototype) {
   	          for(var i = rule.style.length; i--;) {
   	            var propName = rule.style[i];
   	            if(propName.startsWith("margin") && propName != "margin-bottom")
   	              	marginCSS[propName] = rule.style[propName];
   	          }
   	        }
   	      }
   	    }
   	  }
  	  Object.keys(marginCSS).forEach((key, index) => {
  	    bodyElement.css(key, marginCSS[key]);  
  	  });
    },
    undo(iframeContents) {
      var bodyElement = iframeContents.find("body");

      // Check whether the style attribute on the body had been saved in the execute function and restore it
      if(bodyElement.attr("data-old-attr-style") !== undefined) {
        bodyElement.attr("style", bodyElement.attr("data-old-attr-style"));
        bodyElement.attr("data-old-attr-style", null);
      } else {
        //simply remove the style attribute completly which would revert setting of the margin in the execute function
        bodyElement.attr("style", null);
      }
      
      // remove the additonal styles added in the execute command
      iframeContents.find("head > style.composerStyle").remove();
    }
}

const droppableCommand = {
    elements(iframeContents) {
   	  	// select the elements after which draggable elements can be inserted and make them droppable
      	var droppableDivs = iframeContents.find("body > *");
   	  	return droppableDivs;
    },
    execute(iframeContents){
  	  	// all droppable elements get the same configuration 
      	this.elements(iframeContents).droppable(DROPPABLECONFIG);
    },
    undo(iframeContents) {
  		// we could remove the droppable handling but as it doesn't change the DOM of the iframed document 
  		// there is no need to
    }
}

const removeEmptyClassesCommand = {
    execute(iframeContent) {
      // this is an undo only command
    },
    undo(iframeContent) {
      iframeContent.find("[class='']").each(function(index, elem) {
        $(elem).removeAttr("class");
      });
    }
}

const decoratePlaceholdersCommand = {
    /**
     * method to call when elements are added or removed to the documentdefinition
     * also when the checkbox to highlight the placeholders is checked
     */
    execute(iframeContent) {
      if($("#checkShowPlaceholders").prop("checked"))
        iframeContent.find("[th\\:text]").addClass("placeholders");
      else
        this.undo(iframeContent);
    },
    undo(iframeContent) {
      iframeContent.find("[th\\:text]").removeClass("placeholders");
    }
}

const ruleEditorIconCommand = {
    
    execute(iframeContent) {
      iframeContent.find("[data-matter-link]").each(function(index) {
	      if($(this).children("img.ruleicon").length == 0) {
         $(this).prepend("<img class=\"ruleicon\" src=\"/static/img/ruleicon.png\" />");
         $(this).find("img.ruleicon").click(openRuleEditor)
	     }
      });
    },
    undo(iframeContent) {
      iframeContent.find("img.ruleicon").remove();
    }
}

const commands =  [
  editableEditorCommand, 
  styleEditorCommand, 
  droppableCommand, 
  decoratePlaceholdersCommand, 
  ruleEditorIconCommand,
  /*this should be the last one or at least after all commands that do something with css classes */ 
  removeEmptyClassesCommand];

function openRuleEditor(openRuleEvent) {
	
	var matterElement = $(openRuleEvent.delegateTarget).parents("[data-matter-link]:first");
	
  $("#ruleEditorDialog button.confirm").click(function(confirmEvent) {

    var code = Blockly.Thymeleaf.workspaceToCode(Blockly.getMainWorkspace());    
	  var newRule = StringUtils.trimToNull(code)
    
    transformExpressionToBlockly(newRule, 
      function() { //successhandler
        matterElement.attr("th:if", newRule );
        $("#ruleEditorDialog").modal('hide');
      },
      function() {
        $("#title-alert")
          .html("Error while saving the rule.")
          .show()
          .effect("bounce", { times: 3 }, "slow")
      });
    
  });
  // the special click event handler must be removed when the dialog closes so that we have a clean
  // state when it's opened the next time'
  $("#ruleEditorDialog").on('hide.bs.modal', function() {
    $("#ruleEditorDialog button.confirm").off("click");
    Blockly.getMainWorkspace().clear();
    $("#title-alert").hide();
  });
  // now we need to send the current th:if content to the server in order to get the 
  // blockly XML back and load it to the Blockly workspace
  
  var currentRule = matterElement.attr("th:if");
  if(currentRule != null && currentRule.trim() != "") {
    transformExpressionToBlockly(currentRule,
      function(data){
         Blockly.Xml.clearWorkspaceAndLoadFromXml(
          // query should have parsed the XML already
          data.firstElementChild,
          Blockly.getMainWorkspace()
        );
      },
      function() {
        $("#title-alert")
          .html("Error while loading the rule")
          .show()
          .effect("bounce", { times: 3 }, "slow")
      });
  }
  
  $("#ruleEditorDialog").modal({ focus:false,  backdrop:false});
}

function transformExpressionToBlockly (expression, successhandler, errorhandler) {
  $.ajax({
    type : "POST",
    // someday this url may be acquired via REST hrefs 
    url: "/rules/transformation", 
    data: expression,
    accepts: { 
      blockly : "application/vnd.blockly+xml"
    },
    contentType: 'text/plain',
    success: function(data, textStatus, jqXHR) {
      if(successhandler !== undefined) successhandler(data);
    },
    error : function(jqXHR, textStatus, errorThrown) {
      if(errorhandler !== undefined) errorhandler(errorThrown);
    }
  });
};

function toast(header, body, isError) {
  $("#toast-header").html(header);
  if(isError) {
    $(".toast-header").css("background", "red");
    $(".toast-header").css("color", "white");
  }
  else {
    $(".toast-header").css("background", "");
    $(".toast-header").css("color", "");
  }
  $("#toast-body").html(body);
  $(".toast").toast("show");
}

function saveDocumentDefinition(successhandler) {
  
  //we need to make a copy of the iframe content which will be run through the undo commands
  // and then sent to the backend
  // this is necessay as the iframe still contains our documentdefinition and we don't want
  // to remove the things on it we have added when it was loaded
  // but we don't want to save those changes.
  // so we remove the changes from the copy and save that one. 
  var $copy = $("#composer-iframe").contents().find("html").clone(true);
  
  commands.forEach(function (cmd) {
	  cmd.undo($copy);
  });
  
  $.ajax({
    type : "POST",
    url : $("#composer-iframe").attr("src"),
    data : "<!DOCTYPE html>\n<html>\n"+$copy.html()+"\n</html>",
    contentType : 'text/html; charset=UTF-8',
    dataType : "html",
    success: successhandler,
    complete: function(xhr, textStatus) {
      if(textStatus === "success") {
        toast("Success", "Successfully saved documemtdefinition");
      } else {
        toast("Error", "Unexpected response from server "+xhr.status+" "+xhr.statusText, true);
      }
    }
  });
}

function buildPartialTree(resource, contextRoot, leafCssClass) {
  //TODO: this maybe a bit simpler if the Controller would return only the content directly below the requested Context Path.
  // but that would require the tree to load the data on expansion of a tree node. 
  return resource._links.self.href
	.substring(resource._links.self.href.indexOf(contextRoot)) // use only the part of the href after the context called to fetch the data 
	.split('/') // split the remaining parts an the slash to get an array of context path parts
	.slice(2) // use everthing from the second array element onwards, as the first is the "matter" part that was used as search element in substring method
	.map(x => ({text : x})) // map each element to a tree node element with a text and an optional nodes array
	.reduceRight((acc, cur, i, a) => {
      // the nodes array of the acc element can only be empty when it's the leaf node. So we add the href Attribute on this
      if(acc.text == null) {
        acc.text = cur.text;
        acc.href = resource._links.self.href;
        acc.class = leafCssClass;
        return acc;
      } else{
     	// now we're building the partial Tree reducing the array in reverse order
        	// in each step the acc is already a partial tree which is added to the nodes array of the next iteration element
        	// in the first iteration the "partial tree" is only the leaf node
      	cur.nodes = [acc];
      	return cur;
      }
	}, {text : null}); // we need a default object so that all elements are processed; escpecially leaf nodes below the context root
}

function buildTree(root, partialTree) {
	var existingElement = root.nodes.find(e => e.text == partialTree.text);
	if(existingElement === undefined) {
	  root.nodes.push(partialTree)
	} else {
	  buildTree(existingElement, partialTree.nodes[0]) //partialTree should essentielly be a list so each node contains only one element
	}
	
	return root;
}

function doAfterConfirm(afterConfirmFunction) {
  $("#confirmDialog button.confirm").click(function(e) {
    $(this).off("click");
  	afterConfirmFunction();
  	$("#confirmDialog").modal('hide');
  });
  $("#confirmDialog").modal('show');
  
}

export function iframeLoaded() {
  
  const iframe = $("#composer-iframe");
  const src = iframe.attr("src");
  
  if(src !== undefined) {
     SchemaLoader
    .load(iframe.attr('src')+'?type=schema')
    .then(function(loadedSchema) {
      const vars = SchemaLoader.readVariableNames(loadedSchema);
      initVariables(vars.map(v => [v.name, v.name, v.type]));
      
      if(Blockly.getMainWorkspace() != null) {
        Blockly.getMainWorkspace().dispose();
      } 
      
      Blockly.inject('#ruleEditorDialog div.modal-body',
        {toolbox: document.getElementById('ruleEditorToolbox')}
      );
    })
  }
  
	commands.forEach(function (cmd) {
  		cmd.execute(iframe.contents());
	});
}


$(document).ready(function() {
  
  $(".toast").toast({delay: 5000, autohide: false});
  
	$("#checkShowPlaceholders").change((e) => {
	  decoratePlaceholdersCommand.execute($("#composer-iframe").contents());	    
	});
	
	$("#save").click(() => {
	  doAfterConfirm(saveDocumentDefinition);
	});
	
	$("#publish").click(() => {
	  doAfterConfirm(() => {
    	// when the docdef should be published we need to save it first.
        // so we basically execute the publishing as successhandler of the saveDocumentDefinition function
    	saveDocumentDefinition(function() {
    	  //successhandler
    	  $.ajax({
    	    type : "POST",
    	    url : $("#composer-iframe").attr("src")+"/publishment",
    	    complete: function(xhr, textStatus) {
    	      if(textStatus === "nocontent") {
    	        toast("Success", "Successfully published documemtdefinition");
    	      } else {
    	        toast("Error", "Unexpected response from server "+xhr.status+" "+xhr.statusText, true);
    	      }
    	    }
    	  });
    	});
	  })
	});
	
	$("#reload").click(e => {
	  doAfterConfirm(() => {
	    $("#composer-iframe").attr('src', function(i, val) {return val;});
	  });
	});
	    
	// load all available documentdefinitions and fill the select box with them
	$.getJSON("/documentdefinitions", function (data) {
	  
	  	var tree = data._embedded.documentDefinitions
	  		.map( dd =>  buildPartialTree(dd, "/documentdefinitions", "ddTreeNode"))
	  		.reduce(buildTree, {text : "root", nodes : []});
	  		
	  	$("#docdeftree").bstreeview({
	  	  data: JSON.stringify(tree.nodes)
	  	});
		
		$(".ddTreeNode").click(function() {
		  doAfterConfirm(() => {
    		  $("#composer-iframe").attr("src", $(this).attr('href'));
		  })
		})
	});
	
	// load all matter and display them
	// make the elements draggable
	$.getJSON("/matter", function (data) {
	  
	  	var tree = data._embedded.matters
	  		.map(m => buildPartialTree(m , "/matter", "matterTreeNode" ))
	  		.reduce(buildTree, {text : "root", nodes : []});
	  	
	  	// display the tree with the treeData
		$("#mattertree").bstreeview({
		  data: JSON.stringify(tree.nodes)
		});
		
	  	// make the leaf nodes (which got the corresponding class) draggable
		$(".matterTreeNode").draggable({
            revert: true, 
            cursor: "crosshair",
            cursorAt: {top:0, left:0},
            iframeFix: true
        });
	    
	});
  
  $("#ruleEditorDialog ").on('shown.bs.modal', function(e) {
    // this should force the workspace to make itself visible when the modal is shown
    // if this is not called then the workspace won't be visible' 
//    window.dispatchEvent(new Event('resize'));
      Blockly.svgResize(Blockly.getMainWorkspace());
  
  });
  
});