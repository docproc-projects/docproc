(function () {
  
  Blockly.defineBlocksWithJsonArray([  
     {
       "type": "logic_operation_stackable",
       "message0": "%1 %2",
       "args0": [
         {
           "type": "field_dropdown",
           "name": "OP",
           "options": [
             [
               "and",
               "AND"
             ],
             [
               "or",
               "OR"
             ]
           ]
         },
         {
           "type": "input_statement",
           "name": "CONDITIONS",
           "check": "Boolean"
         }
       ],
       "previousStatement": "Boolean",
       "nextStatement": "Boolean",
       "colour": 315,
       "tooltip": "",
       "helpUrl": ""
     },
     {
       "type": "logic_compare_stackable",
       "message0": "%1 %2 %3 %4",
       "args0": [
         {
           "type": "input_value",
           "name": "LEFT"
         },
         {
           "type": "field_dropdown",
           "name": "OP",
           "options": [
             ["=","EQ"],
             ["\u2260","NEQ"],
             ["\u200F>","GT"],
             ["\u200F\u2265","GE"],
             ["\u200F<","LT"],
             ["\u200F\u2264","LE"],
             ["contains","CONTAINS"]
           ]
         },
         {
           "type": "input_dummy"
         },
         {
           "type": "input_value",
           "name": "RIGHT"
         }
       ],
       "previousStatement": "Boolean",
       "nextStatement": "Boolean",
       "colour": 230,
       "tooltip": "",
       "helpUrl": ""
     }
   ]);

   /*initVariables([ 
    ['Choose ...', 'defaultInputDataValue', 'default'],
    ['documentType', 'documentType' , 'string'],
    ['cases.supplements', 'cases.supplements', 'array'], 
    ['cases.installationNeeded', 'cases.installationNeeded', 'boolean']
  ]);*/

})();

export default function initVariables(options) {
  /* 
   * it may be better to acutally use real variables for this but I decided agains it 
   * initially because of the following:
   * 1.) We probably don't need the user to define variables. input data structure
   *     can only change with addition/deletion of subtemplates/matter)
   * 2.) veriable_get block do have a special context menu which would allow the user to 
   *     do things we don't want (e.g. add a variable_set block) and I don't know how to 
   *     change that'
   * If variables will be used in the future we need to be sure to change the code 
   * generation too.
   * 
   */
  Blockly.Blocks['rule_input_data'] = {
    init : function () {
      var input = this.appendDummyInput()
                   .appendField('input');
      this.setOutput(true);
      // the blockly documentation says only that the options must be an array of arrays
      // see https://developers.google.com/blockly/reference/js/Blockly.FieldDropdown#FieldDropdown
      // so we passing more than the two mandatory fields and adding a third one for the type
      // we need this in the code generation
      input.appendField(new Blockly.FieldDropdown(options), 'INPUTDATA');
    }  
  };  
}