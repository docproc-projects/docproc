class SchemaLoader {
  
  constructor() {
    this.alreadyLoadedUris = new Map();
  }
  
  load(uri) {
    
    /*
     * brace yourself. The following code is full with promises and maybe hard to
     * understand .. i'll try my best to comment what's happening there
     * Note to future self: Do you still understand this?
     */
    
    return $.get(uri) // jQuery.get returns a promise compatible object. So just assume that the chain of promises starts here 
    .then(function(schema) {
      
      if(schema.allOf === undefined) {
        // shortcut for "simple" schemas that are not contained in an allOf and are 
        // not simple references to other schemas
        if(schema["$ref"] === undefined) return new Promise((res) => res([schema]/*it's important to transport the schema in an array. See below'*/));
        // it's possible that the schema contains only a $ref property .. so we need to load that
        // and return a promise that transports the result to the next promise
        else return $.get(schema["$ref"]).then(schema => new Promise((res) => res([schema] /*it's important to transport the schema in an array. See below'*/)));
      }
      
      // schema may contain an allOf element that contains multiple objects with $ref properties.
      // we need to load each of them and that will happen asynchronously so we will add 
      // the promises that we use for loading to an array.
      // after that we will combine those promises with Promises.all (see below)
      const references = [];
      // we start iterating over the current content of the allOf array
      schema.allOf.forEach(function(sub, subindex) { 
        // trying to read the $ref property of the current object in the array
        const ref = sub["$ref"];
        if(ref !== undefined ) {
          // if a $ref exists ... 
          references.push(
            $.get(ref) // we will create a promise by calling $.get for the url
            .then(function(subref){ // and bind that promise in to an anonymous function that get's the result of the $.get as parameter
              schema.allOf[subindex] = subref; // so we replace the object in the allOf array with the loaded schema element
              return new Promise((res) => res(schema)) // and return a promise that transports the schema so that we can use it in later promises
              // this last promise is the one that is added to the references array! That is important when trying to understand 
              // what happens in the Promise.all call below
          }));
        }
      });
      // now we create a promise from all the promises ... that were added in the loop 
      // so the returned promise resolves when ALL passed promises have resolved themself
      return Promise.all(Array.from(references.values()));
    })
    .then(function(result){
      // this should be called when all promises that loaded referenced schemas
      // the result will contain an array where all items reference the same object => the schema object that was passed 
      // to the Promises that loaded the referenced schemas
      // but where all $ref elements have been replaced  
      const schema = result[0]
      // referenced schemas may be empty so we remove all empty objects
      if(schema.allOf !== undefined) {
        const oldAllOf = schema.allOf;
        schema.allOf = []
        oldAllOf.forEach(function(i) {if(Object.keys(i).length != 0) schema.allOf.push(i)});
      }
      // and return the finally resolved schema
      return new Promise((resolve) => resolve(schema));
    });
  }
  
  /**
   * function the read the schema and create "flattened", full qualified property names
   * for properties defined in the schema.
   * if the schema defines an object named "foo" and definies a spring property "bar" on that
   * the function will return "foo.bar" as an entry in an array.
   *
   * it's important to note that we assume that there is always an object definition at the root of each schema 
   * or an allOf definition whose elements are objects with an object definition at it's root.
   * so even if <code>{ "type" : "string"}</code> would be a valid JSON Schema we can't support it as it won't resolve 
   * to a variable name. Instead <code>{ "type": "object", "properties" : {"myroot" : { "type" : "string"}}}</code>
   * must be used instead
   * 
   * @param schema an json object that adheres to JSON schema spec
   * @param prefix optional attribute that defines a prefix for the returned variables. used for recursion
   */
  readVariableNames(schema, prefix = "") {
    const me = this;
    
    // array to collect the variable names
    const vars = [];
    
    // if the schema has an allOf property we will iterate over all "subschemas" defined in the allOf array
    // and recursivly dive into them passing the current prefix
    if(schema.allOf != null) {
      schema.allOf.forEach(function(sub) {
        me.addAll(vars, me.readVariableNames(sub,prefix));
      });
    } else {
      // now we check the type property of the current JSON Schema object
      switch(schema.type) {
        case "object": // if it's a nested object it must have a properties member which definies the properties of the nested
                       // nested object
          for(var objectProperty in schema.properties) {
            // so iterate over the array and call the function recursivly for each of the properties 
            // it's important to note that we change the prefix here to incorporate the current property
            // if the property is of a basic type the prefix will become the final variable name
            // if the property is an object or an array the prefix will still be used as prefix for further 
            // recursive calls
            this.addAll(vars, this.readVariableNames(schema.properties[objectProperty], (prefix == "" ? prefix : (prefix+".")) + objectProperty))
          }  
          break;
        case "array":
          if(schema.items.type != "object") vars.push({name: prefix, type: schema.type})
          //TODO: handle non simple array types .. but the question is whether thymeleaf could handle those
          break;
        case "string":
        case "number":
        case "integer":
        case "boolean":
          // if we have basic json types we assume that the current prefix contains the full qualified name of the 
          // property because it was created when the function iterated over the properties of the object
          vars.push({name: prefix, type: schema.type})
          break;
      }
    }
    
    return vars;
  }
  
  addAll(array1, array2) {
    Array.prototype.push.apply(array1, array2);
  }

};

var instance = new SchemaLoader();

export default instance;