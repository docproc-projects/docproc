export function trimToNull(string) {
  if(string == null) return string;
  else {
    var trimmed = string.trim();
    return trimmed == "" ? null : trimmed;
  }
}