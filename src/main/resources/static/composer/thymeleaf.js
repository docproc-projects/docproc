(function() {

  Blockly.Thymeleaf = new Blockly.Generator('Thymeleaf');
  
  Blockly.Thymeleaf.ORDER_ATOMIC = 0;
  Blockly.Thymeleaf.ORDER_RELATIONAL = 8;
  Blockly.Thymeleaf.ORDER_EQUALITY = 9;     
  Blockly.Thymeleaf.ORDER_LOGICAL_AND = 13; 
  Blockly.Thymeleaf.ORDER_LOGICAL_OR = 14;
  
  Blockly.Thymeleaf["logic_boolean"] = function(block) {
    const code = (block.getFieldValue('BOOL') == 'TRUE') ? 'true' : 'false';
    return [code, Blockly.Thymeleaf.ORDER_ATOMIC];
  }
  
  Blockly.Thymeleaf["math_number"] = function(block) {
    const code = block.getFieldValue('NUM');
    return [
      // for some reason the value 0 won't work if we return it as number. 
      /// it would only use the empty string in the comparison. 
      // So we return the literal string value here
      code == 0 ? '0' : code,  
      Blockly.Thymeleaf.ORDER_ATOMIC];
  };
  
  Blockly.Thymeleaf['text'] = function(block) {
    const textValue = block.getFieldValue('TEXT');
    const  code = '\'' + textValue + '\'';
    return [code, Blockly.Thymeleaf.ORDER_ATOMIC];
  };
  
  Blockly.Thymeleaf['rule_input_data'] = function(block) {
    
    const inputDataName = block.getFieldValue('INPUTDATA');
    
    // shortcut if the default value is selected. It may be better to throw an exception. we shall see...
    if(inputDataName == 'defaultInputDataValue') return null;
    
    const parentBlock = block.getParent();
    const operation   = (parentBlock && parentBlock.type == 'logic_compare_stackable') ? parentBlock.getFieldValue('OP') : null;
    
    var code = null;
    if(operation == "CONTAINS") {
      // find the array in the fields options for the selected entry
      // it should contain the type of the input data
      var type = getTypeFromDropdownFieldOptions(block, "INPUTDATA");
    
      switch(type) {
        case "array": 
        case "string" : 
          //for arrays and strings the actual OGNL expression must be created by the logic_compare_stackable block
          code = inputDataName;
          break;
        // TODO: maybe we could call toString in the ONGL expression and then use #strings.contains on the result
        default: throw "Unsupported variable type "+type+" for operation CONTAINS ";
      }      
    } else {
      code = '${' + inputDataName + '}';
    }
    
    return [code , Blockly.Thymeleaf.ORDER_ATOMIC];
  };
  
  function getTypeFromDropdownFieldOptions(block, fieldname) {
    const value = block.getFieldValue(fieldname);
    const selectedOption = block.getField(fieldname).getOptions().find(e => e[1] == value);
    return selectedOption !== undefined ? selectedOption[2] : null;
  }
  
  Blockly.Thymeleaf['logic_compare_stackable'] = function(block) {
    
    const operation = block.getFieldValue('OP');
    
    var precedence = null;
    var thymeleafOpSymbol = null;
    
    switch(operation) {
      case "EQ":
        precedence = Blockly.Thymeleaf.ORDER_EQUALITY;
        thymeleafOpSymbol = "==";
        break;
      case "NEQ" : 
        precedence = Blockly.Thymeleaf.ORDER_EQUALITY;
        thymeleafOpSymbol = "!=";
        break;
      case "GT"  : 
        precedence = Blockly.Thymeleaf.ORDER_RELATIONAL;
        thymeleafOpSymbol = "gt";
        break;
      case "GE"  : 
        precedence = Blockly.Thymeleaf.ORDER_RELATIONAL;
        thymeleafOpSymbol = "ge";
        break;
      case "LT"  : 
        precedence = Blockly.Thymeleaf.ORDER_RELATIONAL;
        thymeleafOpSymbol = "lt";
        break;
      case "LE"  : 
        precedence = Blockly.Thymeleaf.ORDER_RELATIONAL;
        thymeleafOpSymbol = "le";
        break;
      case "CONTAINS" : 
        precedence = Blockly.Thymeleaf.ORDER_ATOMIC;
        break;
      default:
        throw "unsupported operation \""+operation+"\". can't generate code for this operation";  
    }
    
    const leftCode  = Blockly.Thymeleaf.valueToCode(block, 'LEFT', precedence);
    const rightCode = Blockly.Thymeleaf.valueToCode(block, 'RIGHT', precedence);
    
    var code = null;
    if(operation == "CONTAINS") {
      var rightBlock = block.getInputTargetBlock("RIGHT");
      if(rightBlock != null && rightBlock.type == "rule_input_data") throw "When using CONTAINS operation the right side MUST NEVER by of type rule_input_data";
      
      var leftBlock  = block.getInputTargetBlock("LEFT");
      if(leftBlock  != null && leftBlock.type == "rule_input_data") {
        var type = getTypeFromDropdownFieldOptions(leftBlock, "INPUTDATA");
        
        switch(type) {
          case "array": code = "${#lists.contains("+leftCode+","+rightCode+")}"; break; 
          case "string": code = "${#strings.contains("+leftCode+","+rightCode+")}"; break;
          default: throw ("variable on the left side of the expression must be a string or array but is "+type);
        }
      } 
    }
    
    // there may be multiple ways throw the code above. So if none of them set the code
    // variable we fall back to the default'
    if(code == null) {
      code = leftCode + ' ' + thymeleafOpSymbol + ' ' +rightCode;  
    } 
    
    return code;
  }
  
  Blockly.Thymeleaf['logic_operation_stackable'] = function(block) {

    var opToken = block.getFieldValue("OP");
    var operationSymbol = null;
    switch(opToken) {
      case "AND" : operationSymbol = " and "; break;
      case "OR"  : operationSymbol = " or "; break;
      default: throw "Unsupported operation "+opToken+". Could not generate code for it."
    }
    
    var conditions = [];
    for(var statement = block.getInputTargetBlock("CONDITIONS"); statement != null; ) {
      
      // if the block is also a logic_operation_stackable we need to make sure that
      // the operator precence is correct and add parantheses if necessary
      if(statement.type == 'logic_operation_stackable' && opToken == "AND" && "OR" == statement.getFieldValue("OP")) {
        conditions.push("(" + Blockly.Thymeleaf.blockToCode(statement) + ")" );
      } else {
        conditions.push(Blockly.Thymeleaf.blockToCode(statement));
      }
      statement = statement.getNextBlock();
    }
    
    return conditions.join(operationSymbol);
  }
  
})();