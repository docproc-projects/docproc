package net.lueckonline.docproc.composer;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import net.lueckonline.docproc.configs.SecurityConfig;
import net.lueckonline.docproc.configs.WebConfig;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {WebConfig.class, SecurityConfig.class})
@WebAppConfiguration
@AutoConfigureMockMvc
class ComposerTest {

  @Autowired
  MockMvc mvc;
  
  @Test
  void shouldNotServerComposerWithoutUser() throws Exception {
    mvc.perform(get("/static/composer.html"))
      .andExpect(status().is(302))
      .andExpect(header().string("Location", "http://localhost/login"));
  }
  
  @Test
  @WithMockUser
  void shouldServerComposerWithoutUser() throws Exception {
    mvc.perform(get("/static/composer.html"))
      .andExpect(status().is(200));
  }

}
