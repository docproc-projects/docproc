package net.lueckonline.docproc.controllers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import net.lueckonline.docproc.services.BlocklyConversionService;
import net.lueckonline.docproc.services.BlocklyConversionService.ConversionError;
import net.lueckonline.docproc.thymeleaf.blockly.Workspace;

@WebMvcTest(controllers = BlocklyController.class)
class BlocklyControllerTest {

  @Autowired
  MockMvc mvc;
  
  @MockBean
  BlocklyConversionService conversionService;
  
  @Test
  @WithMockUser
  void shouldRespondWithWorkspaceAsXml() throws Exception {

    final var expectedRule = "this would be the rule";
    
    var workspace = new Workspace();
    
    when(conversionService.convertRule(expectedRule))
      .thenReturn(workspace);
    
    mvc.perform(post("/rules/transformation").accept("application/vnd.blockly+xml").content(expectedRule))
      .andExpect(status().isOk())
      .andExpect(content().contentType("application/vnd.blockly+xml;charset=ISO-8859-1"))
      .andExpect(content().string("<xml xmlns=\"https://developers.google.com/blockly/xml\"/>"));
  }
  
  @Test
  @WithMockUser
  void shouldResponseWith400OnConversionError() throws Exception {

    String exceptionMessage = "Message from Exception";
    
    when(conversionService.convertRule(anyString()))
      .thenThrow(new ConversionError(exceptionMessage));
    
    mvc.perform(post("/rules/transformation").accept("application/vnd.blockly+xml").content("foobar"))
      .andExpect(status().isBadRequest())
      .andExpect(content().string(containsString(exceptionMessage)));
  }

}
