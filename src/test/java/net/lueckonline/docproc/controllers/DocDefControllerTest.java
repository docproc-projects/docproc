package net.lueckonline.docproc.controllers;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.FileNotFoundException;
import java.util.Arrays;

import org.apache.commons.lang3.RandomStringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.commons.util.StringUtils;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.google.common.net.HttpHeaders;

import net.lueckonline.docproc.errors.DocProcException;
import net.lueckonline.docproc.model.DocumentDefinition;
import net.lueckonline.docproc.services.DocDefService;
import net.lueckonline.docproc.services.PublishingService;
import net.lueckonline.docproc.services.TemplateService;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = DocDefController.class)
class DocDefControllerTest {
  
  @MockBean
  DocDefService docDefServiceMock;
  
  @MockBean
  PublishingService publishServiceMock;
  
  @MockBean
  TemplateService templateServiceMock;

  @Autowired
  MockMvc mvc;
  
  @Captor
  ArgumentCaptor<String> docDefCaptor;

  @Test
  void shouldReturnDocDefs() throws Exception {
    
    when(docDefServiceMock.getDocumentDefinitions())
      .thenAnswer(i -> Arrays.asList( new DocumentDefinition[]{
          DocumentDefinition.builder().templatepath("projectA/invoice.html").build(),
          DocumentDefinition.builder().templatepath("projectB/anotherdocDef.html").build(),
      }));
    
    mvc.perform(get("/documentdefinitions"))
       .andExpect(status().isOk())
       .andExpect(jsonPath("$._embedded.documentDefinitions[?(@.templatepath == 'projectA/invoice.html')].templatepath", 
           hasItems("projectA/invoice.html")))
       .andExpect(jsonPath("$._embedded.documentDefinitions[?(@.templatepath == 'projectB/anotherdocDef.html')].templatepath", 
           hasItems("projectB/anotherdocDef.html")));
  }
  
  @Test
  void shouldReturnDocDefHtml() throws Exception {
    String docDefContent = "<html><body>Hello World</body></html>";
    
    when(docDefServiceMock.readDocumentDefinition("an/possible/path.html"))
      .thenReturn(docDefContent);
    
    mvc.perform(get("/documentdefinitions/an/possible/path.html"))
      .andExpect(header().string(HttpHeaders.CONTENT_TYPE, "text/html;charset=UTF-8"))
      .andExpect(content().string(docDefContent));
  }
  
  @Test
  @WithMockUser
  void shouldUploadDocDef() throws Exception {
    
    var dd = DocumentDefinition.builder().templatepath("foobar/file.html").build();
    
    Document document = Jsoup.parse("<!DOCTYPE html>\n<html></html>");
    
    String outerHtml = document.outerHtml().replaceAll("\\n","").replaceAll(">\\s+<", "><");
    mvc.perform(post("/documentdefinitions/foobar/file.html").content(outerHtml))
       .andExpect(status().isOk());
    
    verify(docDefServiceMock).saveDocumentDefinition(eq(dd), docDefCaptor.capture());
    
    assertEquals(outerHtml, docDefCaptor.getValue().replaceAll("\\n", "").replaceAll(">\\s+<", "><"));
  }
  
  @Test
  @WithMockUser
  void shouldPublishDocDef() throws Exception {
    final var path = "projectA/subdir/invoice.html";
    
    var docdef = DocumentDefinition.builder().templatepath(path).build();
    
    when(docDefServiceMock.getDocumentDefinitionForPath(path))
      .thenReturn(docdef);
    mvc.perform(post("/documentdefinitions/"+path+"/publishment"))
      .andExpect(status().isNoContent());
    
    verify(publishServiceMock, times(1)).publish(eq(docdef));
  }
  
  @Test
  @WithMockUser
  void shouldReturnInternalServerErrorOnPublishingError() throws Exception {
    final var path = "projectA/subdir/invoice.html";
    
    var docdef = DocumentDefinition.builder().templatepath(path).build();
    
    doThrow(new DocProcException("Test exception from mock")).when(publishServiceMock).publish(eq(docdef));
    
    when(docDefServiceMock.getDocumentDefinitionForPath(path))
      .thenReturn(docdef);
    mvc.perform(post("/documentdefinitions/"+path+"/publishment"))
      .andExpect(status().isInternalServerError());
    
  }
  
  @Test
  @WithMockUser
  void shouldNotRemoveMatterOnUpload() throws Exception {
    var dd = DocumentDefinition.builder().templatepath("projectA/fileWithMatterLink.html").build();
    
    Document doc = Jsoup.parse("<!DOCTYPE html><html><head></head><body></body></html>");
    
    for(int i=0; i<5; i++) {
      doc.select("body").append("<div>Content of the div "+RandomStringUtils.randomAlphabetic(30)+"</div>");
    }
    doc.select("div").forEach(div -> div.attr("data-matter-link", "http://foobar:8080/matter/"+RandomStringUtils.randomAlphanumeric(1, 15)));
    
    mvc.perform(post("/documentdefinitions/projectA/fileWithMatterLink.html").content(doc.outerHtml()))
        .andExpect(status().isOk());
    
    verify(docDefServiceMock).saveDocumentDefinition(eq(dd), this.docDefCaptor.capture());
    
    assertTrue(docDefCaptor.getValue() != null);
    
    Document savedDoc = Jsoup.parse(this.docDefCaptor.getValue());
    
    savedDoc.select("[data-matter-link]").forEach(e -> assertTrue(StringUtils.isNotBlank(e.html()),"content of element should not be empty but was \""+e.html()+"\""));
  }
    
  @Test
  void should404WhenFileNotFound() throws Exception {
    when(docDefServiceMock.readDocumentDefinition("abc/def"))
      .thenThrow(FileNotFoundException.class);
    
    mvc.perform(get("/documentdefinitions/abc/def"))
      .andExpect(status().isNotFound());
  }
  
  @Test
  void shouldReturnClientError() throws Exception {
    when(docDefServiceMock.readDocumentDefinition("foobar"))
      .thenThrow(IllegalArgumentException.class);
    
    mvc.perform(get("/documentdefinitions/foobar"))
      .andExpect(status().is4xxClientError());
  }
  
  @Test
  void shouldReturnSchemaOfDocumentdefinition() throws Exception {
    var docdef = DocumentDefinition.builder().templatepath("a/resource/with/a/schema.html").build();
    
    //It's actually irrelevant what is returned here as it is forwareded to templateService 
    //which is mocked too
    when(docDefServiceMock.readDocumentDefinition(docdef.getTemplatepath()))
      .thenReturn("");
    
    var schema = "{\"type\" : \"object\", \"properties\" : { \"test\" : {\"type\" : \"string\"}}}";
    when(templateServiceMock.schema(any(), eq("http://localhost")))
      .thenReturn(schema);
    
    mvc.perform(get("/documentdefinitions/"+docdef.getTemplatepath()).param("type", "schema"))
       .andExpect(header().string(HttpHeaders.CONTENT_TYPE, "application/json;charset=UTF-8"))
       .andExpect(content().string(schema));
  }
  
}
