package net.lueckonline.docproc.controllers;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import de.redsix.pdfcompare.CompareResult;
import de.redsix.pdfcompare.CompareResultImpl;
import de.redsix.pdfcompare.PdfComparator;

@WebMvcTest(controllers = DocumentController.class)
@TestPropertySource(properties = {"output.dir=target/test/doccontroller/"})
public class DocumentControllerTest {
  
  @Autowired
  MockMvc mvc;
  
  @Value("${output.dir}")
  Path outputDir;
  
  @BeforeEach
  void before() throws IOException {
    Files.createDirectories(outputDir);
  }
  
  @AfterEach
  void after() throws IOException {
    FileUtils.deleteDirectory(outputDir.toFile());
  }
  
  @Test
  void shouldReturnDocument() throws Exception {
    
    //TODO create documentService
    
    final String referenceDocPath = "/referenceDocuments/createDocumentFromDocumentDefinition.pdf";
    Files.copy(this.getClass().getResourceAsStream(referenceDocPath), outputDir.resolve("1234567.pdf"));
    
    MvcResult result = mvc.perform(get("/documents/1234567").accept(MediaType.APPLICATION_PDF))
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_PDF))
      .andReturn();
    
    try ( InputStream actual = new ByteArrayInputStream(result.getResponse().getContentAsByteArray()); 
          InputStream expected = this.getClass().getResourceAsStream(referenceDocPath)) {
      PdfComparator<CompareResultImpl> comparator = new PdfComparator<CompareResultImpl>(expected, actual);
      CompareResult compareResult = comparator.compare();
      assertTrue(compareResult.isEqual(), compareResult.toString());
    }
    
  }

}
