package net.lueckonline.docproc.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.net.URL;
import java.util.Date;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import net.lueckonline.docproc.model.Job;
import net.lueckonline.docproc.repositories.JobRepository;
import net.lueckonline.docproc.services.validation.ValidationReport;
import net.lueckonline.docproc.services.validation.ValidationService;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = JobController.class)
class JobControllerTest {

  @Autowired 
  MockMvc mvc;
  
  @MockBean
  JobRepository jobRepository;
  
  @MockBean
  ValidationService validationServiceMock;
  
  @Captor
  ArgumentCaptor<Job> jobCaptor;
  
  @BeforeEach
  void setup() {
    when(validationServiceMock.validate(notNull(), notNull()))
      .thenReturn(new ValidationReport() {
        @Override
        public boolean isSuccess() {
          return true;
        }
      });
  }
  
  @Test
  @WithMockUser
  void shouldAssociateDocDefWithJob() throws Exception {
    Job job = Job.builder().id("jobid").created(new Date()).data("{\"foobar\":\"test\"}").build();
    URL docDefUrl = new URL("http://localhost/documentdefinitions/projectA/foobar.html");
    
    when(jobRepository.findById(job.getId()))
      .thenReturn(Optional.of(job));
    
    mvc.perform(post("/jobs/"+job.getId()+"/htmltemplate").contentType("text/uri-list").content(docDefUrl.toString()))
        .andExpect(status().isNoContent());

    verify(jobRepository).save(jobCaptor.capture());
    assertEquals(docDefUrl, jobCaptor.getValue().getHtmlTemplateUri());
  }
  
  @Test
  void shouldRespond404IfNoDocDefAssociated() throws Exception {
    Job job = Job.builder().id("job-no-template").created(new Date()).data("{}").build();
    
    when(jobRepository.findById(job.getId()))
      .thenReturn(Optional.of(job));
    
    mvc.perform(get("/jobs/"+job.getId()+"/htmltemplate"))
      .andExpect(status().isNotFound());
  }
  
  @Test
  void shouldRedirectToDocDefWhenTemplateIsAssociated() throws Exception {
    String templateUri = "http://localhost:8080/somewhere/over/the/rainbow";
    Job job = Job.builder().id("job-no-template").created(new Date()).data("{}").htmlTemplateUri(new URL(templateUri)).build();
    
    when(jobRepository.findById(job.getId()))
      .thenReturn(Optional.of(job));
    
    mvc.perform(get("/jobs/"+job.getId()+"/htmltemplate"))
      .andExpect(status().isSeeOther())
      .andExpect(header().string(HttpHeaders.LOCATION, templateUri));
  }
  
  @Test
  @WithMockUser
  void shouldReturnBadRequestOnExternalUri() throws Exception {
    Job job = Job.builder().id("jobid").created(new Date()).data("{\"foobar\":\"test\"}").build();
    when(jobRepository.findById(job.getId()))
    .thenReturn(Optional.of(job));
    
    mvc.perform(post("/jobs/"+job.getId()+"/htmltemplate").contentType("text/uri-list").content("http://example.com/documentdefinitions/projectA/foobar.html"))
      .andExpect(status().isBadRequest());
  }
  
  @Test
  @WithMockUser
  void shouldReturnBadRequestOnInvalid() throws Exception {
    Job job = Job.builder().id("jobid").created(new Date()).data("{\"foobar\":\"test\"}").build();
    when(jobRepository.findById(job.getId()))
    .thenReturn(Optional.of(job));
    
    mvc.perform(post("/jobs/"+job.getId()+"/htmltemplate").contentType("text/uri-list").content("../documentdefinitions/projectA/foobar.html"))
      .andExpect(status().isBadRequest());
  }
  
  @Test
  @WithMockUser
  void shouldReturnNotFoundOnInvalidJobId() throws Exception {
    when(jobRepository.findById("JOBID"))
      .thenReturn(Optional.empty());
    
    mvc.perform(post("/jobs/"+"JOBID"+"/htmltemplate").contentType("text/uri-list").content("http://localhost/documentdefinitions/projectA/foobar.html"))
      .andExpect(status().isNotFound());
  }
  
  @Test
  @WithMockUser
  void shouldReturnBadRequestOnInvalidData() throws Exception {
    Job job = Job.builder().id("jobid").created(new Date()).data("{\"foobar\":\"test\"}").build();
    when(jobRepository.findById(job.getId()))
      .thenReturn(Optional.of(job));
    
    String expectedErrorMessage = "Validation Error Message";
    
    reset(validationServiceMock);
    when(validationServiceMock.validate(notNull(), notNull()))
      .thenReturn(new ValidationReport() {
        @Override
        public boolean isSuccess() {
          return false;
        }
        @Override
        public String toString() {
          return expectedErrorMessage;
        }
      });
    
    mvc.perform(post("/jobs/"+job.getId()+"/htmltemplate").contentType("text/uri-list").content("http://localhost/documentdefinitions/projectA/foobar.html"))
      .andExpect(status().isBadRequest())
      .andExpect(content().string(expectedErrorMessage));
  }

}
