package net.lueckonline.docproc.controllers;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.Matchers.emptyString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collections;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import net.lueckonline.docproc.model.Matter;
import net.lueckonline.docproc.services.MatterService;
import net.lueckonline.docproc.services.TemplateService;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = MatterController.class)
class MatterControllerTest {
  
  @MockBean
  MatterService matterServiceMock;
  
  @MockBean
  TemplateService templateServiceMock;

  @Autowired
  MockMvc mvc;
  
  @Test
  void shouldReturnMatter() throws Exception {
    
    when(matterServiceMock.getMatter())
      .thenAnswer(i -> Collections.singletonList(Matter.builder().matterPath("subsidiary/footer.html").contentType("text/html").build()));
    
    mvc.perform(get("/matter"))
       .andExpect(status().isOk())
       .andExpect(jsonPath("$._embedded.matters[?(@.matterPath == 'subsidiary/footer.html')]._links.self.href", 
                  hasItem("/matter/subsidiary/footer.html")))
       .andDo(print());
  }
  
  @ParameterizedTest
  @CsvSource({
    "subsidiary/footer.html, text/html",
    "img/anyPic.png, image/png",
    "downloadContent/another.pdf, application/pdf",
    "differenceBetweenExtenstionAndMimeType.png, application/pdf" // this combination should only show that the mimetype is determined by the service
  })
  void shouldReturnSingleMatter(String matterPath, String matterMimeType) throws Exception {
    
    var matter = Matter.builder().matterPath(matterPath).contentType(matterMimeType).build();
    final var matterContent = "the actual Content is not relevant for this. Only that the Controller returns the content from the service";
    
    when(matterServiceMock.getMatterForPath(matterPath))
      .thenReturn(matter);
    when(matterServiceMock.readMatter(matter))
      .thenReturn(new ByteArrayInputStream(matterContent.getBytes(StandardCharsets.UTF_8)));
    
    mvc.perform(get("/matter/"+matterPath))
      .andExpect(status().isOk())
      .andExpect(header().string(HttpHeaders.CONTENT_TYPE, matterMimeType))
      .andExpect(content().string(matterContent));
  }
  
  @Test
  void shouldReturnStylesOfMatter() throws Exception {
    var matter = Matter.builder().matterPath("a/resource/with/a/style.html").contentType("text/html;charset=UTF-8").build();
    when(matterServiceMock.getMatterForPath(matter.getMatterPath()))
      .thenReturn(matter);
    when(templateServiceMock.css(any(), eq("http://localhost")))
      .thenReturn(".aCssClass {font-size:20px;}");
    
    mvc.perform(get("/matter/"+matter.getMatterPath()).accept("text/css"))
       .andExpect(header().string(HttpHeaders.CONTENT_TYPE, "text/css;charset=UTF-8"))
       .andExpect(content().string(".aCssClass {font-size:20px;}"));
  }
  
  @Test
  void shouldReturn200IfHTMLMatterContainsNoCSS() throws Exception {
    var matter = Matter.builder().matterPath("a/resource/with/a/style.html").contentType("text/html").build();
    when(matterServiceMock.getMatterForPath(matter.getMatterPath()))
      .thenReturn(matter);
    when(matterServiceMock.readMatter(matter))
      .thenReturn(new ByteArrayInputStream(
          "<html><head></head></body><div class=\"aCssClass\">TestDoc</div></body></html>"
          .getBytes(StandardCharsets.UTF_8)));
    
    mvc.perform(get("/matter/"+matter.getMatterPath()).accept("text/css"))
       .andExpect(header().string(HttpHeaders.CONTENT_TYPE, "text/css;charset=UTF-8"))
       .andExpect(content().string(emptyString()));
  }
  
  @Test
  void shouldReturnSchemaOfMatter() throws Exception {
    var matter = Matter.builder().matterPath("a/resource/with/a/schema.html").contentType("text/html;charset=UTF-8").build();
    when(matterServiceMock.getMatterForPath(matter.getMatterPath()))
      .thenReturn(matter);
    var schema = "{\"type\" : \"object\", \"properties\" : { \"test\" : {\"type\" : \"string\"}}}";
    when(templateServiceMock.schema(any(), eq("http://localhost")))
      .thenReturn(schema);
    
    mvc.perform(get("/matter/"+matter.getMatterPath()).param("type", "schema"))
       .andExpect(header().string(HttpHeaders.CONTENT_TYPE, "application/json;charset=UTF-8"))
       .andExpect(content().string(schema));
  }
  
}
