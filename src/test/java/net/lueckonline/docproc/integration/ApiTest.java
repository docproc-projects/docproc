package net.lueckonline.docproc.integration;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.client.Traverson;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import net.lueckonline.docproc.model.Job;
import net.lueckonline.docproc.repositories.JobRepository;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class ApiTest {
  
  @Value("${documentporfolio.docdef.path}")
  Path docDefPath;
  
  @LocalServerPort
  int port;
  
  @Autowired
  JobRepository jobRepo;
  
  private Traverson traverson;

  @BeforeEach
  void before() throws IOException {
    traverson = new Traverson(URI.create("http://localhost:"+port), MediaTypes.HAL_JSON);
    
    Files.createDirectories(docDefPath);
    Files.writeString(docDefPath.resolve("test.template"), "test template");
    
    jobRepo.save(Job.builder().created(new Date()).build());
  }
  
  @Test
  void mainRelationsShouldExist() {
    Link jobLink = traverson.follow("jobs").asLink();
    assertNotNull(jobLink);
    Link docDefLink = traverson.follow("documentdefinitions").asLink();
    assertNotNull(docDefLink);
    Link matterLink = traverson.follow("matter").asLink();
    assertNotNull(matterLink);
  }
  
  @Test 
  void additionalDocDefRelationsInCollectionModel() throws IOException {
    String docDefSelfRel = traverson
        .follow("documentdefinitions")
        .toObject("$._embedded.documentDefinitions[0]._links.self.href");
    assertNotNull(docDefSelfRel);
    
    String docDefPublishmentRel = traverson
        .follow("documentdefinitions")
        .toObject("$._embedded.documentDefinitions[0]._links.publishment.href");
    assertNotNull(docDefPublishmentRel);
    
  }
  
  @Test
  void additionalDocDefRelationsOnEntityModel() {
    var urlOfTemplateInJob= traverson
      .follow("jobs")
      .toObject("$._embedded.jobs[0]._links.htmltemplate");
    assertNotNull(urlOfTemplateInJob);
  }
}
