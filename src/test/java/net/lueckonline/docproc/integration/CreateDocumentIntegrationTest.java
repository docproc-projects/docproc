package net.lueckonline.docproc.integration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Base64;
import java.util.Date;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.awaitility.Awaitility;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.client.Traverson;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.support.CronSequenceGenerator;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.redsix.pdfcompare.CompareResultImpl;
import de.redsix.pdfcompare.PdfComparator;
import net.lueckonline.docproc.model.Job;
import net.minidev.json.JSONArray;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class CreateDocumentIntegrationTest {

  @LocalServerPort
  int port;
  
  @Value("${documentporfolio.docdef.path}")
  Path docDefPath;
  
  @Value("${jobs.interval}")
  String cronExpression;
  
  @Value("${output.dir}")
  Path outputDir;

  @Autowired
  ObjectMapper om;
  
  @Autowired
  @Qualifier("jobExecutors")
  ThreadPoolExecutor executorService;
  
  private String docDefId;

  private String authHeaderValue;
  
  public CreateDocumentIntegrationTest(@Value("${spring.security.user.name}") String username, @Value("${spring.security.user.password}") String password) {
    super();
    this.authHeaderValue = "Basic " + Base64.getEncoder().encodeToString((username+":"+password).getBytes(StandardCharsets.UTF_8));
  }

  @BeforeEach
  void before() throws IOException {
    docDefId = RandomStringUtils.randomAlphabetic(10);
    Files.createDirectories(docDefPath);
    Files.writeString(docDefPath.resolve(docDefId+".html"), "<html><head></head><body><span th:text=\"${greeting.hello}\">Good Bye</span></body></html>");
    Files.createDirectories(outputDir);
  }
  
  @AfterEach
  public void afterEach() throws IOException {
    FileUtils.deleteDirectory(docDefPath.toFile());
    FileUtils.deleteDirectory(outputDir.toFile());
  }
  
  @Test
  void createDocumentFromDocumentDefinition() throws MalformedURLException, IOException, InterruptedException {
    
    Traverson traverson = new Traverson(URI.create("http://localhost:"+port), MediaTypes.HAL_JSON);
    
    JsonNode jobResponse = createJob(traverson);
    JsonNode templateHref = jobResponse.findPath("_links").findPath("htmltemplate").findPath("href");
    JsonNode documentHref = jobResponse.findPath("_links").findPath("document").findPath("href");
    assertFalse(templateHref.isMissingNode(), "no href for relation \"htmltemplate\" found in response of job creation");
    
    var linkArray = (JSONArray) traverson
        .follow("documentdefinitions")
        .toObject("$._embedded.documentDefinitions[?(@.templatepath == '"+docDefId+".html')]._links.self.href");
    var docdefLink = (String) linkArray.get(0);
    
    associateDocDefWithJob(templateHref.asText(), docdefLink);
    
    /* 
     * we need to wait for the job runner to execute. as it's cron based we need to calculate the next 
     * activation time and wait until then
     */
    Date nextJobRunnerExecution = new CronSequenceGenerator(cronExpression).next(new Date());
    Awaitility.await()
      .timeout(2, TimeUnit.SECONDS)
      .pollInterval(200, TimeUnit.MILLISECONDS)
      .until(() -> Instant.now().isAfter(nextJobRunnerExecution.toInstant().plus(500, ChronoUnit.MILLIS)));
    
    // we've waited for the jobRunner to start the threads. Now we need to wait for the threads to complete
    Awaitility.await().until(() -> executorService.getActiveCount() == 0);
    
    HttpURLConnection documentConnection = (HttpURLConnection) new URL(documentHref.asText()).openConnection();
    documentConnection.addRequestProperty(HttpHeaders.ACCEPT, MediaType.APPLICATION_PDF_VALUE);
    assertEquals(200, documentConnection.getResponseCode());
   
    PdfComparator<CompareResultImpl> comparator = new PdfComparator<CompareResultImpl>(
        this.getClass().getResourceAsStream("/referenceDocuments/createDocumentFromDocumentDefinition.pdf"), 
        documentConnection.getInputStream());
    
    assertTrue(comparator.getResult().isEqual());
    
  }

  private void associateDocDefWithJob(String hrefToTemplateOfJob, String docdefLink)
      throws IOException, MalformedURLException {
    HttpURLConnection jobConn = (HttpURLConnection) new URL(hrefToTemplateOfJob).openConnection();
    try {
      jobConn.setRequestProperty("Content-Type", "text/uri-list");
      jobConn.setDoOutput(true);
      jobConn.setRequestProperty(org.springframework.http.HttpHeaders.AUTHORIZATION, authHeaderValue);
      jobConn.getOutputStream().write(docdefLink.getBytes(StandardCharsets.UTF_8));
      assertEquals(204, jobConn.getResponseCode(), "Unexpected responsecode for associating docdef with job"); 
    } finally {
      jobConn.disconnect();
    }
  }

  private JsonNode createJob(Traverson traverson)
      throws IOException, MalformedURLException, JsonGenerationException, JsonMappingException {
    
    var jobsLink = traverson.follow("jobs").asLink();
    
    var jobsConn = (HttpURLConnection) new URL(jobsLink.getHref()).openConnection();
    try {
      jobsConn.setDoOutput(true);
      jobsConn.setRequestProperty(org.springframework.http.HttpHeaders.AUTHORIZATION, authHeaderValue);
      jobsConn.setRequestProperty("Content-type", MediaType.APPLICATION_JSON_VALUE);
      om.writeValue(
          jobsConn.getOutputStream(), 
          Job.builder().created(new Date()).data("{\"greeting\" : {\"hello\" : \"Hello World!\"}}").build());
      assertEquals(201, jobsConn.getResponseCode());
      assertNotNull(jobsConn.getHeaderField("Location"));
      
      return om.readTree(jobsConn.getInputStream());
    } finally {
      jobsConn.disconnect();
    }
  }

}
