package net.lueckonline.docproc.integration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNotNull;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URI;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;

import net.lueckonline.docproc.model.Matter;
import net.lueckonline.docproc.services.DocDefService;
import net.lueckonline.docproc.services.MatterService;
import net.lueckonline.docproc.services.PublishingService;
import net.lueckonline.docproc.services.TemplateService;
import net.lueckonline.docproc.services.validation.ValidationService;

/**
 * the test should verify that the validation works and the schema is compiled
 * from multiple sources
 * 
 * @author thuri
 *
 */
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@MockBean(classes = {PublishingService.class})
class ValidationTest {

  @LocalServerPort
  int port;
  
  @MockBean
  TemplateService templateServiceMock;
  
  @MockBean
  DocDefService docDefServiceMock;
  
  @MockBean
  MatterService matterServiceMock;
  
  @Autowired
  ValidationService validationService;
  
  @ParameterizedTest
  @CsvSource({
    "v1,v1,v2,v2,v3,v3,true", 
    "v1,wrong,v2,v2,v3,v3,false", 
    "v1,v1,v2,wrong,v3,v3,false", 
    "v1,v1,v2,v2,v3,wrong,false"
  })
  void docDefReferencesOtherSchema(String expectedDocProp, 
                                   String actualDocProp, 
                                   String expectedMatterProp, 
                                   String actualMatterProp, 
                                   String expectedNestedProp, 
                                   String actualNestedProp, 
                                   boolean valid) throws ProcessingException, IOException {
    
    when(docDefServiceMock.readDocumentDefinition("testdocdef.html"))
      .thenReturn(""); // not relevant for this test as we're faking/mocking the schema returned by the services
    when(templateServiceMock.schema(isNotNull(), eq("http://localhost:"+port)))
      .thenReturn("{"
                + "  \"allOf\" : ["
                + "    { \"$ref\" : \"/matter/referencedmatter.html?type=schema\" }, "
                + "    {"
                + "      \"type\" : \"object\", "
                + "      \"properties\" : {"
                + "        \"propFromDocDef\" : {"
                + "          \"type\" : \"string\", \"enum\" : [\""+expectedDocProp+"\"]" 
                + "        }"
                + "      },"
                + "      \"required\" : [\"propFromDocDef\"]"
                + "    }"
                + "  ]"
                + "}");
    
    when(matterServiceMock.getMatterForPath("referencedmatter.html"))
      .thenReturn(Matter.builder().contentType("text/html").build());
    var fakeStream = new ByteArrayInputStream(new byte[0]);
    when(matterServiceMock.readMatter(notNull()))
      .thenReturn(fakeStream); // not relevant either
    when(templateServiceMock.schema(same(fakeStream), eq("http://localhost:"+port)))
      .thenReturn("{"
              + "  \"allOf\" : ["
              + "    { \"$ref\" : \"/matter/nested/nestedmatter.html?type=schema\" }, "
              + "    {"
              + "      \"type\" : \"object\", "
              + "      \"properties\" : {"
              + "        \"propFromReferencedMatter\" : {"
              + "          \"type\" : \"string\", \"enum\" : [\""+expectedMatterProp+"\"]" 
              + "        }"
              + "      },"
              + "      \"required\" : [\"propFromReferencedMatter\"]"
              + "    }"
              + "  ]"
              + "}");
    
    var nestedMatter = Matter.builder().contentType("text/html").build();
    when(matterServiceMock.getMatterForPath("nested/nestedmatter.html"))
      .thenReturn(nestedMatter);
    var fakeNestedStream = new ByteArrayInputStream(new byte[0]);
    when(matterServiceMock.readMatter(same(nestedMatter)))
      .thenReturn(fakeNestedStream);
    when(templateServiceMock.schema(same(fakeNestedStream), eq("http://localhost:"+port)))
      .thenReturn("{"
                + "  \"type\" : \"object\", "
                + "  \"properties\" : {"
                + "    \"propFromNestedMatter\" : {"
                + "      \"type\" : \"string\", \"enum\" : [\""+expectedNestedProp+"\"]"
                + "    }"
                + "  },"
                + "  \"required\" : [\"propFromNestedMatter\"]"
                + "}");
    
    
    var toValidate = new ObjectMapper().readTree(
          "{"
        + "  \"propFromDocDef\" : \""+actualDocProp+"\","
        + "  \"propFromReferencedMatter\" : \""+actualMatterProp+"\","
        + "  \"propFromNestedMatter\" : \""+actualNestedProp+"\""
        + "}");
    var report = validationService.validate(URI.create("http://localhost:"+port+"/documentdefinitions/testdocdef.html").toURL(), toValidate);
    
    assertEquals(valid, report.isSuccess(), report.toString());
  }

}
