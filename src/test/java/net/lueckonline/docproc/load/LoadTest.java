package net.lueckonline.docproc.load;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Disabled("Only to run manual")
class LoadTest {

  @Test
  void test() throws MalformedURLException, IOException {
    
    ObjectMapper om = new ObjectMapper();
    var json = Files.readString(Paths.get("src/test/resources/load.json"));
    
    for(int i=0;i < 5000; i++) {
      HttpURLConnection conn = (HttpURLConnection) new URL("http://localhost:8080/jobs").openConnection();
      try {
        conn.setRequestProperty("Authorization", "Basic ZG9jcHJvYzpwYXNzd29ydA==");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setDoOutput(true);
        IOUtils.write(json, conn.getOutputStream(), StandardCharsets.UTF_8.name());
        assertEquals(201, conn.getResponseCode());
        
        JsonNode response = om.readTree(conn.getInputStream());
        var templateConn = (HttpURLConnection) new URL(response.path("_links").path("htmltemplate").path("href").asText()).openConnection();
        try {
          templateConn.setDoOutput(true);
          templateConn.setRequestProperty("Authorization", "Basic ZG9jcHJvYzpwYXNzd29ydA==");
          templateConn.setRequestProperty("Content-Type", "text/uri-list");
          templateConn.getOutputStream().write("http://localhost:8080/documentdefinitions/incorp/orderconfirmation.html".getBytes(StandardCharsets.UTF_8));
          assertEquals(204, templateConn.getResponseCode());
        }finally {
          templateConn.disconnect();
        }
        
      } finally {
        conn.disconnect();
      }
    }
  }

}
