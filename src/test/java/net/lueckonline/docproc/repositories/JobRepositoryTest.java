package net.lueckonline.docproc.repositories;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import net.lueckonline.docproc.model.Job;

@DataJpaTest
class JobRepositoryTest {

  @Autowired
  JobRepository jobRepo;
  
  @Test
  void shouldNotFindUnreadyJobs() {
    
    jobRepo.save(Job.builder().data("{}").created(new Date()).build());
    
    List<Job> jobs = jobRepo.findByStartedIsNullAndHtmlTemplateUriIsNotNull();
    
    assertThat(jobs, empty());
    
  }
  
  @Test
  void shouldFindReadyJobs() throws MalformedURLException {
    
    Job job = jobRepo.save(Job.builder()
        .data("{}")
        .created(new Date())
        .htmlTemplateUri(new URL("http://localhost/documentdefinitions/projectA/foobar.html"))
        .build());
    
    List<Job> jobs = jobRepo.findByStartedIsNullAndHtmlTemplateUriIsNotNull();
    
    assertThat(jobs,containsInAnyOrder(Collections.singletonList(is(job))));
    
  }
  
  @Test
  void shouldNotFindFinishedJobs() throws MalformedURLException {
    jobRepo.save(Job.builder()
        .data("{}")
        .created(new Date())
        .htmlTemplateUri(new URL("http://localhost/documentdefinitions/projectA/foobar.html"))
        .started(new Date())
        .build());
    
    List<Job> jobs = jobRepo.findByStartedIsNullAndHtmlTemplateUriIsNotNull();
    
    assertThat(jobs,is(empty()));
  }

}
