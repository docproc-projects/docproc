package net.lueckonline.docproc.services.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.context.IContext;
import org.thymeleaf.templateresolver.FileTemplateResolver;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import com.openhtmltopdf.pdfboxout.PdfRendererBuilder;

import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import net.lueckonline.docproc.model.Job;
import net.lueckonline.docproc.openhtmltopdf.PdfRendererBuilderFactory;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {CreateDocumentJob.class, ObjectMapper.class})
@TestPropertySource(properties = {"output.dir=target/documents/"})
class CreateDocumentJobTest {

  @MockBean
  ITemplateEngine templateEngine;
  
  @MockBean
  PdfRendererBuilderFactory rendererBuilderFactory;
  
  @Mock
  PdfRendererBuilder rendererBuilder;
  
  @Autowired
  CreateDocumentJob jobProcessor;
  
  //TODO: Send file via http to document server 
  @Value("${output.dir}")
  private Path outputDir; 
                                          // use a fixed seed, so the data will be random but the same in every run
  Faker faker = new Faker(Locale.ENGLISH, new Random(152411510));
  
  @Captor
  ArgumentCaptor<IContext> contextCaptor;
  @Captor
  ArgumentCaptor<Job> jobCaptor;
  
  final private URL templateUrl;
  
  private Job job;
  
  public CreateDocumentJobTest() throws MalformedURLException {
    templateUrl = new URL("http://localhost/documentdefinition/projectJobs/letter.html");
  }
  
  @BeforeEach
  void before() throws IOException {
    Files.createDirectories(outputDir);
  }
  
  @Test
  void shouldCreatePDF() throws Exception {
    
    doNothing().when(templateEngine).process(anyString(), contextCaptor.capture(), any(Writer.class));
    
    when(rendererBuilderFactory.createRendererBuilder(any(), any(), any()))
      .thenReturn(rendererBuilder);
    
    jobProcessor.process(Job.builder()
      .id("job-id")
      .data("{\"greetings\" : { \"hello\": {\"german\" : \"Hallo Welt\"}}, \"anotherfield\" : \"hello from the otherside\"}")
      .htmlTemplateUri(templateUrl)
      .created(new Date())
      .build());
    
    Context ctx = (Context) contextCaptor.getValue();
    assertNotNull(ctx, "No context passed to templateEngine");
    assertThat(ctx.getVariableNames(), contains("greetings", "anotherfield"));
    
    verify(rendererBuilder, times(1)).run();
    
  }
  
  @ParameterizedTest
  @ValueSource(strings = {"[], [{\"greetings\" : \"hello\"}"})
  @NullSource
  void shouldNotAcceptInvalidJobData(String data) throws MalformedURLException {
    
    assertThrows(Exception.class, () -> 
      jobProcessor.process(Job.builder().data(data).htmlTemplateUri(templateUrl).build())
    );
    
    verify(templateEngine, never()).process(anyString(), any(IContext.class), any());
  }
  
  @Test
  void shouldHandleIOExceptionFromTemplateEngine() throws IOException {
    
    when(rendererBuilderFactory.createRendererBuilder(any(), any(), any()))
    .thenReturn(rendererBuilder);
    
    doThrow(new RuntimeException("JUnit Test exception")).when(templateEngine).process(anyString(), any(IContext.class), any(Writer.class));
    
    assertThrows(Exception.class, () -> 
      jobProcessor.process(job)
    );
    
    verify(rendererBuilder, times(0)).run();
  }
  
  @Test
  @Disabled("a simple first test, that used an external resource for creating a test PDF")
  void test() throws Exception {
    
    String jobId = "t";
    URL url = Paths.get("../documentportfolio/matter/orderpositions.html")
         .toUri().toURL();
    
    jobProcessor.process(Job.builder()
        .htmlTemplateUri(url)
        .id(jobId)
        .build());
    
    fail("Not yet implemented");
  }
  
  @Test
  @Disabled("Just an experiment test that helped create the code and example html")
  void experiment() throws Exception {
    TemplateEngine templateEngine = new TemplateEngine();
    FileTemplateResolver templateResolver = new FileTemplateResolver();
    templateResolver.setPrefix("../documentportfolio/matter/");
    templateResolver.setTemplateMode("HTML");
    templateResolver.setCharacterEncoding("UTF-8");
    templateResolver.setCacheable(false);
    templateEngine.setTemplateResolver(templateResolver);
    
    // we define the supplier to generate orderItems here which will be used in the generation of the locations
    // if we'd add it inline it would make the location generation code incomprehendable
    Supplier<OrderItem> orderItemsGenerator = () -> OrderItem.builder()
        .date(faker.date().future(365, TimeUnit.DAYS))
        .netPrice(String.format("%.2f",  faker.random().nextDouble() * faker.random().nextInt(1, 1000)))
        .grossPrice("14,14 €")
        .orderItemTexts(Stream.generate(() -> String.join(" ", faker.lorem().words(faker.random().nextInt(50)))).limit(faker.random().nextInt(0,3)).collect(Collectors.toList()))
        .name(faker.commerce().productName())
        .build();
    
    var locations = Stream.generate(() -> Location.builder()
        .billingAddress(String.join(",", faker.address().streetAddress(), faker.address().streetAddressNumber(), faker.address().zipCode(), faker.address().city()))
        .linkLocation(String.join(",", faker.address().streetAddress(), faker.address().streetAddressNumber(), faker.address().zipCode(), faker.address().city()))
        .phoneNumbers(Stream.generate(() -> faker.phoneNumber().phoneNumber()).limit(faker.random().nextInt(1, 3)).collect(Collectors.toList()))
        .itemsPerInterval(Stream.of("Payable Monthly","Payable Once")
            .map(intervalName -> Pair.of(intervalName, Stream.generate(orderItemsGenerator).limit(faker.random().nextInt(0,10)).collect(Collectors.toList())))
            .collect(Collectors.toMap(Pair::getLeft, Pair::getRight)))
        .build()
    )
    .limit(faker.random().nextInt(1, 3))
    .collect(Collectors.toList());
    
    ObjectMapper om = new ObjectMapper();
    System.out.println(om.writeValueAsString(locations));
    
    Context context = new Context();
    context.setVariable("locations", locations);
    
    File htmlFile = new File("target/documents/orderpositions.html");
    try(FileWriter writer = new FileWriter(htmlFile)) {
      templateEngine.process("orderpositions.html", context, writer);
    }
    
    jobProcessor.process(Job.builder()
        .htmlTemplateUri(htmlFile.toURI().toURL())
        .id("t")
        .build()
    );
  }
  
  @Data @Builder
  final static class Location {
    @Default
    private List<String> phoneNumbers = Collections.emptyList();
    private String linkLocation;
    private String billingAddress;
    private String paymentMethod;
    @Default
    private Map<String, List<OrderItem>> itemsPerInterval = Collections.emptyMap();
  }
  
  @Data @Builder
  final static class OrderItem {
    private String name;
    private Date date;
    private String netPrice;
    private String grossPrice;
    @Default
    private List<String> orderItemTexts = Collections.emptyList();
  }

}
