package net.lueckonline.docproc.services.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import net.lueckonline.docproc.errors.DocProcException;
import net.lueckonline.docproc.model.DocumentDefinition;

@ExtendWith(SpringExtension.class)
@TestPropertySource(
    properties = {"documentporfolio.path=target/documentportfolio"}, 
    // this is needed so we can make sure that the acual used property "documentporfolio.docdef.path" is
    // expanded from the documentporfolio.path
    locations = "classpath:/application.properties")
@ContextConfiguration(classes = DocDefServiceImpl.class)
class DocDefServiceImplTest {

  @Autowired
  DocDefServiceImpl templateService;
  
  @Value("${documentporfolio.docdef.path}")
  Path docdefPath;
  
  @BeforeEach
  public void beforeEach() throws IOException {
    Files.createDirectories(docdefPath);
  }
  
  @AfterEach
  public void afterEach() throws IOException {
    Files.walk(docdefPath)
    .map(Path::toFile)
    .sorted((o1, o2) -> -o1.compareTo(o2)) //to delete files first and directories later
    .forEach(File::delete);
  }
  
  @Test
  void shouldReturnValidFilesAsDocDefs() throws IOException, DocProcException {
    
    Files.writeString(docdefPath.resolve("index.html"), "This is the indexFile");
    Files.writeString(docdefPath.resolve("embedded.html"), "This is the embedded file");
    Files.createDirectories(docdefPath.resolve("subdir").resolve("emptydir"));
    Files.writeString(docdefPath.resolve("subdir").resolve(".htaccess"), "Test");
    Files.writeString(docdefPath.resolve("subdir").resolve("realfile"), "Real file in subdir");
    
    assertThat(templateService.getDocumentDefinitions(), containsInAnyOrder(
        DocumentDefinition.builder().templatepath("index.html").build(), 
        DocumentDefinition.builder().templatepath("embedded.html").build(),
        DocumentDefinition.builder().templatepath("subdir/realfile").build())
    );
  }
  
  /*
   * #readDocumentDefinition
   */
  @ParameterizedTest
  @NullAndEmptySource
  @ValueSource(strings = {"../test", "/asd/../adasd", "/abc/def"})
  public void shouldNotAcceptInvalidPaths(String invalidPath) {
    assertThrows(IllegalArgumentException.class, () -> {
        templateService.readDocumentDefinition(invalidPath);
    });
  }

  @Test
  public void shouldThrowExceptionWhenFileIsNotFound() {
    assertThrows(FileNotFoundException.class, () -> {
      templateService.readDocumentDefinition("notexistingfile");
    });
  }
  
  @Test
  public void shouldNotFindHiddenFiles() throws IOException {
    String fileName = ".hiddenfile";
    Files.writeString(docdefPath.resolve(fileName), "Test");
    
    assertThrows(FileNotFoundException.class, () -> {
      templateService.readDocumentDefinition(fileName);
    });
  }
  
  @Test
  public void shouldNotFindDirectoriesFiles() throws IOException {
    String fileName = "directory/subdir";
    Files.createDirectories(docdefPath.resolve(fileName));
    assertThrows(FileNotFoundException.class, () -> {
      templateService.readDocumentDefinition(fileName);
    });
  }
}
