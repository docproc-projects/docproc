package net.lueckonline.docproc.services.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;

import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.revwalk.RevCommit;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import net.lueckonline.docproc.errors.DocProcException;
import net.lueckonline.docproc.model.DocumentDefinition;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = JGitPublishingService.class)
@TestPropertySource(properties = {"documentporfolio.docdef.path=target/jgit/repos/development/documentdefinitions","publishment.git.commit.message=JunitTest CommitMessage"})
class JGitPublishingServiceTest {

  @Autowired
  JGitPublishingService service;
  
  @Value("${documentporfolio.docdef.path}")
  Path docDefLibraryPath;
  
  @Value("${publishment.git.remote:staging}")
  String pushRemote;
  
  @Value("${publishment.git.commit.message}")
  String commitMessage;

  private Git staging;
  private Git git;

  private Path stagingPath = Paths.get("target/jgit/repos/staging");
  
  @BeforeEach
  void before() throws IOException, IllegalStateException, GitAPIException {
    Files.createDirectories(docDefLibraryPath.getParent().getParent());
    Files.createDirectories(stagingPath);
    
    staging = Git.init().setBare(true).setGitDir(stagingPath.toFile()).call();
    git = Git.cloneRepository()
             .setURI(stagingPath.toAbsolutePath().toUri().toString())
             .setDirectory(docDefLibraryPath.getParent().toFile())
             .setRemote(pushRemote)
             .call();
    
    Files.createDirectories(docDefLibraryPath);
  }
  
  @AfterEach
  void after() throws IOException {
    FileUtils.deleteDirectory(docDefLibraryPath.getParent().toFile());
    FileUtils.deleteDirectory(stagingPath.toFile());
  }
  
  @Test
  void test() throws DocProcException, IOException, NoHeadException, GitAPIException {
    Files.writeString(docDefLibraryPath.resolve("fileToCommit"), "Commit me!");
    
    service.publish(DocumentDefinition.builder().templatepath("fileToCommit").build());
    
    Iterator<RevCommit> commitIterator= git.log().all().call().iterator();
    assertTrue(commitIterator.hasNext(), "no commits found in repository");
    RevCommit commit = commitIterator.next();
    assertFalse(commitIterator.hasNext(), "expected only one commit");
    
    // currently we're checking the default values for author and email
    assertEquals("docproc", commit.getAuthorIdent().getName());
    assertEquals("docproc@lueckonline.net", commit.getAuthorIdent().getEmailAddress());
    
    assertEquals(commitMessage, commit.getFullMessage());
    
    Iterator<RevCommit> stagingIterator = staging.log().all().call().iterator();
    assertTrue(stagingIterator.hasNext(), "no commits found in staging");
    assertEquals(commit.getId(), stagingIterator.next().getId());
   
  }

}
