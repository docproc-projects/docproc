package net.lueckonline.docproc.services.impl;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Date;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.awaitility.Awaitility;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import net.lueckonline.docproc.configs.JobConfig;
import net.lueckonline.docproc.model.Job;
import net.lueckonline.docproc.repositories.JobRepository;
import net.lueckonline.docproc.services.JobProcessor;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {JobRunnerService.class, JobConfig.class})
class JobRunnerServiceTest {

  @MockBean
  JobProcessor jobProcessor;
  
  @MockBean
  JobRepository jobRepository;
  
  @Autowired 
  JobRunnerService jobRunnerService;
  
  @Autowired
  @Qualifier("jobExecutors")
  ThreadPoolExecutor executorService;
  
  @Test
  void shouldExecuteJobsBasedOnScheduling() throws Exception {
    when(jobRepository.findByStartedIsNullAndHtmlTemplateUriIsNotNull())
    .thenReturn(
        // a list of 10 basic jobs without any data because it's not necessary for this test
        Stream.iterate(Job.builder().build(), j -> j.toBuilder().build())
        .limit(10)
        .collect(Collectors.toList()))
    .thenReturn(Collections.emptyList());

    jobRunnerService.executeJobs();

    Awaitility.await().until(() -> executorService.getActiveCount() == 0);
    
    verify(jobProcessor, times(10)).process(any());
  }
  
  @Test
  void testSettingDates() throws InterruptedException {
    
    final Job job = Job.builder().build();
    when(jobRepository.findByStartedIsNullAndHtmlTemplateUriIsNotNull())
      .thenReturn(Collections.singletonList(job));
    
    assertTrue(job.getStarted() == null, "started field must be null before calling processor. Error in DataModel");
    assertTrue(job.getFinished() == null, "finished field must be null before calling processor. Error in DataModel");
    
    jobRunnerService.executeJobs();
    Awaitility.await().until(() -> executorService.getActiveCount() == 0);
    
    assertTrue(job.getStarted() != null && job.getStarted().before(new Date()), "Start date should always be set to the start time of the execution");
    assertTrue(job.getFinished() != null && job.getFinished().before(new Date()),"Finished date should be set to the end time of the execution when jobProcessor completes correctly");
  }
  
  @Test
  void shouldNotSetFinishedOnException() throws Exception {
    final Job job = Job.builder().build();
    when(jobRepository.findByStartedIsNullAndHtmlTemplateUriIsNotNull())
      .thenReturn(Collections.singletonList(job));
    
    doThrow(new Exception("JUNit Test exception")).when(jobProcessor).process(same(job));
    
    jobRunnerService.executeJobs();
    Awaitility.await().until(() -> executorService.getActiveCount() == 0);
    
    assertTrue(job.getStarted() != null && job.getStarted().before(new Date()), "Start date should always be set to the start time of the execution");
    assertTrue(job.getFinished() == null, "Finished date should NOT be set when jobProcessor throws an exception");
  }

}
