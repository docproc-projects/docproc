package net.lueckonline.docproc.services.impl;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

import org.apache.pdfbox.io.IOUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import net.lueckonline.docproc.errors.DocProcException;
import net.lueckonline.docproc.model.Matter;
import net.lueckonline.docproc.services.MatterService;

@ExtendWith(SpringExtension.class)
@TestPropertySource(
    properties = {"documentporfolio.path=target/documentportfolio"}, 
    // this is needed so we can make sure that the acual used property "documentporfolio.docdef.path" is
    // expanded from the documentporfolio.path
    locations = "classpath:/application.properties")
@ContextConfiguration(classes = MatterServiceImpl.class)
class MatterServiceImplTest {

  @Autowired
  MatterService matterService;
  
  @Value("${documentporfolio.matter.path}")
  Path matterLibraryPath;
  
  @BeforeEach
  public void beforeEach() throws IOException {
    Files.createDirectories(matterLibraryPath);
    
    /*
     * the following paths and files are needed in multiple tests
     */
    Files.createDirectories(matterLibraryPath.resolve("footers").resolve("emptydir"));
    Files.writeString(matterLibraryPath.resolve("footers").resolve(".gitignore"), "Test");
  }
  
  @AfterEach
  public void afterEach() throws IOException {
    Files.walk(matterLibraryPath)
    .map(Path::toFile)
    .sorted((o1, o2) -> -o1.compareTo(o2)) //to delete files first and directories later
    .forEach(File::delete);
  }
  
  @Test
  void shouldReturnValidFiles() throws IOException, DocProcException {
    
    Files.writeString(matterLibraryPath.resolve("address.html"), "This may be a template for a postal address");
    Files.writeString(matterLibraryPath.resolve("purchase.html"), "This may be a template for a table listing purchased items");
    Files.writeString(matterLibraryPath.resolve("footers").resolve("subsidiary.html"), "Footer for a subsidiary");
    
    assertThat(matterService.getMatter(), containsInAnyOrder(
        Matter.builder().matterPath("address.html").build(), 
        Matter.builder().matterPath("purchase.html").build(),
        Matter.builder().matterPath("footers/subsidiary.html").build())
    );
  }
  
  @Test
  void shouldReturnMatterForPath() throws IOException {
    long copy = Files.copy(this.getClass().getResourceAsStream("/templates/matter/img/Pixel.png"), matterLibraryPath.resolve("pixel.png"));
    assertTrue(copy > 0, "should have copied than 0 bytes");
    
    assertThat(matterService.getMatterForPath("pixel.png"), is(equalTo(Matter.builder().matterPath("pixel.png").contentType("image/png").build())));
  }
  
  static Stream<String> invalidMatterPaths() {
    return Stream.of(
      "../index.html", // invalid path 
      "/home/user/.ssh/id_rsa", // absolute path
      "~/.ssh/id_rsa", // not found because it is resolved to a path below matterPath
      "footers", // is a directory (created in @BeforeEach method)
      "footers/.gitignore" // is a hidden file  (created in @BeforeEach)
    );
  }
  
  @ParameterizedTest
  @MethodSource("invalidMatterPaths")
  void shouldNotReturnMatterForErrorneousPaths(String path) throws IOException {
    
    assertThrows(Exception.class, () -> {
      matterService.getMatterForPath(path);
    });
  }
  
  @ParameterizedTest
  @MethodSource("invalidMatterPaths")
  void shouldNotReadMatterWithErrorneousPaths(String path) throws IOException {
    
    assertThrows(Exception.class, () -> {
      matterService.readMatter(Matter.builder().matterPath(path).build());
    });
  }
  
  @Test
  void shouldReturnInputStreamForMatter() throws IOException {
    long copy = Files.copy(this.getClass().getResourceAsStream("/templates/matter/img/Pixel.png"), matterLibraryPath.resolve("pixel.png"));
    assertTrue(copy > 0, "should have copied than 0 bytes");
    
    InputStream readMatter = matterService.readMatter(Matter.builder().matterPath("pixel.png").build());
    
    assertArrayEquals(Files.readAllBytes(matterLibraryPath.resolve("pixel.png")), IOUtils.toByteArray(readMatter));
  }
  
  @ParameterizedTest
  @CsvSource({".txt,text/plain", ".html,text/html", ".properties,text/plain"})
  void shouldReturnTextMediaTypeWithCharset(String fileEnding, String type) throws IOException {
    var pathToMatter = "resource"+fileEnding;
    Files.writeString(matterLibraryPath.resolve(pathToMatter),"");
    var matter = matterService.getMatterForPath(pathToMatter);
    assertEquals(type+";charset=UTF-8", matter.getContentType());
  }
  
  @Test
  void shouldNotReturnHtmlMimeTypeForOtherFiles() throws IOException {
    var pathToMatter = "fileWithWrongExtension.png";
    Files.writeString(matterLibraryPath.resolve(pathToMatter),"<html></html>");
    var matter = matterService.getMatterForPath(pathToMatter);
    assertNotEquals("text/html", matter.getContentType());
  }

}
