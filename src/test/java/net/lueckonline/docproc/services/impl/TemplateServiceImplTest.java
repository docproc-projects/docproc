package net.lueckonline.docproc.services.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.lueckonline.docproc.services.TemplateService;

class TemplateServiceImplTest {
  
  private TemplateService templateService;
  
  private String baseUrl = "http://localhost:8080";
  
  private ObjectMapper om = new ObjectMapper();

  @BeforeEach
  void setup() {
    templateService = new TemplateServiceImpl();
  }

  @Test
  void shouldExctractCss() throws IOException {
    
    var html = "<html><head><style type=\"text/css\">Style to find</style></head></html>";
    
    assertEquals("Style to find", templateService.css(new ByteArrayInputStream(html.getBytes(StandardCharsets.UTF_8)), baseUrl));
  }
  
  @Test
  void shouldReturnEmptyStringOnNoCss() throws IOException {
    var html = "<html><head></head></html>";
    
    assertEquals("", templateService.css(new ByteArrayInputStream(html.getBytes(StandardCharsets.UTF_8)), baseUrl));
  }
  
  @Test
  void shouldConcatenateMultipleStyles() throws IOException {
    var html = 
          "<html><head>"
        + "  <style type=\"text/css\">Style to find</style>"
        + "  <style type=\"text/css\">Second style to find</style>"
        + "</head></html>";
    
    assertEquals("Style to find\nSecond style to find", templateService.css(new ByteArrayInputStream(html.getBytes(StandardCharsets.UTF_8)), baseUrl));
  }
  
  @Test
  void shouldExctractSchema() throws IOException {
    var html = 
      "<html><head>"
    + "  <script class=\"schema\" type=\"application/json\">{\"type\" : \"string\"}</script>"
    + "</head></html>";

    assertEquals("{\"type\" : \"string\"}", templateService.schema(new ByteArrayInputStream(html.getBytes(StandardCharsets.UTF_8)), baseUrl));
  }
  
  
  @Test
  void shouldExctractUberSchema() throws IOException {
    var html = 
      "<html>"
    + "  <head>"
    + "    <script class=\"schema\" type=\"application/json\">{\"type\" : \"string\"}</script>"
    + "  </head>"
    + "  <body>"
    + "    <div data-matter-link=\""+baseUrl+"/any/valid/url.html\"/>"
    + "    <div data-matter-link=\""+baseUrl+"/any/valid/url2.html\"/>"
    + "  </body>"
    + "</html>";

    var expectedUberSchema = 
      "{"
    + "  \"allOf\" : ["
    + "    { \"type\" : \"string\" },"
    + "    { \"$ref\" : \""+baseUrl+"/any/valid/url.html?type=schema\"},"
    + "    { \"$ref\" : \""+baseUrl+"/any/valid/url2.html?type=schema\"}"
    + "  ]"
    + "}";
    
    var actual = templateService.schema(new ByteArrayInputStream(html.getBytes(StandardCharsets.UTF_8)), baseUrl);
    
    assertEquals(om.readTree(expectedUberSchema), om.readTree(actual));
  }
  
  @Test
  void shouldReturnDistinctSchemaReferences() throws IOException {
    var html = 
        "<html>"
      + "  <body>"
      + "    <div data-matter-link=\""+baseUrl+"/any/valid/url.html\"/>"
      + "    <div data-matter-link=\""+baseUrl+"/any/valid/url2.html\"/>"
      + "    <div data-matter-link=\""+baseUrl+"/any/valid/url2.html\"/>"
      + "  </body>"
      + "</html>";
    
    var expectedUberSchema = 
        "{"
      + "  \"allOf\" : ["
      + "    { \"$ref\" : \""+baseUrl+"/any/valid/url.html?type=schema\"},"
      + "    { \"$ref\" : \""+baseUrl+"/any/valid/url2.html?type=schema\"}"
      + "  ]"
      + "}";
      
      var actual = templateService.schema(new ByteArrayInputStream(html.getBytes(StandardCharsets.UTF_8)), baseUrl);
      
      assertEquals(om.readTree(expectedUberSchema), om.readTree(actual));
  }
  
  @Test
  void shouldReturnOnlyDataMatterLinkReferencesWhenDocumentSchemaIsEmpty() throws IOException {
    var html = 
        "<html>"
      + "  <body>"
      + "    <div data-matter-link=\""+baseUrl+"/any/valid/url.html\"/>"
      + "  </body>"
      + "</html>";
    
    var expectedUberSchema = 
      "{"
    + "  \"$ref\" : \""+baseUrl+"/any/valid/url.html?type=schema\""
    + "}";

    var actual = templateService.schema(new ByteArrayInputStream(html.getBytes(StandardCharsets.UTF_8)), baseUrl);
    
    assertEquals(om.readTree(expectedUberSchema), om.readTree(actual));
  }
  
  @Test
  void shouldThrowExceptionOnMultipleDocumentSchemas() throws IOException {
    var html = 
        "<html>"
      + "  <head>"
      + "    <script class=\"schema\" type=\"application/json\">{\"type\" : \"string\"}</script>"
      + "    <script class=\"schema\" type=\"application/json\">{\"type\" : \"string\"}</script>"
      + "  </head>"
      + "</html>";
    
    assertThrows(IllegalStateException.class, () -> 
      templateService.schema(new ByteArrayInputStream(html.getBytes(StandardCharsets.UTF_8)), baseUrl)
    );
  }

}
