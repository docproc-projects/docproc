package net.lueckonline.docproc.services.impl;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.matching.UrlPattern;

class ValidationServiceImplTest {

  private static WireMockServer wms;

  @BeforeAll
  static void init() {
    int port = RandomUtils.nextInt(50000, 65000);
    wms = new WireMockServer(port);
    wms.start();
  }
  
  @AfterAll
  static void destroy() { 
    wms.stop();
  }

  private ObjectMapper om = new ObjectMapper();

  @Test
  void testCacheDisabling() throws JsonMappingException, JsonProcessingException, MalformedURLException {
    
    // initialize the ValidationService so that the schemas should not be cached
    var service = new ValidationServiceImpl(false);
    
    UrlPattern pathPattern = urlEqualTo("/test?type=schema");
    URL callUrl = new URL(wms.baseUrl()+"/test");
    JsonNode toValidate = om.readTree("\"value1\"");
    
    // on the first call the wiremock server should reply with the first schema where the request is valid
    wms.stubFor(
        get(pathPattern)
       .willReturn(aResponse().withBody("{ \"type\" : \"string\", \"enum\" : [\"value1\"]}")));
    
    var report1 = service.validate(callUrl, toValidate);
    // on the second call the wiremock server should reply with the second schema where the request is invalid
    wms.resetMappings();
    wms.stubFor(
        get(pathPattern)
       .willReturn(aResponse().withBody("{ \"type\" : \"string\", \"enum\" : [\"value2\"]}")));
      
    var report2 = service.validate(callUrl, toValidate);
    
    wms.verify(2, getRequestedFor(pathPattern));
    
    assertTrue(report1.isSuccess(), report1.toString());
    assertFalse(report2.isSuccess(), report2.toString());
    
  }

}
