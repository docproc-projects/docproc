package net.lueckonline.docproc.thymeleaf;

import static net.lueckonline.docproc.thymeleaf.JsonNodeToObjectMapper.mapJsonNode;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

class JsonNodeToObjectMapperTest {

  private ObjectMapper objectMapper = new ObjectMapper();
  
  @ParameterizedTest
  @ValueSource(strings = {"true", "false", "TRUE", "FALSE"})
  void shouldHandleBoolean(Boolean booleanValue) throws JsonMappingException, JsonProcessingException {
    var node = objectMapper.readTree("{\"prop\" : "+String.valueOf(booleanValue)+"}");
    Object mapped = mapJsonNode(node.get("prop"));
    
    assertEquals(booleanValue, mapped);
    assertEquals(Boolean.class, mapped.getClass());
  }
  
  @Test
  void shouldHandleString() throws JsonMappingException, JsonProcessingException {
    var node = objectMapper.readTree("{\"prop\" : \"stringPropValue\"}");
    assertEquals("stringPropValue", mapJsonNode(node.get("prop")));
  }

  @ParameterizedTest
  @ValueSource(ints = {Integer.MIN_VALUE, Integer.MAX_VALUE, 0})
  void shouldHandleInteger(Integer intValue) throws JsonMappingException, JsonProcessingException {
    var node = objectMapper.readTree("{\"prop\" : "+String.valueOf(intValue)+"}");
    Object mapped = mapJsonNode(node.get("prop"));
    
    assertEquals(intValue, mapped);
    assertEquals(Integer.class, mapped.getClass());
  }
  
  @ParameterizedTest
  @ValueSource(longs = {Long.MIN_VALUE, Long.MAX_VALUE, -2147483649l, 2147483648l})
  void shouldHandleLong(Long longValue) throws JsonMappingException, JsonProcessingException {
    var node = objectMapper.readTree("{\"prop\" : "+String.valueOf(longValue)+"}");
    Object mapped = mapJsonNode(node.get("prop"));
    
    assertEquals(longValue, mapped);
    assertEquals(Long.class, mapped.getClass());
  }
  
  @ParameterizedTest
  @ValueSource(doubles = {Double.MIN_VALUE, Double.MAX_VALUE, 2147483648.123})
  void shouldHandleDouble(Double doubleValue) throws JsonMappingException, JsonProcessingException {
    var node = objectMapper.readTree("{\"prop\" : "+String.valueOf(doubleValue)+"}");
    Object mapped = mapJsonNode(node.get("prop"));
    
    assertEquals(doubleValue, mapped);
    assertEquals(Double.class, mapped.getClass());
  }

  /**
   * <p>it's important that the mapping returns an objectNode for embedded objects</p>
   * <p>by this we make sure that the {@link ognl.PropertyAccessor} for which the {@link JsonNodeToObjectMapper} 
   * was created initially can evaluate a string lazy or incrementally. 
   * essentially it will call {@link JsonNodeToObjectMapper#mapJsonNode(com.fasterxml.jackson.databind.JsonNode)} 
   * again for the objectProp when it is used in an OGNL expression</p>
   * <p>The same idea is used when the root object is mapped to variables for the template engine call. All scalar
   * properties are mapped to normal java types but embedded objects are kept as JsonNode descendants so that
   * they are evaluated when OPNL expressions are evaluated</p>
   * @throws JsonMappingException
   * @throws JsonProcessingException
   */
  @Test
  void shouldHandleObject() throws JsonMappingException, JsonProcessingException {
    var jsonNode = objectMapper.readTree(
          "{"
        + "  \"stringProp\" : \"stringPropValue\","
        + "  \"intProp\"    : 5,"
        + "  \"booleanProp\": true,"
        + "  \"doubleProp\" : 123.456,"
        + "  \"objectProp\" : {"
        + "    \"embeddedStringProp\" : \"embeddedStringPropValue\""
        + "  }"
        + "}");

    Object mapped = mapJsonNode(jsonNode);
    assertTrue(mapped instanceof Map);
    var map = (Map<?,?>) mapped;
    assertEquals("stringPropValue", map.get("stringProp"));
    assertEquals(Integer.valueOf(5), map.get("intProp"));
    assertEquals(Boolean.valueOf(true), map.get("booleanProp"));
    assertEquals(Double.valueOf(123.456), map.get("doubleProp"));
    assertTrue(map.get("objectProp") instanceof ObjectNode);
  }
  
  @Test
  public void shouldHandleArray() throws JsonMappingException, JsonProcessingException {
    var jsonNode = objectMapper.readTree(
          "{"
        + "  \"rootArray\" : [\"rootArrayValue\", \"anotherValueInRootArray\"]"
        + "}");
    
    var mappedRootArray = mapJsonNode(jsonNode.get("rootArray"));
    assertTrue(mappedRootArray instanceof List, "arrays should be converted to List objects");
    var mappedList = ((List<?>)mappedRootArray);
    assertTrue(mappedList.size() == 2, "Unexpected number of results in list");
    assertEquals("rootArrayValue", mappedList.get(0));
    
  }
  
}
