package net.lueckonline.docproc.thymeleaf;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathMatching;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.matching.UrlPathPattern;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ThymeleafConfig.class})
@TestPropertySource(properties = {"documentportfolio.docdef.cache=2000"})
class ThymeleafConfigTest {

  @Value("${documentportfolio.docdef.cache}")
  private Long cacheTTL;
  
  @Autowired
  TemplateEngine templateEngine;
  
  private WireMockServer wm = new WireMockServer(65432);
  
  @BeforeEach
  void before() {
     wm.start();
  }
  
  @AfterEach
  void after() {
    wm.stop();
  }
  
  @Test
  void shouldFetchTemplateViaURL() throws IOException, InterruptedException {
    
    UrlPathPattern urlPathPattern = urlPathMatching("/documentdefinitions/projectA/.*");
    wm.stubFor(get(urlPathPattern)
        .willReturn(aResponse().withBody("<p><span th:if=\"true\" th:text=\"'Hello from Thymeleaf'\">Zonk</span></p>")));

    final Document document;
    try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
      templateEngine.process(wm.baseUrl()+"/documentdefinitions/projectA/test.html", new Context(), new OutputStreamWriter(baos, StandardCharsets.UTF_8));
      try(ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray())) {
        document = Jsoup.parse(bais, StandardCharsets.UTF_8.name(), wm.baseUrl()+"/documentdefinitions/projectA/test.html");
      }
    }
      
    assertEquals("Hello from Thymeleaf", document.select("span").first().html());
    
    wm.verify(1, getRequestedFor(urlPathPattern));
  }
  
  @Test
  void shouldCacheDocDefTemplate() throws IOException, InterruptedException {
    
    UrlPathPattern urlPathPattern = urlPathMatching("/documentdefinitions/cached/.*");
    wm.stubFor(get(urlPathPattern)
        .willReturn(aResponse().withBody("<p><span th:if=\"true\" th:text=\"'Hello from Thymeleaf'\">Zonk</span></p>")));

    try(ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
      for(int i=0; i<2; i++) {
        templateEngine.process(wm.baseUrl()+"/documentdefinitions/cached/test.html", new Context(), new OutputStreamWriter(baos, StandardCharsets.UTF_8));
      }
    }
    
    wm.verify(1, getRequestedFor(urlPathPattern));
    
    // wait for the cache to timeout
    Thread.sleep(cacheTTL + 500);
    
    try(ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
      templateEngine.process(wm.baseUrl()+"/documentdefinitions/cached/test.html", new Context(), new OutputStreamWriter(baos, StandardCharsets.UTF_8));
    }
    
    //after the cache timed out an additional request must have been made
    wm.verify(2, getRequestedFor(urlPathPattern));
  }

}
