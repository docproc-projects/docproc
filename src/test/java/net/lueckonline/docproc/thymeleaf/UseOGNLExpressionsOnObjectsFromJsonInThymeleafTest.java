package net.lueckonline.docproc.thymeleaf;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.StringTemplateResolver;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = ThymeleafConfig.class)
@DirtiesContext(classMode = ClassMode.AFTER_CLASS)
class UseOGNLExpressionsOnObjectsFromJsonInThymeleafTest {
  
  private static final Logger logger =  LogManager.getLogger();

  @Autowired
  TemplateEngine templateEngine;
  
  @BeforeEach
  void addStringTemplateResolver() {
    if(!templateEngine.isInitialized()) 
      // as the engine is supplied by spring it is created only once and used in each test
      // but the manipulation of the resolver is only possible before it is used for the first time
      templateEngine.setTemplateResolver(new StringTemplateResolver());
  }
  
  @Test
  void shouldHandleJsonMapOfStringToString() throws IOException {
    // the json contains a simple object that contains only string properties
    // such objects must be mapped to Map<String,String>
    // this allows us to iterate over instances of MapEntry in thymeleaf. So in the template below 
    // we can use the "value" property of the "item" iteration variable.
    var template = "<ul><li th:each=\"item : ${simpleMap}\" th:text=\"${item.value}\"></li></ul>";
    
    // this object contains a property ("simpleMap") whose properties are not objects but scalar types
    var json = "{\n" + 
        "  \"simpleMap\": {\n" + 
        "    \"Map Entry 1\": \"Intelligent Marble Bag\",\n" + 
        "    \"Map Entry 2\": \"Mediocre Cotton Bottle\"\n" + 
        "  }\n" + 
        "}";
    
    Document resultDoc = runEngineAndParseResult(template, json);
    var liContent = resultDoc.select("li").stream().map(e -> e.html()).collect(toList());
    assertThat(liContent, contains("Intelligent Marble Bag","Mediocre Cotton Bottle"));
  }
  
  @Test
  void shouldHandleJsonMapOfStringToListOfObject() throws IOException {
    
    var template = "<ul><li th:each=\"item : ${mapWithListValue['Map Entry 1']}\" th:text=\"${item.content}\"></li></ul>";
    
    // this oject contains a property (mapWithListValue) which itself contains properties (like 'Map Entry 1') 
    // that are objects themselfs
    var json = "{\n" + 
        "  \"mapWithListValue\": {\n" + 
        "    \"Map Entry 1\": [\n" + 
        "      {\n" + 
        "        \"content\": \"Intelligent Marble Bag\"\n" + 
        "      },\n" + 
        "      {\n" + 
        "        \"content\": \"Shiny Pokemon Ball\"\n" + 
        "      }\n" + 
        "    ],\n" + 
        "    \"Map Entry 2\": [\n" + 
        "      {\n" + 
        "        \"content\": \"Mediocre Cotton Bottle\"\n" + 
        "      }\n" + 
        "    ]\n" + 
        "  }\n" + 
        "}";
    
    Document resultDocument = runEngineAndParseResult(template, json);
    
    List<String> liTexts = resultDocument.select("li").stream().map(e -> e.html()).collect(toList());
    assertThat(liTexts, contains("Intelligent Marble Bag","Shiny Pokemon Ball"));
    assertThat(liTexts, not(contains("Mediocre Cotton Bottle")));
  }
  
  @Test
  void shouldHandleNestedObjects() throws JsonMappingException, JsonProcessingException {
    var template = "<p><span th:text=\"${nested.nestedValue}\"></span></p>";
    var json = "{ \"nested\" : {\"nestedValue\" : \"Hello from nested\"}}";
    
    Document resultDoc = runEngineAndParseResult(template, json);
    assertEquals("Hello from nested", resultDoc.select("span").first().html());
  }
  
  @Test
  void shouldHandleArraysOfPrimitiveTypes() throws JsonMappingException, JsonProcessingException {
    var template = "<p><span th:text=\"${myArray[1]}\"></span></p>";
    var json = "{\"myArray\" : [1,2,3,4,5]}";
    
    Document resultDoc = runEngineAndParseResult(template, json);
    assertEquals("2", resultDoc.select("span").first().html());
  }
  
  @Test
  void shouldHandleArraysOfStrings () throws JsonMappingException, JsonProcessingException {
    var template = "<p><span th:text=\"${myArray[1]}\"></span></p>";
    var json = "{\"myArray\" : [\"first\",\"second\",\"third\",\"fourth\",\"fifth\"]}";
    
    Document resultDoc = runEngineAndParseResult(template, json);
    assertEquals("second", resultDoc.select("span").first().html());
  }
  
  @Test
  void shouldHandleFloatingPointNumber() throws JsonMappingException, JsonProcessingException {
    var template ="<span>PI rounded is </span><span th:text=\"${#numbers.formatDecimal(floatValue, 1, 2, 'COMMA')}\"></span>";
    var json = "{\"floatValue\" : 3.1415}";
    
    var resultDoc = runEngineAndParseResult(template, json);
    assertEquals("3,14", resultDoc.select("span:nth-child(2)").first().html());
  }
  
  @Test
  void shouldHandleDates() throws JsonMappingException, JsonProcessingException {

    Instant timestamp = LocalDateTime.of(2020, 6, 29, 11, 51, 10).toInstant(ZoneOffset.UTC);
    
    var resultDoc = 
      runEngineAndParseResult(
        "<span>Current Time is </span><span th:text=\"${#dates.format(startedAt,'dd.MM.yyyy HH:mm:ss')}\"></span>",
        "{\"startedAt\" : \""+DateTimeFormatter.ISO_INSTANT.format(timestamp)+"\"}");
    
    // we need to define an sdf here WITHOUT specifiy the timezone. So we get the time string in the same timezone as thymeleaf 
    // does. That's because thymeleaf seems to render the date in the current timezone only. 
    SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
    
    assertEquals(sdf.format(Date.from(timestamp)), resultDoc.select("span:nth-child(2)").first().html());
  }
  
  @Test
  void supportOGNInIfExpression() throws JsonMappingException, JsonProcessingException {
    var resultDoc = runEngineAndParseResult(
      "<ul>" 
    + "  <li th:if=\"${rootString} == 'rootStringValue'\" th:text=\"${rootString}\">value of RootString</li>"
    + "  <li th:if=\"${rootBoolean} == false\" th:text=\"${rootBoolean}\">value of rootBoolean</li>" 
    // No idea how we could acutally use real double values for the check. So for now we assume that we could convert the value to string 
    // and use the string equality check
    // but comparators other than equal work ... se below on rootEmbedded.aDecimal
    + "  <li th:if=\"${#numbers.formatDecimal(rootDecimal,1,3,'COMMA')} == '2147483648,123'\" th:text=\"${#numbers.formatDecimal(rootDecimal,1,3,'COMMA')}\">value of rootDecimal</li>" 
    + "  <li th:if=\"${rootInteger} == 2147483647\" th:text=\"${rootInteger}\">value of rootInteger</li>" 
    + "  <li th:if=\"${rootLong} == 2147483648\" th:text=\"${rootLong}\">value of rootLong</li>"
    + "  <li th:if=\"${rootEmbedded.aDecimal} &gt; 12.12\" th:text=\"${rootEmbedded.aDecimal}\">value of rootEmbedded.aDecimal</li>"
    + "  <li th:if=\"${rootEmbedded.anInteger} == 12\" th:text=\"${rootEmbedded.anInteger}\">value of rootEmbedded.anInteger</li>"
    + "  <li th:if=\"${rootEmbedded.aString} == 'foobar'\" th:text=\"${rootEmbedded.aString}\">value of rootEmbedded.aString</li>"
    + "  <li th:if=\"${rootEmbedded.aBoolean} == true\" th:text=\"${rootEmbedded.aBoolean}\">value of rootEmbedded.aBoolean</li>"
    + "  <li th:if=\"${#lists.contains(rootArray,'rootArrayValue')}\" th:text=\"${rootArray[0]}\">value of rootArray</li>"
    + "  <li th:if=\"${#lists.contains(rootEmbedded.anArray,'anArrayValue')}\" th:text=\"${rootEmbedded.anArray[0]}\">value of rootArray</li>"
    + "</ul>", 
      "{"
    + "  \"rootString\" : \"rootStringValue\","  
    + "  \"rootBoolean\" : false," 
    + "  \"rootDecimal\" : 2147483648.123,"  
    + "  \"rootInteger\" : 2147483647,"  
    + "  \"rootLong\"    : 2147483648,"
    + "  \"rootArray\"   : [\"rootArrayValue\"],"
    + "  \"rootEmbedded\" : {"  
    + "    \"aDecimal\" : 12.121,"  
    + "    \"anInteger\": 12,"  
    + "    \"aString\" : \"foobar\","  
    + "    \"aBoolean\" : true,"
    + "    \"anArray\"  : [\"anArrayValue\"]"  
    + "  }"
    + "}");
    
    assertAll(
      () -> assertEquals("rootStringValue", resultDoc.select("li:nth-child(1)").text()),
      () -> assertEquals("false", resultDoc.select("li:nth-child(2)").text()),
      () -> assertEquals("2147483648,123", resultDoc.select("li:nth-child(3)").text()),
      () -> assertEquals("2147483647", resultDoc.select("li:nth-child(4)").text()),
      () -> assertEquals("2147483648", resultDoc.select("li:nth-child(5)").text()),
      () -> assertEquals("12.121", resultDoc.select("li:nth-child(6)").text()),
      () -> assertEquals("12", resultDoc.select("li:nth-child(7)").text()),
      () -> assertEquals("foobar", resultDoc.select("li:nth-child(8)").text()),
      () -> assertEquals("true", resultDoc.select("li:nth-child(9)").text()),
      () -> assertEquals("rootArrayValue", resultDoc.select("li:nth-child(10)").text()),
      () -> assertEquals("anArrayValue", resultDoc.select("li:nth-child(11)").text())
    );
  }
  
  private Document runEngineAndParseResult(String template, String json)
      throws JsonProcessingException, JsonMappingException {
    
    Context ctx = new Context();
    
    final JsonNode jsonNode = new ObjectMapper().readTree(json);
    for(var iterator = jsonNode.fields(); iterator.hasNext(); ) {
      
      var fieldEntry = iterator.next();
      ctx.setVariable(fieldEntry.getKey(), JsonNodeToObjectMapper.mapJsonNode(fieldEntry.getValue()));
    }
    
    String result = templateEngine.process(template, ctx);
    
    logger.atDebug().log(result);
    
    Document resultDocument = Jsoup.parse(result);
    return resultDocument;
  }
  
  @Test
  public void useArray() {

    var template = "<span th:if=\"${#lists.contains(myList, 'Test')}\">Hello World</span>";
    var ctx = new Context();
    ctx.setVariable("myList", Collections.singletonList("Test"));
    var resultDoc = Jsoup.parse(templateEngine.process(template, ctx));
    
    assertEquals("Hello World", resultDoc.select("span").text());
  }

}
