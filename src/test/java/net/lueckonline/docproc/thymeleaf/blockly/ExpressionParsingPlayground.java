package net.lueckonline.docproc.thymeleaf.blockly;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.ByteArrayInputStream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.commons.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.thymeleaf.IEngineConfiguration;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.standard.expression.AndExpression;
import org.thymeleaf.standard.expression.StandardExpressions;
import org.thymeleaf.standard.expression.VariableExpression;

import net.lueckonline.docproc.thymeleaf.ThymeleafConfig;
import ognl.ASTChain;
import ognl.Node;
import ognl.Ognl;
import ognl.OgnlException;
import ognl.OgnlParser;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = ThymeleafConfig.class)
@DirtiesContext(classMode = ClassMode.AFTER_CLASS)
class ExpressionParsingPlayground {

  @Autowired
  TemplateEngine templateEngine;
  
  @Test
  void thymeleafStandardExpressionParser() throws OgnlException {
    
    IEngineConfiguration engineConfig = templateEngine.getConfiguration();
    var expressionParser = StandardExpressions.getExpressionParser(engineConfig);
    
    var expressionContext = engineConfig.getEngineContextFactory().createEngineContext(engineConfig, null, null, new Context());
    
    var expression = expressionParser.parseExpression(expressionContext, "${cases.installationNeeded} and ${#lists.contains(cases.supplements,'TCK007')}");
    
    var and = ((AndExpression)expression);
    var andLeft = (VariableExpression) and.getLeft();
    var andRight = (VariableExpression) and.getRight();
    
    assertEquals("cases.installationNeeded", andLeft.getExpression());
    assertEquals("#lists.contains(cases.supplements,'TCK007')", andRight.getExpression());
    
    Node ognlExpression = (Node) Ognl.parseExpression(andLeft.getExpression());
    NodeVisitor.visit(ognlExpression);
    System.out.println("################################");
    NodeVisitor.visit((ASTChain) Ognl.parseExpression(andRight.getExpression()));
    assertNotNull(ognlExpression);
    System.out.println("################################");
    NodeVisitor.visit((Node) Ognl.parseExpression("test"));
    System.out.println("################################");
    NodeVisitor.visit((Node) Ognl.parseExpression("foobar.test == abc"));
    System.out.println("################################");
    NodeVisitor.visit((Node) Ognl.parseExpression("#lists.contains(cases.supplements, 5)"));
    System.out.println("################################");
    NodeVisitor.visit((Node) Ognl.parseExpression("foobar.test.test"));
    
  }
  
  public static class NodeVisitor {
    
    static int level = 0;
    
    public static void visit(Node node) {

      var intend = org.apache.commons.lang3.StringUtils.repeat(" ", level);
      System.out.println(intend + node.getClass().getName()+"::"+node.toString());
      
      for(int i=0; i< node.jjtGetNumChildren(); i++) {
        level++;
        visit(node.jjtGetChild(i));
        level--;
      }
    }
   
  }
  
  @Test
  void test() {
    var parser = new OgnlParser(new ByteArrayInputStream("${foobar.test} == 'Test'".getBytes()));
    for(var token = parser.getNextToken(); StringUtils.isNotBlank(token.toString()); parser.getNextToken()) {
      token = parser.token;
      System.out.println(token.toString());
    }
  }
  

}
