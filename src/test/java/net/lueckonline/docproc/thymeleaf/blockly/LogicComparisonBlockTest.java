package net.lueckonline.docproc.thymeleaf.blockly;

import static net.lueckonline.docproc.thymeleaf.blockly.TestUtils.assertNode;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.Mockito;
import org.w3c.dom.Element;

import net.lueckonline.docproc.thymeleaf.blockly.LogicComparisonBlock.Operation;

class LogicComparisonBlockTest {

  private Element root;

  @BeforeEach
  void setup() throws ParserConfigurationException {
   root = TestUtils.createTestDoc();
  }
  
  @ParameterizedTest
  @EnumSource(value = Operation.class)
  void shouldSupportDifferentOperations(Operation op) {
    var block = new LogicComparisonBlock(op);
    
    block.toDOM(root);
    
    assertNode(root.getFirstChild(), "block", "type", "logic_compare_stackable");
    assertNode(root.getFirstChild().getFirstChild(), "field", "name", "OP");
    assertThat(root.getFirstChild().getFirstChild().getTextContent(), is(op.name()));
  }
  
  @Test
  void shouldCreateCompleteDOM() {
    
    Operation op = Operation.EQ;
    
    /*
     * Arrange
     */
    var comparisonBlock = new LogicComparisonBlock(op);
    
    var leftValueBlock = Mockito.mock(ValueBlock.class);
    comparisonBlock.setLeftValueBlock(leftValueBlock);
    
    var rightValueBlock = Mockito.mock(ValueBlock.class);
    comparisonBlock.setRightValueBlock(rightValueBlock);
    
    /*
     * Act 
     */
    comparisonBlock.toDOM(root);

    /*
     * Arrange
     */
    
    assertThat(root.getChildNodes().getLength(),is(1));
    
    var assumedBlockNode = root.getFirstChild();
    assertNode(assumedBlockNode, "block", "type", "logic_compare_stackable");
    assertThat(assumedBlockNode.getChildNodes().getLength(),is(3));

    var assumedFieldNode = assumedBlockNode.getChildNodes().item(0);
    assertNode(assumedFieldNode, "field", "name", "OP");
    assertThat(assumedFieldNode.getTextContent(), is(op.name()));

    var assumedLeftValueNode = assumedBlockNode.getChildNodes().item(1);
    assertNode(assumedLeftValueNode, "value", "name", "LEFT");
    verify(leftValueBlock, times(1)).toDOM(same(assumedLeftValueNode));
    
    var assumedRightValueNode = assumedBlockNode.getChildNodes().item(2);
    assertNode(assumedRightValueNode, "value", "name", "RIGHT");
    verify(rightValueBlock, times(1)).toDOM(same(assumedRightValueNode));
    
  }
  
  @Test
  void shouldNotCreateChildNodesIfBlocksAreNotSet() {
    
    /*
     * Arrange
     */
    var comparisonBlock = new LogicComparisonBlock(Operation.EQ);
    
    /*
     * Act
     */
    comparisonBlock.toDOM(root);
    
    /*
     * Assert
     */
    assertThat(root.getChildNodes().getLength(), is(1));
    
    assertNode(root.getFirstChild(), "block", "type", "logic_compare_stackable");
    assertThat(root.getFirstChild().getChildNodes().getLength(), is(1));
    
    assertNode(root.getFirstChild().getFirstChild(), "field", "name", "OP");
    assertThat(root.getFirstChild().getFirstChild().getTextContent(), is("EQ"));
    
  }

}
