package net.lueckonline.docproc.thymeleaf.blockly;

import static net.lueckonline.docproc.thymeleaf.blockly.TestUtils.assertNode;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import net.lueckonline.docproc.thymeleaf.blockly.LogicOperationBlock.Operation;

class LogicOperationBlockTest {
  
  private Element root;

  @BeforeEach
  void setup() throws ParserConfigurationException {
    root = TestUtils.createTestDoc();
  }
  
  @ParameterizedTest
  @EnumSource(Operation.class)
  void shouldCreateDOM(Operation op) {

    /*
     * Arrange
     */
    
    var block = new LogicOperationBlock(op);
    
    /*
     * Act
     */
    block.toDOM(root);
    
    /*
     * Assert
     */

    assertThat(root.getChildNodes().getLength(),is(1));
    
    var assumedBlockNode = root.getFirstChild();
    assertNode(assumedBlockNode, "block", "type", "logic_operation_stackable");
    assertThat(assumedBlockNode.getChildNodes().getLength(), is(1));
    
    var assumedFieldNode = assumedBlockNode.getFirstChild();
    assertNode(assumedFieldNode, "field", "name", "OP");
    assertThat(assumedFieldNode.getTextContent(), is(op.name()));

  }

  @Test
  void testOperands() {
    
    var block = new LogicOperationBlock(Operation.AND);
    
    OperandBlock operand1 = new FakeOperand();
    OperandBlock operand2 = new FakeOperand();
    OperandBlock operand3 = new FakeOperand();
    
    block.addOperand(operand1);
    block.addOperand(operand2);
    block.addOperand(operand3);
    
    block.toDOM(root);
    
    assertNode(root.getFirstChild(), "block", "type", "logic_operation_stackable");
    Node assumedStatementNode = root.getFirstChild().getLastChild();
    assertNode(assumedStatementNode, "statement", "name", "CONDITIONS");
    Node assumedFirstFake = assumedStatementNode.getFirstChild();
    assertThat(assumedFirstFake.getNodeName(), is("fake"));
    assertThat(assumedFirstFake.getFirstChild().getNodeName(), is("next"));
    assertThat(assumedFirstFake.getFirstChild().getFirstChild().getNodeName(), is("fake"));
    assertThat(assumedFirstFake.getFirstChild().getFirstChild().getFirstChild().getNodeName(), is("next"));
    assertThat(assumedFirstFake.getFirstChild().getFirstChild().getFirstChild().getFirstChild().getNodeName(), is("fake"));
    assertThat(assumedFirstFake.getFirstChild().getFirstChild().getFirstChild().getFirstChild().hasChildNodes(), is(false));
    
  }
  
  private static final class FakeOperand implements OperandBlock {

    @Override
    public Element toDOM(Node parent) {
      var fake = parent.getOwnerDocument().createElement("fake");
      parent.appendChild(fake);
      return fake;
    }
    
  }
  
}
