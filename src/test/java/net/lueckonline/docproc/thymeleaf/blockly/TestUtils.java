package net.lueckonline.docproc.thymeleaf.blockly;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

class TestUtils {
  
  static void assertNode(Node node, String nodeName, String attributeName, String attributeValue) {
    assertAll(
      () -> assertThat(node.getNodeName(), is(nodeName)),
      () -> assertThat(node.getAttributes().getLength(), is(1)),
      () -> assertThat(node.getAttributes().getNamedItem(attributeName).getNodeValue(), is(attributeValue))
    );
  }
  
  static Element createTestDoc() throws ParserConfigurationException {
    var doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
    var root = doc.createElement("root");
    doc.appendChild(root);
    return root;
  }

}
