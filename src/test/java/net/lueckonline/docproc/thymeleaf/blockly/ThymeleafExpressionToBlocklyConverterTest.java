package net.lueckonline.docproc.thymeleaf.blockly;

import static net.lueckonline.docproc.thymeleaf.blockly.LogicComparisonBlock.Operation.CONTAINS;
import static net.lueckonline.docproc.thymeleaf.blockly.LogicComparisonBlock.Operation.EQ;
import static net.lueckonline.docproc.thymeleaf.blockly.LogicComparisonBlock.Operation.GE;
import static net.lueckonline.docproc.thymeleaf.blockly.LogicComparisonBlock.Operation.GT;
import static net.lueckonline.docproc.thymeleaf.blockly.LogicComparisonBlock.Operation.LE;
import static net.lueckonline.docproc.thymeleaf.blockly.LogicComparisonBlock.Operation.LT;
import static net.lueckonline.docproc.thymeleaf.blockly.LogicComparisonBlock.Operation.NEQ;
import static net.lueckonline.docproc.thymeleaf.blockly.LogicOperationBlock.Operation.AND;
import static net.lueckonline.docproc.thymeleaf.blockly.LogicOperationBlock.Operation.OR;
import static net.lueckonline.docproc.thymeleaf.blockly.ValueBlock.Type.BOOLEAN;
import static net.lueckonline.docproc.thymeleaf.blockly.ValueBlock.Type.NUMBER;
import static net.lueckonline.docproc.thymeleaf.blockly.ValueBlock.Type.STRING;
import static net.lueckonline.docproc.thymeleaf.blockly.ValueBlock.Type.VARIABLE;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import net.lueckonline.docproc.services.BlocklyConversionService;
import net.lueckonline.docproc.thymeleaf.ThymeleafConfig;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ThymeleafConfig.class, ThymeleafExpressionToBlocklyConverter.class})
class ThymeleafExpressionToBlocklyConverterTest {

  @Autowired
  BlocklyConversionService converter;
  
  static Object[][] simpleValueExpressions() {
    return new Object[][] {
      {"'abc'",             STRING,   "abc"},
      {"''",                STRING,   ""},
      {"123",               NUMBER,   "123"},
      {"-123",              NUMBER,   "-123"},
      {"1.123",             NUMBER,   "1.123"}, 
      {"-1.123",            NUMBER,   "-1.123"},
      {"true",              BOOLEAN,  "TRUE"},
      {"false",             BOOLEAN,  "FALSE"},
      {"TRUE",              BOOLEAN,  "TRUE"},
      {"FALSE",             BOOLEAN,  "FALSE"},
      {"${foobar}",         VARIABLE, "foobar"},
      {"${foobar.property}",VARIABLE, "foobar.property"}
    };
  }
  
  @ParameterizedTest
  @MethodSource("simpleValueExpressions")
  void shouldParseSimpleValueExpression(String expression, ValueBlock.Type type, String value) throws Exception {
    
    var workspace = converter.convertRule(expression);
    // we expect a valueBlock here. So if the case fails the implementation of the parsing is incorrect
    var valueBlock = (ValueBlock) workspace.getRootBlock();
    
    assertThat(valueBlock.getType(), is(type));
    assertThat(valueBlock.getValue(), is(value));
  }
  
  static Object[][] comparisons() {
    return new Object[][] {
      {"${foobar.property} == 'test'",              EQ,       VARIABLE, "foobar.property",STRING,   "test"},
      {"${foobar.property} eq 'test'",              EQ,       VARIABLE, "foobar.property",STRING,   "test"},
      {"true != FALSE",                             NEQ,      BOOLEAN,  "TRUE",           BOOLEAN,  "FALSE"},
      {"true ne FALSE",                             NEQ,      BOOLEAN,  "TRUE",           BOOLEAN,  "FALSE"},
      {"5 > ${intValueVariable}",                   GT,       NUMBER,   "5",              VARIABLE, "intValueVariable"},
      {"5 gt ${intValueVariable}",                  GT,       NUMBER,   "5",              VARIABLE, "intValueVariable"},
      {"${variableLeft} >= ${intValueVariable}",    GE,       VARIABLE, "variableLeft",   VARIABLE, "intValueVariable"},
      {"${variableLeft} ge ${intValueVariable}",    GE,       VARIABLE, "variableLeft",   VARIABLE, "intValueVariable"},
      {"-1 < 2.512",                                LT,       NUMBER,   "-1",             NUMBER,   "2.512"},
      {"-1 lt 2.512",                               LT,       NUMBER,   "-1",             NUMBER,   "2.512"},
      {"-1.41 <= 2.512",                            LE,       NUMBER,   "-1.41",          NUMBER,   "2.512"},
      {"-1.41 le 2.512",                            LE,       NUMBER,   "-1.41",          NUMBER,   "2.512"},
      {"${#lists.contains(foobar.test, 5)}",        CONTAINS, VARIABLE, "foobar.test",    NUMBER,   "5"},
      {"${#lists.contains(foobar.test, 'test')}",   CONTAINS, VARIABLE, "foobar.test",    STRING,   "test"},
      {"${#lists.contains(foobar.test, true)}",     CONTAINS, VARIABLE, "foobar.test",    BOOLEAN,  "TRUE"},
      {"${#lists.contains(foobar.test, 1.41)}",     CONTAINS, VARIABLE, "foobar.test",    NUMBER,   "1.41"},
      {"${#lists.contains(foobar.test, varName)}",  CONTAINS, VARIABLE, "foobar.test",    VARIABLE, "varName"},
      {"${#strings.contains(foobar.test, 'test')}", CONTAINS, VARIABLE, "foobar.test",    STRING,   "test"}
    };
  }
  
  @ParameterizedTest
  @MethodSource("comparisons")
  void shouldParseComparison(
      String expression, 
      LogicComparisonBlock.Operation operation, 
      ValueBlock.Type typeLeft, 
      String valueLeft, 
      ValueBlock.Type typeRight, 
      String valueRight) throws Exception {
    
    var workspace = converter.convertRule(expression);
    var compareBlock = (LogicComparisonBlock) workspace.getRootBlock();
    
    assertThat(compareBlock.getOperation(), is(operation));
    assertThat(compareBlock.getLeftValueBlock().getType(), is(typeLeft));
    assertThat(compareBlock.getLeftValueBlock().getValue(), is(valueLeft));
    assertThat(compareBlock.getRightValueBlock().getType(), is(typeRight));
    assertThat(compareBlock.getRightValueBlock().getValue(), is(valueRight));
  }
  
  static Object[][] logicalOperations() {
    return new Object[][] {
      {"'v' == 'v' and 'v' != 'q'", AND, 2},
      {"'v' == 'v' or  'v' != 'q'", OR, 2},
      {"'v' == 'v' and 'v' != 'q' and 'v' != 'w'", AND, 3},
      {"'v' == 'v' or  'v' != 'q' or  'v' != 'w'", OR, 3},
    };
  }
  
  @ParameterizedTest
  @MethodSource("logicalOperations")
  void shouldParseLogicalOperations(String expression,LogicOperationBlock.Operation operation, int operandCount) throws Exception {

    var workspace = converter.convertRule(expression);
    var compareBlock = (LogicOperationBlock) workspace.getRootBlock();
    
    assertThat(compareBlock.getOperation(), is(operation));
    assertThat(compareBlock.getOperands().size(), is(operandCount));
    for(int i = 0; i <operandCount; i++) {
      assertThat(compareBlock.getOperands().get(i), is(instanceOf(LogicComparisonBlock.class)));
    }
  }
  
  @Test
  void shouldParseOrAfterAnd() throws Exception {
    var workspace = converter.convertRule("'v' == 'v' and 'v' == 'w' or true == false");
    var compareBlock = (LogicOperationBlock) workspace.getRootBlock();
   
    assertThat(compareBlock.getOperation(), is(OR)); // block must be OR block because AND has higher precedence and must be contained in it's own block
   
    assertThat(compareBlock.getOperands().size(), is (2));
    assertThat(compareBlock.getOperands().get(0), is (instanceOf(LogicOperationBlock.class)));
    assertThat(compareBlock.getOperands().get(1), is (instanceOf(LogicComparisonBlock.class)));
    
    var andBlock = (LogicOperationBlock) compareBlock.getOperands().get(0);
    assertThat(andBlock.getOperation(), is(AND));
    assertThat(andBlock.getOperands().size(), is(2));
    assertThat(andBlock.getOperands().get(0), is(instanceOf(LogicComparisonBlock.class)));
    assertThat(andBlock.getOperands().get(1), is(instanceOf(LogicComparisonBlock.class)));
  }
  
  @Test
  void shouldParseOrBeforeAnd() throws Exception {
    var workspace = converter.convertRule("'v' == 'v' or 'v' == 'w' and true == false");
    var compareBlock = (LogicOperationBlock) workspace.getRootBlock();
    
    assertThat(compareBlock.getOperation(), is(OR)); // block must be OR block because AND has higher precedence and must be contained in it's own block
   
    assertThat(compareBlock.getOperands().size(), is (2));
    assertThat(compareBlock.getOperands().get(0), is (instanceOf(LogicComparisonBlock.class)));
    assertThat(compareBlock.getOperands().get(1), is (instanceOf(LogicOperationBlock.class)));
    
    var andBlock = (LogicOperationBlock) compareBlock.getOperands().get(1);
    assertThat(andBlock.getOperation(), is(AND));
    assertThat(andBlock.getOperands().size(), is(2));
    assertThat(andBlock.getOperands().get(0), is(instanceOf(LogicComparisonBlock.class)));
    assertThat(andBlock.getOperands().get(1), is(instanceOf(LogicComparisonBlock.class)));
  }
  
  @Test
  void shouldParseOrInParanthesesBeforeAnd() throws Exception {
    var workspace = converter.convertRule("('v' == 'v' or 'v' == 'w') and true == false");
    var compareBlock = (LogicOperationBlock) workspace.getRootBlock();
    
    assertThat(compareBlock.getOperation(), is(AND)); // block must be AND block because the parantheses make the or a higher precedence
   
    assertThat(compareBlock.getOperands().size(), is (2));
    assertThat(compareBlock.getOperands().get(0), is (instanceOf(LogicOperationBlock.class)));
    assertThat(compareBlock.getOperands().get(1), is (instanceOf(LogicComparisonBlock.class)));
    
    var orBlock = (LogicOperationBlock) compareBlock.getOperands().get(0);
    assertThat(orBlock.getOperation(), is(OR));
    assertThat(orBlock.getOperands().size(), is(2));
    assertThat(orBlock.getOperands().get(0), is(instanceOf(LogicComparisonBlock.class)));
    assertThat(orBlock.getOperands().get(1), is(instanceOf(LogicComparisonBlock.class)));
  }
  
  @Test
  void complexExpressions() throws Exception {
    
    var workspace = converter.convertRule("${v1} == 'test' and ${#lists.contains(v2,5)} or ${booleanVariable} == true or 5 > ${numberVar} and 'test' >= 'te'");
    var compareBlock = (LogicOperationBlock) workspace.getRootBlock();
    
    /*
      OR
        AND
          ${v1} == 'test'
          ${#lists.contains(v2,5)} 
        ${booleanVariable} == true
        AND 
          5 > ${numberVar}
          'test' >= 'te'
    */
    
    
    assertThat(compareBlock.getOperation(), is(OR));

    // Outer OR
    assertThat(compareBlock.getOperands().size(), is(3));
    assertThat(compareBlock.getOperands().get(0), is (instanceOf(LogicOperationBlock.class)));
    assertThat(compareBlock.getOperands().get(1), is (instanceOf(LogicComparisonBlock.class)));
    assertThat(compareBlock.getOperands().get(2), is (instanceOf(LogicOperationBlock.class)));
    
    // First AND below OR
    {
      var andBlock = (LogicOperationBlock) compareBlock.getOperands().get(0);
      assertThat(andBlock.getOperation(), is(AND));
      assertThat(andBlock.getOperands().size(), is(2));
        var firstCompare = (LogicComparisonBlock) andBlock.getOperands().get(0);
        assertThat(firstCompare.getOperation(), is(EQ));
        assertThat(firstCompare.getLeftValueBlock().getType(), is(VARIABLE));
        assertThat(firstCompare.getLeftValueBlock().getValue(), is("v1"));
        assertThat(firstCompare.getRightValueBlock().getType(), is(STRING));
        assertThat(firstCompare.getRightValueBlock().getValue(), is("test"));
        var secondCompare = (LogicComparisonBlock) andBlock.getOperands().get(1);
        assertThat(secondCompare.getOperation(), is(CONTAINS));
        assertThat(secondCompare.getLeftValueBlock().getType(), is(VARIABLE));
        assertThat(secondCompare.getLeftValueBlock().getValue(), is("v2"));
        assertThat(secondCompare.getRightValueBlock().getType(), is(NUMBER));
        assertThat(secondCompare.getRightValueBlock().getValue(), is("5"));
    }
    
    // ${booleanVariable} == true below OR
    {
      var innerCompareBlock = (LogicComparisonBlock) compareBlock.getOperands().get(1);
      assertThat(innerCompareBlock.getOperation(), is(EQ));
      assertThat(innerCompareBlock.getLeftValueBlock().getType(), is(VARIABLE));
      assertThat(innerCompareBlock.getLeftValueBlock().getValue(), is("booleanVariable"));
      assertThat(innerCompareBlock.getRightValueBlock().getType(), is(BOOLEAN));
      assertThat(innerCompareBlock.getRightValueBlock().getValue(), is("TRUE"));
    }
    
    // second AND below OR
    {
      var andBlock = (LogicOperationBlock) compareBlock.getOperands().get(2);
      assertThat(andBlock.getOperation(), is(AND));
      assertThat(andBlock.getOperands().size(), is(2));
        var firstCompare = (LogicComparisonBlock) andBlock.getOperands().get(0);
        assertThat(firstCompare.getOperation(), is(GT));
        assertThat(firstCompare.getLeftValueBlock().getType(), is(NUMBER));
        assertThat(firstCompare.getLeftValueBlock().getValue(), is("5"));
        assertThat(firstCompare.getRightValueBlock().getType(), is(VARIABLE));
        assertThat(firstCompare.getRightValueBlock().getValue(), is("numberVar"));
        var secondCompare = (LogicComparisonBlock) andBlock.getOperands().get(1);
        assertThat(secondCompare.getOperation(), is(GE));
        assertThat(secondCompare.getLeftValueBlock().getType(), is(STRING));
        assertThat(secondCompare.getLeftValueBlock().getValue(), is("test"));
        assertThat(secondCompare.getRightValueBlock().getType(), is(STRING));
        assertThat(secondCompare.getRightValueBlock().getValue(), is("te"));
    }
   
  }
  
  static Object[][] unsupportedExpressions() {
    return new Object[][] {
      {"true and false", "true"}, // the conversion will stop on the first error which is the left side 
      {"false or true" , "false"}, // the conversion will stop on the first error which is the left side
      {"false == false and true", "true"} // the left side of th or expression is ok but the right side is not
    };
  }
  
  @ParameterizedTest
  @MethodSource("unsupportedExpressions")
  void conversionErrorTests(String expression, String expectedErrorPart) {
    var thrown = assertThrows(ThymeleafConversionError.class, () -> converter.convertRule(expression));
    assertThat(thrown.getErrorExpression(), is(expectedErrorPart)); 
  }

}
