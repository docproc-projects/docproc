package net.lueckonline.docproc.thymeleaf.blockly;

import static net.lueckonline.docproc.thymeleaf.blockly.TestUtils.assertNode;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.w3c.dom.Element;

import net.lueckonline.docproc.thymeleaf.blockly.ValueBlock.Type;

class ValueBlockTest {

  private Element root;

  @BeforeEach
  void setup() throws ParserConfigurationException {
    root = TestUtils.createTestDoc();
  }
  
  @ParameterizedTest
  @ValueSource(strings = {"rule_input_data", "text", "math_number", "logic_boolean"})
  void shouldCreateDOM(String blockType) {
    
    var value = RandomStringUtils.randomAlphabetic(10);
    
    /*
     * Arrange
     */
    final ValueBlock valueBlock;
    final String fieldName;
    switch(blockType) {
      case "rule_input_data": 
        valueBlock = new ValueBlock(Type.VARIABLE, value);
        fieldName = "INPUTDATA";
        break;
      case "text" : 
        valueBlock = new ValueBlock(Type.STRING, value);
        fieldName = "TEXT";
        break;
      case "math_number": 
        valueBlock = new ValueBlock(Type.NUMBER, value);
        fieldName = "NUM";
        break;
      case "logic_boolean": 
        valueBlock = new ValueBlock(Type.BOOLEAN, value); 
        fieldName = "BOOL";
        break;
      default: throw new IllegalArgumentException("No test definied for "+blockType);
    }

    /*
     * Act
     */
    valueBlock.toDOM(root);
    
    assertThat(root.getChildNodes().getLength(), is(1));
    
    var assumedBlockNode = root.getFirstChild();

    assertNode(assumedBlockNode, "block", "type", blockType);
    assertThat(assumedBlockNode.getChildNodes().getLength(), is(1));
    
    var assumedFieldNode = assumedBlockNode.getFirstChild();
    assertNode(assumedFieldNode, "field", "name", fieldName);
    
    assertThat(assumedFieldNode.getTextContent(), is(value));
    assertThat(assumedFieldNode.getChildNodes().getLength(), is(1)); //the text content
    
  }

}
