package net.lueckonline.docproc.thymeleaf.blockly;

import static net.lueckonline.docproc.thymeleaf.blockly.LogicComparisonBlock.Operation.CONTAINS;
import static net.lueckonline.docproc.thymeleaf.blockly.LogicComparisonBlock.Operation.EQ;
import static net.lueckonline.docproc.thymeleaf.blockly.LogicOperationBlock.Operation.AND;
import static net.lueckonline.docproc.thymeleaf.blockly.LogicOperationBlock.Operation.OR;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import net.lueckonline.docproc.thymeleaf.blockly.ValueBlock.Type;

class WorkspaceTest {

  @Test
  void shouldCreateDOM() {
    
    var blockMock = Mockito.mock(ExportableBlock.class);
    
    var workspace = new Workspace();
    workspace.setRootBlock(blockMock);
    
    var doc = workspace.export();

    assertThat(doc, is(not(nullValue())));
    
    assertThat(doc.getDocumentElement().getNamespaceURI(), is("https://developers.google.com/blockly/xml"));
    assertThat(doc.getDocumentElement().getNodeName(), is("xml"));
    
    verify(blockMock, times(1)).toDOM(same(doc.getDocumentElement()));
  }
  
  @Test
  void shouldCreateEmptyWorkspace() {
    
    var workspace = new Workspace();
    var doc = workspace.export();
    
    assertThat(doc, is(not(nullValue())));
    
    assertThat(doc.getDocumentElement().getNamespaceURI(), is("https://developers.google.com/blockly/xml"));
    assertThat(doc.getDocumentElement().getNodeName(), is("xml"));
    
    assertThat(doc.getDocumentElement().hasChildNodes(), is(false));
    
  }
  
  @Test
  void createComplexWorkspace() throws SAXException, IOException, ParserConfigurationException {
    
    var workspace = new Workspace();
    
    workspace.setRootBlock(new LogicOperationBlock(
       AND))
      .addOperand(LogicComparisonBlock.builder().operation(
         CONTAINS)
        .leftValueBlock(ValueBlock.builder()
          .type(Type.VARIABLE)
          .value("cases.supplements").build())
        .rightValueBlock(ValueBlock.builder()
          .type(Type.STRING)
          .value("TCK007").build()).build())
      .addOperand(LogicComparisonBlock.builder().operation(
         EQ)
        .leftValueBlock(ValueBlock.builder()
          .type(Type.VARIABLE)
          .value("documentType").build())
        .rightValueBlock(ValueBlock.builder()
          .type(Type.STRING)
          .value("Auftragsbestätigung").build()).build())
      .addOperand(LogicOperationBlock.builder().operation(
           OR).build()
          .addOperand(LogicComparisonBlock.builder().operation(
             EQ)
            .leftValueBlock(ValueBlock.builder()
              .type(Type.NUMBER)
              .value("1").build())
            .rightValueBlock(ValueBlock.builder()
              .type(Type.BOOLEAN)
              .value("TRUE").build()).build())
          .addOperand(LogicComparisonBlock.builder().operation(
             EQ)
            .leftValueBlock(ValueBlock.builder()
              .type(Type.NUMBER)
              .value("0").build())
            .rightValueBlock(ValueBlock.builder()
              .type(Type.BOOLEAN)
              .value("TRUE").build()).build()));
    
    final var export = workspace.export();
    
    
    try {
      var transformer = TransformerFactory.newInstance().newTransformer();
      transformer.setOutputProperty(OutputKeys.INDENT, "yes");
      transformer.transform(new DOMSource(export), new StreamResult(new FileOutputStream("target/testoutput.xml")));
    } catch (TransformerException | TransformerFactoryConfigurationError e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    dbf.setNamespaceAware(true);
    Document control;
    try(InputStream stream = this.getClass().getResourceAsStream("/blockly/example.xml")) {
      control = dbf.newDocumentBuilder().parse(stream);
    }
    
    XMLUnit.setIgnoreWhitespace(true);
    XMLUnit.setIgnoreAttributeOrder(true);
    Diff diff = XMLUnit.compareXML(control, export);
    
    assertTrue(diff.identical(), diff.toString());
  }

}
